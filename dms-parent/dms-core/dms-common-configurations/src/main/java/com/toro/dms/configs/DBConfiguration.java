package com.toro.dms.configs;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

@Configuration
@EnableJpaRepositories("com.toro.dms.repository")
@PropertySource(value = "classpath:datasource.properties")
public class DBConfiguration {

	@Value("${driver.class.name}")
	private String driverClassName;
	
	@Value("${db.url}")
	private String dbUrl;
	
	@Value("${db.username}")
	private String dbUsername;
	
	@Value("${db.password}")
	private String dbPpassword;
	
	@Value("${db.platform}")
	private String dbPlatform;
	
	@Value("${hibernate.hbm2ddl.auto}")
	private String hibernateHbm2ddlAuto;
	
	@Value("${hibernate.dialect}")
	private String hibernateDialect;
	
	@Value("${hibernate.show_sql}")
	private String hibernateShow_sql;
	
	@Value("${hibernate.format_sql}")
	private String hibernateFormat_sql;
	
	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();

		/*
		 * //H2 Embedded Data Source
		 * dataSource.setDriverClassName(H2Dialect.class.getName());
		 * dataSource.setUrl("jdbc:h2:h2db");
		 */

		// mySql Data Source
		dataSource.setDriverClassName(driverClassName);
		dataSource.setUrl(dbUrl);
		dataSource.setUsername(dbUsername);
		dataSource.setPassword(dbPpassword);
		return dataSource;
	}

	@Bean
	public JpaVendorAdapter jpaVendorAdapter() {
		HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
		jpaVendorAdapter.setGenerateDdl(true);
		jpaVendorAdapter.setShowSql(true);

		/*
		 * H2 Embedded
		 * jpaVendorAdapter.setDatabasePlatform("org.hibernate.dialect.H2Dialect"
		 * ); jpaVendorAdapter.setDatabase(Database.H2);
		 */

		jpaVendorAdapter.setDatabasePlatform(dbPlatform);
		jpaVendorAdapter.setDatabase(Database.MYSQL);
		return jpaVendorAdapter;
	}

	private Properties getJpaProperties() {
		return new Properties() {
			/**
* 
*/
			private static final long serialVersionUID = 1L;
			{
				setProperty("hibernate.hbm2ddl.auto", hibernateHbm2ddlAuto);
				// H2
				// setProperty("hibernate.dialect",
				// "org.hibernate.dialect.MySQL5Dialect");
				setProperty("hibernate.dialect", hibernateDialect);
				setProperty("hibernate.show_sql", hibernateShow_sql);
				setProperty("hibernate.format_sql", hibernateFormat_sql);
			}
		};
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource, JpaVendorAdapter jpaVendorAdapter) {
		LocalContainerEntityManagerFactoryBean lef = new LocalContainerEntityManagerFactoryBean();
		lef.setDataSource(dataSource);
		lef.setJpaVendorAdapter(jpaVendorAdapter);
		lef.setJpaProperties(getJpaProperties());
		lef.setPackagesToScan("com.toro");
		return lef;
	}

}
