package com.toro.dms.configs;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@Configuration
@EnableWebMvcSecurity
@PropertySource(value = "classpath:users.properties", ignoreResourceNotFound = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	final static Logger logger = LoggerFactory.getLogger(SecurityConfiguration.class);

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("*/bower_components/").permitAll().anyRequest().authenticated();
		http.logout();
		http.httpBasic();
//		http.formLogin();
		http.sessionManagement().sessionFixation().none();
		
//		http.antMatcher("/").anonymous();
	}

	@Configuration
	protected static class AuthenticationConfiguration extends GlobalAuthenticationConfigurerAdapter {

		@Value("${users:super&mario}")
		public String[] props;

		@Override
		public void init(AuthenticationManagerBuilder auth) throws Exception {
			InMemoryUserDetailsService uds = new InMemoryUserDetailsService();
			uds.postConstruct();

			auth.userDetailsService(uds);
		}

		class InMemoryUserDetailsService implements UserDetailsService {

			private Map<String, String[]> users = new HashMap<String, String[]>();

			private Collection<GrantedAuthority> authorities;

			private Logger logger = LoggerFactory.getLogger(getClass());

			public void postConstruct() {
				authorities = new Vector<GrantedAuthority>();
				authorities.add(new SimpleGrantedAuthority("USER"));

				for (String p : props) {

					logger.debug("adding user:" + p);
					String[] d = p.split("&");
					logger.debug("adding user:" + d[0]);
					logger.debug("adding user:" + d[1]);
					users.put(d[0],d);
				}
				
				logger.debug("map size:" + users.size());
			}

			@Override
			public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
				
				String[] u = users.get(username);
			
				return new User(u[0], u[1], authorities);
			}

		}

	}

}
