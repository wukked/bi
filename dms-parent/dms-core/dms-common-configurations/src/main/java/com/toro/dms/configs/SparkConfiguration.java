package com.toro.dms.configs;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.toro.dms.utils.LogFileUtil;



@Configuration
@PropertySource(value = "classpath:spark.properties")
public class SparkConfiguration {
	
	@Value("${app.name}")
	private String appName;
	
	@Value("${master}")
	private String master;
	

	@Value(value = "${event.log.location:}")
	private String path;
	
	@Bean
	public JavaSparkContext sparkContext() {

		return new JavaSparkContext(new SparkConf().setAppName(appName).setMaster(master));
	}
	
	@Bean
	public LogFileUtil logFileUtil() {
		return new LogFileUtil(path);
	}
	
	
	
}
