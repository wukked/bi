package com.toro.dms.constants;

public enum LogIndex {
	ACTION(0), FROM(1), TO(2), START_TIME(3), END_TIME(4), SIR(5), ICCID(6), MSISDN(7), OBJECT_ID(8), MESSAGE_ID(9), 
	FUNCTION_CALL_ID(10), PARENT_MESSAGE_ID(11), STATUS(12), ERROR_MESSAGE(99), EXTRA_INFO(13);
	
	public final int value;

	private LogIndex(int value) {
		this.value = value;
	}

}
