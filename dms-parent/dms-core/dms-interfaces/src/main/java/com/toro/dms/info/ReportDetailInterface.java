package com.toro.dms.info;

public interface ReportDetailInterface {
	
	public String getName();
	
	public String getLink();

}
