package com.toro.dms.jobs;

import java.util.Calendar;

public interface Job {

	public void executeWithCalDate(Calendar calDate);

}
