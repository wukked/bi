package com.toro.dms;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import com.toro.dms.model.SchedulerFailedDate;

import com.toro.dms.repository.SchedulerRepository;
import com.toro.dms.utils.CalendarUtil;
import com.toro.dms.utils.LogFileUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.toro.dms.jobs.Job;

@Component
@PropertySource(value = "classpath:spark.properties")
public class Scheduler {

    private static final Logger logger = LoggerFactory.getLogger(Scheduler.class);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    private static String executedResponse;

    @Inject
    private List<Job> jobs;

    @Inject
    private SchedulerRepository schedulerRepository;

    @Value(value = "${event.log.location:}")
    private String eventsPath;

    @Inject
    private LogFileUtil logFileUtil;

    //fixed time at 3am
    @Scheduled(cron = "0 0 3 * * *")
    public void runScheduler(){
        scheduledTaskExecuteJobs();
    }

    public String scheduledTaskExecuteJobs() {
        this.executedResponse = "";
        logger.info("The time is now " + dateFormat.format(new Date()));

        //Set initial start date if empty db
        //Represents last failed date
        if (schedulerRepository.count() == 0) {
            //Hard coded date if no date exists
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.MONTH, 10);
            cal.set(Calendar.DAY_OF_MONTH, 10);
            cal.set(Calendar.YEAR, 2014);

            SchedulerFailedDate firstEntry = new SchedulerFailedDate();
            firstEntry.setUpdatedDate(cal);
            schedulerRepository.save(firstEntry);
        }

        long smId = schedulerRepository.getLastId();
        SchedulerFailedDate sm = schedulerRepository.findById(smId);

        Calendar reportDate = Calendar.getInstance();
        reportDate.setTime(sm.getUpdatedDate().getTime());

        //the log files names represent generation date and not content log
        //log "user.log.20141129.csv" holds 20141128 log data, therefore -1 from date

        while (checkNextFileExists(reportDate)) {
            logger.debug("do jobs for report " + CalendarUtil.format(reportDate));

            try {
                for (Job j : jobs) {
                    j.executeWithCalDate(reportDate);
                }
                this.executedResponse += "OK report " + CalendarUtil.format(reportDate) + "\r\n";
            } catch (Exception ex) {
                this.executedResponse += "ERROR report " + ex.getMessage() + CalendarUtil.format(reportDate) + "\r\n";
            }

            reportDate.add(Calendar.DAY_OF_MONTH, 1);
        }
        return this.executedResponse;
    }

    public boolean checkNextFileExists(Calendar calDate) {

        String filePath = logFileUtil.getLogFileName(CalendarUtil.format(CalendarUtil.getDatePlus1(calDate)));
        File file = new File(filePath);
        this.executedResponse += "CHECK " + file.getAbsolutePath() + "\r\n";

        boolean exists;
        if (file.exists() && !file.isDirectory()) {
            logger.debug("File exists " + file.getAbsolutePath());
            this.executedResponse += "FOUND " + "can read " + file.canRead() + " " + file.getAbsolutePath() + "\r\n";
            exists = true;
        } else {
            logger.debug("File does NOT exist " + file.getAbsolutePath());
            this.executedResponse += "MISSING " + file.getAbsolutePath() + "\r\n";
            exists = false;
            SchedulerFailedDate sm = new SchedulerFailedDate();

            SchedulerFailedDate status = schedulerRepository.findByUpdatedDate(calDate);

            if (!(status instanceof SchedulerFailedDate)) {
                logger.debug("Insert entry to SchedulerFailedDate db.");
                this.executedResponse += "NEW_LAST_DATE " + CalendarUtil.format(calDate) + "\r\n";
                sm.setUpdatedDate(calDate);
                schedulerRepository.save(sm);
            } else {
                logger.debug("Failed entry exists in db." + file.getAbsolutePath());
                this.executedResponse += "ERROR " + file.getAbsolutePath() + "\r\n";
            }
        }

        return exists;
    }

}
