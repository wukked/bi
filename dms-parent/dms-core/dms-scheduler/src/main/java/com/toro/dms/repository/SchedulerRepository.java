package com.toro.dms.repository;

import com.toro.dms.model.SchedulerFailedDate;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Calendar;

public interface SchedulerRepository extends CrudRepository<SchedulerFailedDate, String> {

    @Query("SELECT max(sm.id) FROM SchedulerFailedDate sm")
    long getLastId();

    SchedulerFailedDate findById(long id);

    SchedulerFailedDate findByUpdatedDate(Calendar calDate);

}
