scripts = {
  lib: {
    dev: [
      'lodash/dist/lodash.js'
      'jquery/dist/jquery.js'
      'angular/angular.js'
      'json3/lib/json3.js'
      'bootstrap/dist/js/bootstrap.js'
      'angular-resource/angular-resource.js'
      'angular-cookies/angular-cookies.js'
      'angular-sanitize/angular-sanitize.js'
      'angular-animate/angular-animate.js'
      'angular-touch/angular-touch.js'
      'angular-route/angular-route.js'
      'angular-animate/angular-animate.js'
      'ngprogress/build/ngProgress.js'
    ]

    prod: [ # get minified js files
      'lodash/dist/lodash.min.js'
      'jquery/dist/jquery.min.js'
      'angular/angular.min.js'
      'json3/lib/json3.min.js'
      'bootstrap/dist/js/bootstrap.min.js'
      'angular-resource/angular-resource.min.js'
      'angular-cookies/angular-cookies.min.js'
      'angular-sanitize/angular-sanitize.min.js'
      'angular-animate/angular-animate.min.js'
      'angular-touch/angular-touch.min.js'
      'angular-route/angular-route.min.js'
      'angular-animate/angular-animate.min.js'
      'ngprogress/build/ngProgress.min.js'
    ]
  }

  app: [
    # initializer
    "app.js"
    "services.js"
    "controllers/main.js"
  ]

  karma: {
    unit: [
      'lib/lodash/dist/lodash.js'
      'lib/jquery/jquery.js'
      # 'lib/moment/moment.js'
      'lib/bootstrap/dist/js/bootstrap.js'
      'lib/handlebars/handlebars.js'
      'lib/ember/ember.js'
      'lib/ember-data/ember-data.js'
      'lib/Faker/Faker.js'
      'lib/gsap/src/uncompressed/TweenMax.js'
      'js/app.js'
      'test/unit/*.js'
    ]
    integration: [
      'lib/lodash/dist/lodash.js'
      'lib/jquery/jquery.js'
      # 'lib/moment/moment.js'
      'lib/bootstrap/dist/js/bootstrap.js'
      'lib/handlebars/handlebars.js'
      'lib/ember/ember.js'
      'lib/ember-data/ember-data.js'
      'lib/Faker/Faker.js'
      'lib/gsap/src/uncompressed/TweenMax.js'
      'js/app.js'
      'lib/ember-mocha-adapter/adapter.js'
      'test/integration/*.js'
    ]
  }
}

gmapKey = "AIzaSyDjcpOfZNZ3pKxRE8UCVBDK0Yv6xNrJu0Q"

# web spider variables
keywords = ""

description = ""

module.exports = (grunt)->

  # Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    less:
      dev:
        options:
          sourceMap: true
          sourceMapFilename: 'less/app.map'
          sourceMapURL: '../less/app.map'
          sourceMapRootpath: '../'
        files:
          "css/app.css": "less/app.less"
      prod:
        options:
          compress: true
          cleancss: true
        files:
          "css/app.min.css": "less/app.less"

    coffee:
      default:
        files: [
          expand: true
          cwd: './'
          src: ['src/**/*.coffee', 'server/**/*.coffee', 'test/**/*.coffee']
          ext: '.js'
        ]

    'compile-handlebars':
      dev:
        template: './index.html.hbs'
        templateData:
          gmapKey: gmapKey
          production: false
          appCss: 'css/app.css'
          libScripts: scripts.lib.dev
          appScript: "app.js"
          keywords: keywords
          description: description
        output: 'index.html'

      production:
        template: './index.html.hbs'
        templateData:
          gmapKey: gmapKey
          production: true
          appCss: 'css/app.min.css'
          libScripts: scripts.lib.prod
          appScript: "app.min.js"
          keywords: keywords
          description: description
        output: 'index.html'

    exec:
      server:
        cmd: 'node server/server.js'

    concat:
      app:
        src: scripts.app.map (script)->
          return "scripts/#{script}"
        dest: 'js/app.js'

    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: 'js/app.js',
        dest: 'js/app.min.js'
      }
    }

    karma: {
      options: {
        browsers: ['PhantomJS']
        frameworks: ['mocha', 'chai']
        reporters: ['mocha']
        singleRun: true
      },
      unit: {
        options: {
          files: scripts.karma.unit
        }
      },
      integration: {
        options: {
          files: scripts.karma.integration
        }
      }
    }

    watch: {
      options: {
        livereload: true
      }
      src: {
        files: ['src/**/*.coffee']
        tasks: ['coffee', 'concat:app']
      }
      less: {
        files: ['less/*.less']
        tasks: ['less:dev']
      }
      hbs: {
        files: ['src/hbs/*.hbs']
        tasks: ['emberTemplates', 'concat:app', 'compile-handlebars:dev']
      }
    }

    copy: {
      default: {
        src: ["index.html", "js/*", "css/*.css", "bower_components/**/*", "images/**/*", "fonts/**/*", "views/**/*"]
        dest: "../dms-ui/src/main/resources/public/"
      }
    }

    clean: ["index.html"]
  })

  # Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify')
  grunt.loadNpmTasks('grunt-contrib-coffee')
  grunt.loadNpmTasks('grunt-exec')
  grunt.loadNpmTasks('grunt-contrib-concat')
  grunt.loadNpmTasks('grunt-compile-handlebars')
  grunt.loadNpmTasks('grunt-contrib-less')
  grunt.loadNpmTasks('grunt-contrib-copy')
  grunt.loadNpmTasks('grunt-contrib-clean')

  # Get the task
  isProd = false
  grunt.cli.tasks.forEach((task)->
    if task is "prod"
      isProd = true
  )
  grunt.log.write("Production mode: #{isProd}")
  # Load dev dependencies only when dev mode
  unless isProd
    grunt.loadNpmTasks('grunt-karma')
    grunt.loadNpmTasks('grunt-contrib-watch')

  # Default task(s).
  grunt.registerTask('default', [
    'less:dev',
    'concat:app',
    'compile-handlebars:dev',
    'copy:default'
  ])

  # grunt.registerTask('server', ['exec:server'])
  grunt.registerTask('dev', [
    'less:dev',
    'concat:app',
    'compile-handlebars:dev',
    'copy:default'
  ])
  grunt.registerTask('prod', [
    'less:prod',
    'concat:app',
    'uglify',
    'compile-handlebars:production',
    'copy:default',
    'clean'
  ])

  # grunt.registerTask('test', ['dev', 'karma'])

