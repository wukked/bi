HOW TO RUN
----------

From scratch
(Please, run cmd in Windows as an Administrator - right click on icon ... Open as Administrator)

> 1. Remove folders /bower_components and /node_modules.
> 2. npm install.
> 3. bower install.
> 4. grunt dev.

UI is ready to be used


PLUGIN HTML
-----------

Please refer to the HTML located at:
> dms-plugins/dms-ec-report/src/main/resources/META-INF/report-ui/ec-report.html

as an example. Just modify the hardcoded names and links of the ul(navbar-nav class)>li list.
