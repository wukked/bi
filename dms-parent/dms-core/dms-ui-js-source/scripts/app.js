'use strict';

/**
 * @ngdoc overview
 * @name widgetstoreApp
 * @description
 * # widgetstoreApp
 *
 * Main module of the application.
 */
var app = angular
  .module('bdmsApp', [
    'ngProgress',
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'bdmsServices'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

/*
 * Define global variables
 */
app.run(["$rootScope", "ngProgress", "Api", function ($rootScope, ngProgress, Api) {
  $rootScope.reports = Api.get(function(data) {
    if(0 < data.length) {
      data[0].active = true;
    }
  });
  $rootScope.pageLoaded = true;
  $rootScope.loadReport = function (report) {
    if($rootScope.pageLoaded) {
      ngProgress.start();
    }
    $rootScope.pageLoaded = false;
    $("#report").load( report.link, function (response, status, xhr) {
      if(status == "error") {
        var msg = "Sorry but there was an error loading the report: ";
        alert(msg + xhr.status + " " + xhr.statusText);
      }
      else {
        _.forEach($rootScope.reports, function(rep){
          if(rep.name == report.name) {
            rep.active = true;
          }
          else {
            rep.active = false;
          }
        });
        $rootScope.$apply();
      }
      ngProgress.complete();
      $rootScope.pageLoaded = true;
    });
  };
}]);
