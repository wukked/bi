'use strict';

/**
 * @ngdoc function
 * @name widgetstoreApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the widgetstoreApp
 */
angular.module('bdmsApp')
  .controller('MainCtrl', ["$scope", "$rootScope", function ($scope, $rootScope) {
    angular.element(document).ready(function () {
      var doc = $(this);
      setTimeout(function(){
        if(0 < doc.find("li").length) {
          $(doc.find("li")[0]).find("a").click();
        }
      }, 1000);
    });
  }]);
