'use strict';

var bdmsServices = angular.module('bdmsServices', ['ngResource']);

bdmsServices.factory('Api', ['$resource',
  function($resource, $rootScope) {
    var host = '';
    var services = 'navigation';
    var rootURL = host + services;
    return $resource(rootURL + '/reports', {}, {
      get: { method:'GET', isArray: true }
    });
  }
]);
