package com.toro.dms.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.Calendar;

public class CalendarUtil {

    public static String createYearFormat(Calendar calDate) {
        Calendar now = Calendar.getInstance();
        now.setTime(calDate.getTime());

        return Integer.toString(now.get(Calendar.YEAR));
    }

    public static String createYearMonthFormat(Calendar calDate) {
        Calendar now = Calendar.getInstance();
        now.setTime(calDate.getTime());

        return Integer.toString(now.get(Calendar.YEAR)) + "-" + Integer.toString(now.get(Calendar.MONTH) + 1);
    }

    public static String createYearWeekFormat(Calendar calDate) {
        Calendar now = Calendar.getInstance();
        now.setTime(calDate.getTime());
        now.setFirstDayOfWeek(Calendar.MONDAY);

        return Integer.toString(now.get(Calendar.YEAR)) + "-" + Integer.toString(now.get(Calendar.WEEK_OF_YEAR));
    }

    public static Calendar getStartDateOfYear(Calendar calDate) {
        Calendar now = Calendar.getInstance();
        now.setTime(calDate.getTime());
        now.set(Calendar.MONTH, 0);
        now.set(Calendar.DAY_OF_MONTH, 1);

        return now;
    }

    public static Calendar getEndDateOfYear(Calendar calDate) {
        Calendar now = Calendar.getInstance();
        now.setTime(calDate.getTime());
        now.set(Calendar.MONTH, 11);
        now.set(Calendar.DAY_OF_MONTH, 31);

        return now;
    }

    public static Calendar getStartDateOfMonth(Calendar calDate) {
        Calendar now = Calendar.getInstance();
        now.setTime(calDate.getTime());
        now.set(Calendar.DAY_OF_MONTH, 1);

        return now;
    }

    public static Calendar getEndDateOfMonth(Calendar calDate) {
        Calendar now = Calendar.getInstance();
        now.setTime(calDate.getTime());
        now.set(Calendar.DATE, now.getActualMaximum(Calendar.DATE));

        return now;
    }

    public static Calendar getStartDateOfWeek(Calendar calDate) {
        Calendar now = Calendar.getInstance();
        now.setTime(calDate.getTime());
        now.setFirstDayOfWeek(Calendar.MONDAY);
        now.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        return now;
    }

    public static Calendar getEndDateOfWeek(Calendar calDate) {
        Calendar now = Calendar.getInstance();
        now.setTime(calDate.getTime());
        now.setFirstDayOfWeek(Calendar.MONDAY);
        now.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        now.add(Calendar.DATE, 6);

        return now;
    }

    public static String getYesterday() {
        Calendar date = Calendar.getInstance();
        date.add(Calendar.DAY_OF_MONTH, -1);
        return CalendarUtil.format(date);
    }

    public static Calendar getYesterday1() {
        Calendar date = Calendar.getInstance();
        date.add(Calendar.DAY_OF_MONTH, -1);
        return date;
    }

    public static String getToday() {
        Calendar date = Calendar.getInstance();
        return CalendarUtil.format(date);
    }

    public static Calendar getDatePlus1(Calendar calDate) {
        Calendar date = Calendar.getInstance();
        date.setTime(calDate.getTime());
        date.add(Calendar.DAY_OF_MONTH, 1);
        return date;
    }

    public static Calendar getDateMinus30(Calendar calDate) {
        Calendar date = Calendar.getInstance();
        date.add(Calendar.DAY_OF_MONTH, -30);
        return date;
    }

    public static int getDayOfWeek(String date) {
        Calendar calDate = Calendar.getInstance();
        calDate.set(Calendar.YEAR, Integer.parseInt(date.substring(0, 4)));
        calDate.set(Calendar.MONTH, Integer.parseInt(date.substring(5, 7)) - 1);
        calDate.set(Calendar.DATE, Integer.parseInt(date.substring(8, 10)));
        return calDate.get(Calendar.DAY_OF_WEEK);
    }

    public static String format(Calendar date) {

        int y = date.get(Calendar.YEAR);

        String m = StringUtils.leftPad(
                new Integer(date.get(Calendar.MONTH) + 1).toString(), 2, "0");
        String d = StringUtils
                .leftPad(
                        new Integer(date.get(Calendar.DAY_OF_MONTH)).toString(),
                        2, "0");

        String t = y + "-" + m + "-" + d;

        return t;
    }

}
