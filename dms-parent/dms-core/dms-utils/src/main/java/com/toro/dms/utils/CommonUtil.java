package com.toro.dms.utils;

public class CommonUtil {

    public static long[] addLongArrays(long[] a, long[] b){
        long[] newArray = new long[a.length];
        for(int i=0; i<a.length  ;i++){
            newArray[i] = a[i] + b[i];
        }

        return newArray;
    }

    public static String percentageFormat(Double percentage){
        if( percentage.isNaN() ){
            percentage = 0.0;
        }

        return String.format( "%.2f", 100*percentage );
    }
}
