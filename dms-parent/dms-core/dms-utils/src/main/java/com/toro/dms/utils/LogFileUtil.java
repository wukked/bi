package com.toro.dms.utils;

public class LogFileUtil {

	public LogFileUtil(String path) {
		super();
		this.path = path;
	}

	private String path;
	
	public String getLogFileName(String date){
		date = date.replace("-","");
		String file = path + "user.log." + date + ".csv";
		return file;
	}

}
