package com.toro.dms.rest;

import java.util.List;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.toro.dms.info.ReportDetailInterface;

@RequestMapping("/navigation/reports")
@RestController
public class NavigationController {
	
	@Inject
	List<ReportDetailInterface> reportDetails;

	@RequestMapping(method = RequestMethod.GET)
	public List<ReportDetailInterface> reports() {
		return reportDetails;
	}

}
