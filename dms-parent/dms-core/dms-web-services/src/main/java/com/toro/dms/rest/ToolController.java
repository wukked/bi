package com.toro.dms.rest;

import com.toro.dms.Scheduler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RequestMapping("/tools")
@RestController
public class ToolController {
	
	@Inject
	Scheduler scheduler;

	@RequestMapping(method = RequestMethod.GET, value="/forceRunScheduler")
	public String forceRunScheduler() {
		return scheduler.scheduledTaskExecuteJobs();
	}

}
