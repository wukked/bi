package com.toro.dms.extractors;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Map;

import javax.inject.Inject;

import org.apache.spark.api.java.JavaSparkContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.google.common.collect.Iterables;
import com.toro.dms.constants.LogIndex;
import com.toro.dms.model.DeployServiceData;
import com.toro.dms.utils.CalendarUtil;
import com.toro.dms.utils.LogFileUtil;

@Component
public class DeployServiceDataExtractor implements Serializable {

	Logger logger = LoggerFactory.getLogger(getClass());

	private static final long serialVersionUID = -8940981437234160783L;
	@Inject
	private JavaSparkContext sc;
	
	@Inject
	private LogFileUtil logFileUtil;

	public Map<String, Iterable<DeployServiceData>> extract(Calendar date) {


		String path = logFileUtil.getLogFileName(CalendarUtil.format(CalendarUtil.getDatePlus1(date)));
				
		Map<String, Iterable<DeployServiceData>> result =  sc.textFile(path).filter(l -> (l.startsWith("HandleEndServiceStateChangeNotification;WM;")&& l.contains("operation=1")))
		.map(l -> l.split(";"))
		.sortBy(l -> l[LogIndex.END_TIME.value], true, 1)
		.groupBy(l -> l[LogIndex.SIR.value])
		.map(r -> Iterables.getLast(r._2()) )
		.map(l ->new DeployServiceData(l[LogIndex.SIR.value], l[LogIndex.EXTRA_INFO.value].substring(20), date))
		.groupBy(l -> l.getExecutedStatus()).collectAsMap();

		return result;

	}

}
