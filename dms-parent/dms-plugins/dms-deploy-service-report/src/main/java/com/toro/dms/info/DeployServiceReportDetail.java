package com.toro.dms.info;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(401)
public class DeployServiceReportDetail implements ReportDetailInterface {

	@Override
	public String getName() {
		return "Installation Analysis";
	}

	@Override
	public String getLink() {
		return "report-ui/deploy-service-report.html";
	}

}
