package com.toro.dms.jobs;

import com.google.common.collect.Iterables;
import com.toro.dms.extractors.DeployServiceDataExtractor;
import com.toro.dms.model.*;
import com.toro.dms.repository.*;
import com.toro.dms.transformers.DeployServiceNumberTransformer;
import com.toro.dms.utils.CalendarUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Calendar;
import java.util.Map;
import java.util.Vector;

@Component
//Run after EC, reliance on EC data
@Order(200)
public class DeployServiceJob implements Job {

	@Inject
	private DeployServiceDataExtractor deployServiceDataExtractor;

	@Inject
	private DeployServiceDataRepository deployServiceDataRepository;

	@Inject
	private DeployServiceNumberRepository deployServiceNumberRepository;

	@Inject
	private DeployServiceNumberTransformer deployServiceNumberTransformer;

	@Inject
	private DeployServiceMetricsWeekRepository deployServiceMetricsWeekRepository;

	@Inject
	private DeployServiceMetricsMonthsRepository deployServiceMetricsMonthsRepository;

	@Inject
	private DeployServiceMetricsMonthsSnapshotRepository deployServiceMetricsMonthsSnapshotRepository;

	@Inject
	private DeployServiceMetricsYearsRepository deployServiceMetricsYearsRepository;


	private final Iterable<DeployServiceData> empty = new Vector<DeployServiceData>();

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void executeWithCalDate(Calendar calDate) {

		logger.debug("do deploy service extractor" + calDate.getTime().toString());

		Map<String, Iterable<DeployServiceData>> dsResult = deployServiceDataExtractor.extract(calDate);

		logger.debug("extractors finished");

		logger.debug("saving DeployServiceData");

		dsResult.values().forEach(l -> deployServiceDataRepository.save(l));

		DeployServiceNumber deployServiceNumber = new DeployServiceNumber(calDate, Iterables.size(dsResult.getOrDefault("EXECUTED_SUCCESS", empty)), Iterables.size(dsResult.getOrDefault("FAILED", empty)));

		logger.debug("Get mapped DeployServiceNumber, now save them");

		if (logger.isDebugEnabled()) {
			logger.debug(deployServiceNumber.getDate().getTime() + "  " + deployServiceNumber.getSuccess() + "  " + deployServiceNumber.getFailed());
		}

		deployServiceNumberRepository.save(deployServiceNumber);

		logger.debug("do transform deploy service metrics transformations");

		DeployServiceMetricsWeek deployServiceMetricsWeek = deployServiceNumberTransformer.transformWeek(calDate);
		DeployServiceMetricsMonth deployServiceMetricsMonth = deployServiceNumberTransformer.transformMonth(calDate);
		DeployServiceMetricsYear deployServiceMetricsYear = deployServiceNumberTransformer.transformYear(calDate);
		DeployServiceMetricsMonthSnapshot deployServiceMetricsMonthSnapshot = deployServiceNumberTransformer.transformMonthSnapshot(calDate);

		logger.debug("DeployServiceMetrics transformed, now save them");

		deployServiceMetricsWeekRepository.save(deployServiceMetricsWeek);
		deployServiceMetricsMonthsRepository.save(deployServiceMetricsMonth);
		deployServiceMetricsYearsRepository.save(deployServiceMetricsYear);
		deployServiceMetricsMonthsSnapshotRepository.save(deployServiceMetricsMonthSnapshot);

	}


}