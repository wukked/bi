package com.toro.dms.model;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.*;

@Entity
public class DeployServiceData implements Serializable {

	private static final long serialVersionUID = -4873353309972877707L;
	@Id
	@Column(unique = true)
	private String sir;
	private String executedStatus;
	@Temporal(TemporalType.DATE)
	private Calendar updateDate;

	public DeployServiceData() {
	}

	public DeployServiceData(String sir, String executedStatus, Calendar updateDate) {
		this.sir = sir;
		this.executedStatus = executedStatus;
		this.updateDate = updateDate;
	}

	public String getSir() {
		return sir;
	}

	public void setSir(String sir) {
		this.sir = sir;
	}

	public String getExecutedStatus() {
		return this.executedStatus;
	}

	public void setExecutedStatus(String executedStatus) {
		this.executedStatus = executedStatus;
	}

	public Calendar getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Calendar updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	public String toString() {
		return "DeployServiceData [sir=" + sir + ", executedStatus=" + executedStatus + ", updateDate=" + updateDate + "]";
	}


}
