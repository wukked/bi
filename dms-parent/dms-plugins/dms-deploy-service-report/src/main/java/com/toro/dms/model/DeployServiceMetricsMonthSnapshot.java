package com.toro.dms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class DeployServiceMetricsMonthSnapshot extends DeployServiceType implements Serializable {

    private static final long serialVersionUID = -4873332119972123321L;
    @Id
    @Column(unique = true)
    private String yearMonth;

    public DeployServiceMetricsMonthSnapshot() {
        super();
    }

    public DeployServiceMetricsMonthSnapshot(String yearMonth) {
        super();
        this.yearMonth = yearMonth;
    }

    public DeployServiceMetricsMonthSnapshot(String yearMonth, int success, int failed) {
        super(success, failed);
        this.yearMonth = yearMonth;
    }

    public String getYearMonth() {
        return yearMonth;
    }

    public void setYearMonth(String yearMonth) {
        this.yearMonth = yearMonth;
    }

}
