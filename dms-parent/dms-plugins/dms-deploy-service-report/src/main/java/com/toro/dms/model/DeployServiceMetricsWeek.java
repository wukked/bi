package com.toro.dms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class DeployServiceMetricsWeek extends DeployServiceType implements Serializable {

    private static final long serialVersionUID = -482223306662877227L;
    @Id
    @Column(unique = true)
    private String yearWeek;

    public DeployServiceMetricsWeek() {
        super();
    }

    public DeployServiceMetricsWeek(String yearWeek) {
        super();
        this.yearWeek = yearWeek;
    }

    public DeployServiceMetricsWeek(String yearWeek, int success, int failed) {
        super(success, failed);
        this.yearWeek = yearWeek;
    }

    public String getYearWeek() {
        return yearWeek;
    }

    public void setYearWeek(String yearWeek) {
        this.yearWeek = yearWeek;
    }

}
