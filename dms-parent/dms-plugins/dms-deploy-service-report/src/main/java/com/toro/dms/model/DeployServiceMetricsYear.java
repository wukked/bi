package com.toro.dms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class DeployServiceMetricsYear extends DeployServiceType implements Serializable {

    private static final long serialVersionUID = -4873353309972877707L;
    @Id
    @Column(unique = true)
    private String year;

    public DeployServiceMetricsYear() {
        super();
    }

    public DeployServiceMetricsYear(String yearMonth) {
        super();
        this.year = yearMonth;
    }

    public DeployServiceMetricsYear(String yearMonth, int success, int failed) {
        super(success, failed);
        this.year = yearMonth;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

}
