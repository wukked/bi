package com.toro.dms.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;

@Entity
public class DeployServiceNumber extends DeployServiceType implements Serializable {

    private static final long serialVersionUID = -4872353309232877127L;
    @Id
    @Temporal(TemporalType.DATE)
    @Column(unique = true)
    private Calendar date;

    public DeployServiceNumber() {
        super();
    }

    public DeployServiceNumber(Calendar date, int success, int failed) {
        super(success, failed);
        this.date = date;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

}
