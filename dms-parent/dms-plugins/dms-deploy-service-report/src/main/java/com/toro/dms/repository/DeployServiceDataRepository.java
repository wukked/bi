package com.toro.dms.repository;

import com.toro.dms.model.DeployServiceData;
import org.springframework.data.repository.CrudRepository;

import java.util.Calendar;
import java.util.List;

public interface DeployServiceDataRepository extends CrudRepository<DeployServiceData, String> {

    public List<DeployServiceData> findByUpdateDate(Calendar updateDate);
    public List<DeployServiceData> findByUpdateDateBetweenAndExecutedStatusEquals(Calendar start, Calendar end, String executedStatus);

}
