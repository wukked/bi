package com.toro.dms.repository;

import com.toro.dms.model.DeployServiceMetricsMonth;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DeployServiceMetricsMonthsRepository extends CrudRepository<DeployServiceMetricsMonth, Long> {

    public List<DeployServiceMetricsMonth> findByYearMonth(String yearMonth);

}
