package com.toro.dms.repository;

import com.toro.dms.model.DeployServiceMetricsMonthSnapshot;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DeployServiceMetricsMonthsSnapshotRepository extends CrudRepository<DeployServiceMetricsMonthSnapshot, Long> {

    public List<DeployServiceMetricsMonthSnapshot> findByYearMonth(String yearMonth);

}
