package com.toro.dms.repository;

import com.toro.dms.model.DeployServiceMetricsWeek;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface DeployServiceMetricsWeekRepository extends CrudRepository<DeployServiceMetricsWeek, Long> {

    public List<DeployServiceMetricsWeek> findByYearWeek(String yearWeek);

}
