package com.toro.dms.repository;

import com.toro.dms.model.DeployServiceMetricsYear;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface DeployServiceMetricsYearsRepository extends CrudRepository<DeployServiceMetricsYear, Long> {

    public List<DeployServiceMetricsYear> findByYear(String year);

}
