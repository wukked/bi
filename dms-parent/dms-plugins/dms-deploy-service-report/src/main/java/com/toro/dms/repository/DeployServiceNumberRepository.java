package com.toro.dms.repository;

import com.toro.dms.model.DeployServiceNumber;
import org.springframework.data.repository.CrudRepository;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public interface DeployServiceNumberRepository extends CrudRepository<DeployServiceNumber, String> {

    public List<DeployServiceNumber> findByDate(Calendar date);
    public List<DeployServiceNumber> findByDateBetween(Calendar start, Calendar end);

}
