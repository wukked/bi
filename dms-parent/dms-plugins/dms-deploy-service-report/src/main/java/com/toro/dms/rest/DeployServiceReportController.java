package com.toro.dms.rest;

import java.util.Calendar;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import com.toro.dms.utils.CommonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.toro.dms.model.DeployServiceMetricsMonth;
import com.toro.dms.model.DeployServiceMetricsMonthSnapshot;
import com.toro.dms.model.DeployServiceMetricsWeek;
import com.toro.dms.model.DeployServiceMetricsYear;
import com.toro.dms.model.DeployServiceNumber;
import com.toro.dms.model.DeployServiceType;
import com.toro.dms.model.EcMetricsMonth;
import com.toro.dms.model.EcMetricsMonthSnapshot;
import com.toro.dms.model.EcMetricsWeek;
import com.toro.dms.model.EcMetricsYear;
import com.toro.dms.model.EcNumber;
import com.toro.dms.repository.DeployServiceMetricsMonthsRepository;
import com.toro.dms.repository.DeployServiceMetricsMonthsSnapshotRepository;
import com.toro.dms.repository.DeployServiceMetricsWeekRepository;
import com.toro.dms.repository.DeployServiceMetricsYearsRepository;
import com.toro.dms.repository.DeployServiceNumberRepository;
import com.toro.dms.repository.EcMetricsMonthsRepository;
import com.toro.dms.repository.EcMetricsMonthsSnapshotRepository;
import com.toro.dms.repository.EcMetricsWeekRepository;
import com.toro.dms.repository.EcMetricsYearsRepository;
import com.toro.dms.repository.EcNumberRepository;
import com.toro.dms.utils.CalendarUtil;

@RequestMapping("/deployServiceReport")
@RestController
public class DeployServiceReportController {

    private static final Logger logger = LoggerFactory.getLogger(DeployServiceReportController.class);

    @Inject
    DeployServiceNumberRepository deployServiceNumberRepository;

    @Inject
    DeployServiceMetricsWeekRepository deployServiceMetricsWeekRepository;

    @Inject
    DeployServiceMetricsMonthsRepository deployServiceMetricsMonthsRepository;

    @Inject
    DeployServiceMetricsYearsRepository deployServiceMetricsYearsRepository;

    @Inject
    DeployServiceMetricsMonthsSnapshotRepository deployServiceMetricsMonthsSnapshotRepository;

    @Inject
    EcNumberRepository ecNumberRepository;

    @Inject
    EcMetricsWeekRepository ecMetricsWeekRepository;

    @Inject
    EcMetricsMonthsRepository ecMetricsMonthsRepository;

    @Inject
    EcMetricsMonthsSnapshotRepository ecMetricsMonthsSnapshotRepository;

    @Inject
    EcMetricsYearsRepository ecMetricsYearsRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getYesterday")
    public String getYesterday(HttpServletResponse response) {

        logger.debug("do get yesterday");
        logger.debug("caculated....");

        Calendar yesterday = CalendarUtil.getYesterday1();

        setCsvResponseHeader(response, "DeployService_DayReport.csv");

        Iterable<DeployServiceNumber> rs = deployServiceNumberRepository.findByDate(yesterday);

        String csv = createCsvHeaderTitles();

        for (DeployServiceNumber ds : rs) {
            csv += createCsvRow(ds);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getDay")
    public String getDay(HttpServletResponse response, @RequestParam(required = true) Calendar date) {

        logger.debug("do get date");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "DeployService_DayReport.csv");

        Iterable<DeployServiceNumber> rs = deployServiceNumberRepository.findByDate(date);

        String csv = createCsvHeaderTitles();

        for (DeployServiceNumber ds : rs) {
            csv += createCsvRow(ds);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllDays")
    public String getAllDays(HttpServletResponse response) {

        logger.debug("do get all days");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "DeployService_AllDaysReport.csv");

        Iterable<DeployServiceNumber> rs = deployServiceNumberRepository.findAll();

        String csv = createCsvHeaderTitles();

        for (DeployServiceNumber ec : rs) {
            csv += createCsvRow(ec);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getCurrentWeek")
    public String getCurrentWeek(HttpServletResponse response) {

        logger.debug("do get current week");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "DeployService_WeekReport.csv");

        //get current week/year
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int week = now.get(Calendar.WEEK_OF_YEAR);

        Iterable<DeployServiceMetricsWeek> rs = deployServiceMetricsWeekRepository.findByYearWeek(Integer.toString(year) + "-" + Integer.toString(week));

        String csv = createCsvHeaderTitles();

        for (DeployServiceMetricsWeek ds : rs) {
            csv += createCsvRow(ds);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllWeeks")
    public String getAllWeeks(HttpServletResponse response) {

        logger.debug("do get all weeks");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "DeployService_AllWeekReport.csv");

        Iterable<DeployServiceMetricsWeek> rs = deployServiceMetricsWeekRepository.findAll();

        String csv = createCsvHeaderTitles();

        for (DeployServiceMetricsWeek ds : rs) {
            csv += createCsvRow(ds);
        }
        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getCurrentMonth")
    public String getCurrentMonth(HttpServletResponse response) {

        logger.debug("do get month");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "DeployService_MonthReport.csv");

        //get current week/year
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH) + 1;

        Iterable<DeployServiceMetricsMonth> rs = deployServiceMetricsMonthsRepository.findByYearMonth(Integer.toString(year) + "-" + Integer.toString(month));

        String csv = createCsvHeaderTitles();

        for (DeployServiceMetricsMonth ds : rs) {
            csv += createCsvRow(ds);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllMonths")
    public String getAllMonths(HttpServletResponse response) {

        logger.debug("do get all months");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "DeployService_AllMonthsReport.csv");

        Iterable<DeployServiceMetricsMonth> rs = deployServiceMetricsMonthsRepository.findAll();

        String csv = createCsvHeaderTitles();

        for (DeployServiceMetricsMonth ds : rs) {
            csv += createCsvRow(ds);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getLastMonthSnapshot")
    public String getLastMonthSnapshot(HttpServletResponse response) {

        logger.debug("do get month snapshot");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "DeployService_LastMonthSnapshotReport.csv");

        //get current week/year
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH) + 1;

        Iterable<DeployServiceMetricsMonthSnapshot> rs = deployServiceMetricsMonthsSnapshotRepository.findByYearMonth(Integer.toString(year) + "-" + Integer.toString(month));

        String csv = createCsvHeaderTitles();

        for (DeployServiceMetricsMonthSnapshot ds : rs) {
            csv += createCsvRow(ds);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllMonthSnapshots")
    public String getAllMonthSnapshots(HttpServletResponse response) {

        logger.debug("do get all month snapshot");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "DeployService_AllMonthSnapshotReport.csv");

        Iterable<DeployServiceMetricsMonthSnapshot> rs = deployServiceMetricsMonthsSnapshotRepository.findAll();

        String csv = createCsvHeaderTitles();

        for (DeployServiceMetricsMonthSnapshot ds : rs) {
            csv += createCsvRow(ds);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getCurrentYear")
    public String getCurrentYear(HttpServletResponse response) {

        logger.debug("do get current year");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "DeployService_YearReport.csv");

        //get current year
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);

        Iterable<DeployServiceMetricsYear> rs = deployServiceMetricsYearsRepository.findByYear(Integer.toString(year));

        String csv = createCsvHeaderTitles();

        for (DeployServiceMetricsYear ds : rs) {
            csv += createCsvRow(ds);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllYears")
    public String getAllYears(HttpServletResponse response) {

        logger.debug("do get all years");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "DeployService_AllYearReport.csv");

        Iterable<DeployServiceMetricsYear> rs = deployServiceMetricsYearsRepository.findAll();

        String csv = createCsvHeaderTitles();

        for (DeployServiceMetricsYear ds : rs) {
            csv += createCsvRow(ds);
        }

        return csv;
    }

    private void setCsvResponseHeader(HttpServletResponse response, String fileName) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
        response.setContentType("application/x-download");
        response.addHeader("Content-Disposition", "filename=\"" + fileName + "\"");
    }

    private String createCsvHeaderTitles() {
        return "Time,DeployService started,DeployService started %,DeployService failed,DeployService failed %,DeployService succeeded,DeployService succeeded%\r\n";
    }

    private String createCsvRow(DeployServiceType rs) {
        logger.debug("Create CSV row");

        Calendar date;
        String time = "";
        double dsStartedPer = 0;
        double totalEcSucceeded = 0;

        if (rs instanceof DeployServiceNumber) {
            date = ((DeployServiceNumber) rs).getDate();
            time = CalendarUtil.format(((DeployServiceNumber) rs).getDate());

            Iterable<EcNumber> ecNumberResult = ecNumberRepository.findByDate(date);
            for( EcNumber ec:ecNumberResult){
                totalEcSucceeded += ec.getSuccess();
            }
            dsStartedPer = (double)(rs.totalDeployServiceStarted()) / totalEcSucceeded;

        } else if (rs instanceof DeployServiceMetricsWeek) {
            time = ((DeployServiceMetricsWeek) rs).getYearWeek().toString();

            Iterable<EcMetricsWeek> ecMetricsWeekResult = ecMetricsWeekRepository.findByYearWeek(time);
            for( EcMetricsWeek ec:ecMetricsWeekResult){
                totalEcSucceeded += ec.getSuccess();
            }
            dsStartedPer = (double)(rs.totalDeployServiceStarted()) / totalEcSucceeded;

        } else if (rs instanceof DeployServiceMetricsMonth) {
            time = ((DeployServiceMetricsMonth) rs).getYearMonth().toString();

            Iterable<EcMetricsMonth> ecMetricsMonthResult = ecMetricsMonthsRepository.findByYearMonth(time);
            for( EcMetricsMonth ec:ecMetricsMonthResult){
                totalEcSucceeded += ec.getSuccess();
            }
            dsStartedPer = (double)(rs.totalDeployServiceStarted()) / totalEcSucceeded;

        } else if (rs instanceof DeployServiceMetricsYear) {
            time = ((DeployServiceMetricsYear) rs).getYear().toString();

            Iterable<EcMetricsYear> ecMetricsYearResult = ecMetricsYearsRepository.findByYear(time);
            for( EcMetricsYear ec:ecMetricsYearResult){
                totalEcSucceeded += ec.getSuccess();
            }
            dsStartedPer = (double)(rs.totalDeployServiceStarted()) / totalEcSucceeded;

        } else if (rs instanceof DeployServiceMetricsMonthSnapshot) {
            time = ((DeployServiceMetricsMonthSnapshot) rs).getYearMonth().toString();

            Iterable<EcMetricsMonthSnapshot> ecMetricsMonthSnapshotResult = ecMetricsMonthsSnapshotRepository.findByYearMonth(time);
            for( EcMetricsMonthSnapshot ec:ecMetricsMonthSnapshotResult){
                totalEcSucceeded += ec.getSuccess();
            }
            dsStartedPer = (double)(rs.totalDeployServiceStarted()) / totalEcSucceeded;

        } else {
            logger.debug("Error finding result set class type");
        }

        int dsStarted = rs.getSuccess() + rs.getFailed();

        return time + "," +
                dsStarted + "," +
                CommonUtil.percentageFormat(dsStartedPer) + "," +
                rs.getFailed() + "," +
                CommonUtil.percentageFormat(rs.failedPercent()) + "," +
                rs.getSuccess() + "," +
                CommonUtil.percentageFormat(rs.successPercent()) +
                "\r\n";
    }

}