package com.toro.dms.transformers;

import com.toro.dms.model.*;
import com.toro.dms.repository.DeployServiceDataRepository;
import com.toro.dms.repository.DeployServiceNumberRepository;
import com.toro.dms.utils.CalendarUtil;
import org.apache.spark.api.java.JavaSparkContext;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class DeployServiceNumberTransformer {

    @Inject
    private DeployServiceNumberRepository deployServiceNumberRepository;

    @Inject
    private DeployServiceDataRepository deployServiceDataRepository;

    @Inject
    private JavaSparkContext sc;

    public DeployServiceMetricsWeek transformWeek(Calendar calDate) {

        DeployServiceMetricsWeek deployServiceMetricsWeek = new DeployServiceMetricsWeek(CalendarUtil.createYearWeekFormat(calDate));
        List<DeployServiceNumber> rs = deployServiceNumberRepository.findByDateBetween(CalendarUtil.getStartDateOfWeek(calDate), CalendarUtil.getEndDateOfWeek(calDate));

        //accumulate
        for (DeployServiceNumber ds : rs) {
            deployServiceMetricsWeek.setSuccess(deployServiceMetricsWeek.getSuccess() + ds.getSuccess());
            deployServiceMetricsWeek.setFailed(deployServiceMetricsWeek.getFailed() + ds.getFailed());
        }

        return deployServiceMetricsWeek;
    }

    public DeployServiceMetricsMonth transformMonth(Calendar calDate) {

        DeployServiceMetricsMonth deployServiceMetricsMonth = new DeployServiceMetricsMonth(CalendarUtil.createYearMonthFormat(calDate));
        List<DeployServiceNumber> rs = deployServiceNumberRepository.findByDateBetween(CalendarUtil.getStartDateOfMonth(calDate), CalendarUtil.getEndDateOfMonth(calDate));

        //accumulate
        for (DeployServiceNumber ds : rs) {
            deployServiceMetricsMonth.setSuccess(deployServiceMetricsMonth.getSuccess() + ds.getSuccess());
            deployServiceMetricsMonth.setFailed(deployServiceMetricsMonth.getFailed() + ds.getFailed());
        }

        return deployServiceMetricsMonth;
    }

    public DeployServiceMetricsYear transformYear(Calendar calDate) {

        DeployServiceMetricsYear deployServiceMetricsYear = new DeployServiceMetricsYear(CalendarUtil.createYearFormat(calDate));
        List<DeployServiceNumber> rs = deployServiceNumberRepository.findByDateBetween(CalendarUtil.getStartDateOfYear(calDate), CalendarUtil.getEndDateOfYear(calDate));

        //accumulate
        for (DeployServiceNumber ds : rs) {
            deployServiceMetricsYear.setSuccess(deployServiceMetricsYear.getSuccess() + ds.getSuccess());
            deployServiceMetricsYear.setFailed(deployServiceMetricsYear.getFailed() + ds.getFailed());
        }

        return deployServiceMetricsYear;
    }

    public DeployServiceMetricsMonthSnapshot transformMonthSnapshot(Calendar calDate) {

        DeployServiceMetricsMonthSnapshot deployServiceMetricsMonthSnapshot = new DeployServiceMetricsMonthSnapshot(CalendarUtil.createYearMonthFormat(calDate));
        List<DeployServiceData> rsExecutedStatus = deployServiceDataRepository.findByUpdateDateBetweenAndExecutedStatusEquals(CalendarUtil.getStartDateOfMonth(calDate), CalendarUtil.getEndDateOfMonth(calDate), "EXECUTED_SUCCESS");
        List<DeployServiceData> rsFailed = deployServiceDataRepository.findByUpdateDateBetweenAndExecutedStatusEquals(CalendarUtil.getStartDateOfMonth(calDate), CalendarUtil.getEndDateOfMonth(calDate), "FAILED");

        //accumulate
        deployServiceMetricsMonthSnapshot.setSuccess(rsExecutedStatus.size());
        deployServiceMetricsMonthSnapshot.setFailed(rsFailed.size());

        return deployServiceMetricsMonthSnapshot;
    }
}