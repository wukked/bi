package com.toro.dms.test;

import java.util.Calendar;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.toro.dms.extractors.DeployServiceDataExtractor;
import com.toro.dms.model.DeployServiceData;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = { TestConfiguration.class })
@PropertySource(value = "classpath:spark.properties")
public class DeployServiceDataExtractorTest {

	@Autowired
	private DeployServiceDataExtractor deployServiceDataExtractor;
	
	Logger logger = LoggerFactory.getLogger(getClass());

	@Test
	public void test() {
		Calendar calDate = Calendar.getInstance();
		calDate.set(Calendar.YEAR, 2014);
		calDate.set(Calendar.MONTH, 11);
		calDate.set(Calendar.DATE, 4);

		Map<String, Iterable<DeployServiceData>> result = deployServiceDataExtractor.extract(calDate);
		logger.debug(result.toString());
	}


}
