package com.toro.dms.test;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;

import com.toro.dms.extractors.DeployServiceDataExtractor;
import com.toro.dms.utils.LogFileUtil;
@PropertySource(value = "classpath:spark.properties")
public class TestConfiguration {
	@Bean
	public DeployServiceDataExtractor deployServiceDataExtractor() {
		return new DeployServiceDataExtractor();
	}

	@Bean
	public JavaSparkContext sc() {
		return new JavaSparkContext(new SparkConf().setAppName("Toro-DMS-test").setMaster("local"));
	}
	
	@Bean
	public LogFileUtil logFileUtil() {
		return new LogFileUtil("");
	}
}
