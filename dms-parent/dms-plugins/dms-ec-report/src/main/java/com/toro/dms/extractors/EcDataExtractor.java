package com.toro.dms.extractors;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Map;

import javax.inject.Inject;

import org.apache.spark.api.java.JavaSparkContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.google.common.collect.Iterables;
import com.toro.dms.constants.LogIndex;
import com.toro.dms.model.EcData;
import com.toro.dms.utils.CalendarUtil;
import com.toro.dms.utils.LogFileUtil;

@Component
public class EcDataExtractor implements Serializable {

	Logger logger = LoggerFactory.getLogger(getClass());
	
	private static final long serialVersionUID = -8940981437234160783L;
	@Inject
	private JavaSparkContext sc;
	
	 @Inject
		private LogFileUtil logFileUtil;


	public void testFunction( ){

	}

	public Map<String, Iterable<EcData>> extract(Calendar date) {

		String path = logFileUtil.getLogFileName(CalendarUtil.format(CalendarUtil.getDatePlus1(date)));

		return sc.textFile(path).map(l -> l.split(";")).filter(l -> l[LogIndex.ACTION.value].equals("CheckGlobalEligibility") && l.length != 13).sortBy(l -> l[LogIndex.END_TIME.value], true, 1)
				.groupBy(l -> l[LogIndex.ICCID.value])
				.map(r -> Iterables.getLast(r._2())).map(l -> new EcData(l[LogIndex.ICCID.value], l[LogIndex.EXTRA_INFO.value].substring(l[LogIndex.EXTRA_INFO.value].indexOf('=') + 1).split(",")[0], date))
				.groupBy(l -> l.getExecutedStatus()).collectAsMap();

/*

EXECUTED_SUCCESS, eligible=true, failCodes=[]
l[LogIndex.EXTRA_INFO.value]

		return sc.textFile(path).map(l -> l.split(";")).filter(l -> l[LogIndex.ACTION.value].equals("CheckGlobalEligibility")).sortBy(l -> l[LogIndex.END_TIME.value], true, 1)
				.groupBy(l -> l[LogIndex.ICCID.value])
				.map(r -> Iterables.getLast(r._2())).map(l -> new EcData(l[LogIndex.ICCID.value], l[LogIndex.EXTRA_INFO.value].substring(7), date))
				.groupBy(l -> l.getExecutedStatus()).collectAsMap();


		//fixed
		return sc.textFile(path).map(l -> l.split(";")).filter(l -> l[LogIndex.ACTION.value].equals("CheckGlobalEligibility")).sortBy(l -> l[LogIndex.END_TIME.value], true, 1)
				.groupBy(l -> l[LogIndex.ICCID.value])
				.map(r -> Iterables.getLast(r._2())).map(l -> new EcData(l[LogIndex.ICCID.value], l[LogIndex.EXTRA_INFO.value].substring(l[LogIndex.EXTRA_INFO.value].indexOf('=')+1, l[LogIndex.EXTRA_INFO.value].indexOf(',')), date))
				.groupBy(l -> l.getExecutedStatus()).collectAsMap();
*/
	}

}
