package com.toro.dms.info;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(301)
public class EcReportDetail implements ReportDetailInterface {

	@Override
	public String getName() {
		return "Eligibility Check Analysis";
	}

	@Override
	public String getLink() {
		return "report-ui/ec-report.html";
	}

}
