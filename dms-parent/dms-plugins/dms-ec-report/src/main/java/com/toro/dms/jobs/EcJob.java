package com.toro.dms.jobs;

import java.util.Calendar;
import java.util.Map;
import java.util.Vector;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.google.common.collect.Iterables;
import com.toro.dms.extractors.EcDataExtractor;
import com.toro.dms.model.EcData;
import com.toro.dms.model.EcMetricsMonth;
import com.toro.dms.model.EcMetricsMonthSnapshot;
import com.toro.dms.model.EcMetricsWeek;
import com.toro.dms.model.EcMetricsYear;
import com.toro.dms.model.EcNumber;
import com.toro.dms.repository.EcDataRepository;
import com.toro.dms.repository.EcMetricsMonthsRepository;
import com.toro.dms.repository.EcMetricsMonthsSnapshotRepository;
import com.toro.dms.repository.EcMetricsWeekRepository;
import com.toro.dms.repository.EcMetricsYearsRepository;
import com.toro.dms.repository.EcNumberRepository;
import com.toro.dms.transformers.EcNumberTransformer;
import com.toro.dms.utils.CalendarUtil;

@Component
//Run first, no data reliance
@Order(100)
public class EcJob implements Job {

	@Inject
	private EcDataExtractor ecExtractor;

	@Inject
	private EcDataRepository ecDataRepository;

	@Inject
	private EcNumberRepository ecNumberRepository;

	@Inject
	private EcNumberTransformer ecNumberTransformer;

	@Inject
	private EcMetricsWeekRepository ecMetricsWeekRepository;

	@Inject
	private EcMetricsMonthsRepository ecMetricsMonthsRepository;

	@Inject
	private EcMetricsMonthsSnapshotRepository ecMetricsMonthsSnapshotRepository;

	@Inject
	private EcMetricsYearsRepository ecMetricsYearsRepository;

	private final Iterable<EcData> empty = new Vector<EcData>();

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void executeWithCalDate(Calendar calDate) {

		logger.debug("do ec extractor" + calDate.getTime().toString());

		Map<String, Iterable<EcData>> ecResult = ecExtractor.extract(calDate);

		logger.debug("extractors finished");

		logger.debug("saving Ec");

		ecResult.values().forEach(l -> ecDataRepository.save(l));

		// ecDataResult.forEach(action -> ecDataRepository.save(action));
		// ecDataRepository.save(entities)

		EcNumber ecNumber = new EcNumber(calDate, Iterables.size(ecResult.getOrDefault("EXECUTED_SUCCESS", empty)), Iterables.size(ecResult.getOrDefault("FAILED", empty)));

		logger.debug("Get mapped EcNumber, now save them");

		if (logger.isDebugEnabled()) {
			logger.debug(ecNumber.getDate().getTime() + "  " + ecNumber.getSuccess() + "  " + ecNumber.getFailed());
		}

		ecNumberRepository.save(ecNumber);

		logger.debug("do transform ec metrics transformations");

		EcMetricsWeek ecMetricsWeek = ecNumberTransformer.transformWeek(calDate);
		EcMetricsMonth ecMetricsMonth = ecNumberTransformer.transformMonth(calDate);
		EcMetricsYear ecMetricsYear = ecNumberTransformer.transformYear(calDate);
		EcMetricsMonthSnapshot ecMetricsMonthSnapshot = ecNumberTransformer.transformMonthSnapshot(calDate);

		logger.debug("EcMetrics transformed, now save them");

		ecMetricsWeekRepository.save(ecMetricsWeek);
		ecMetricsMonthsRepository.save(ecMetricsMonth);
		ecMetricsYearsRepository.save(ecMetricsYear);
		ecMetricsMonthsSnapshotRepository.save(ecMetricsMonthSnapshot);

	}

}