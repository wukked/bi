package com.toro.dms.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

import javax.persistence.*;

@Entity
public class EcData implements Serializable {

    private static final long serialVersionUID = -4873312349976667707L;
    @Id
    @Column(unique = true)
    private String iccid;
    private String executedStatus;
    @Temporal(TemporalType.DATE)
    private Calendar updateDate;

    //Todo:
    // WM 2.2
    // private boolean eligible;
    // private int reason;

    public EcData() {
    }

    public EcData(String iccid, String executedStatus, Calendar updateDate) {
        this();
        this.iccid = iccid;
        this.executedStatus = executedStatus;
        this.updateDate = updateDate;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public String getExecutedStatus() {
        return this.executedStatus;
    }

    public void setExecutedStatus(String executedStatus) {
        this.executedStatus = executedStatus;
    }

    public Calendar getUpdateDate() {
        return this.updateDate;
    }

    public void setUpdateDate(Calendar updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        return "EcData [iccid=" + iccid + ", executedStatus=" + executedStatus + ", updateDate=" + updateDate + "]";
    }

}
