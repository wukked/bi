package com.toro.dms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class EcMetricsMonth extends EcType implements Serializable {

    private static final long serialVersionUID = -4223353309972811701L;
    @Id
    @Column(unique = true)
    private String yearMonth;

    public EcMetricsMonth() {
        super();
    }

    public EcMetricsMonth(String yearMonth) {
        super();
        this.yearMonth = yearMonth;
    }

    public EcMetricsMonth(String yearMonth, int success, int failed) {
        super(success, failed);
        this.yearMonth = yearMonth;
    }

    public String getYearMonth() {
        return yearMonth;
    }

    public void setYearMonth(String yearMonth) {
        this.yearMonth = yearMonth;
    }

}
