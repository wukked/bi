package com.toro.dms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class EcMetricsMonthSnapshot extends EcType implements Serializable {

    private static final long serialVersionUID = -999353309972877707L;
    @Id
    @Column(unique = true)
    private String yearMonth;

    public EcMetricsMonthSnapshot() {
        super();
    }

    public EcMetricsMonthSnapshot(String yearMonth) {
        super();
        this.yearMonth = yearMonth;
    }

    public EcMetricsMonthSnapshot(String yearMonth, int success, int failed) {
        super(success, failed);
        this.yearMonth = yearMonth;
    }

    public String getYearMonth() {
        return yearMonth;
    }

    public void setYearMonth(String yearMonth) {
        this.yearMonth = yearMonth;
    }

}
