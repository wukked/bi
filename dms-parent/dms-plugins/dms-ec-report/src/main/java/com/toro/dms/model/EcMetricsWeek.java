package com.toro.dms.model;

import java.io.Serializable;
import javax.persistence.*;

@Entity
public class EcMetricsWeek extends EcType implements Serializable {

    private static final long serialVersionUID = -4878888309972877707L;
    @Id
    @Column(unique = true)
    private String yearWeek;

    public EcMetricsWeek() {
        super();
    }

    public EcMetricsWeek(String yearWeek) {
        super();
        this.yearWeek = yearWeek;
    }

    public EcMetricsWeek(String yearWeek, int success, int failed) {
        super(success, failed);
        this.yearWeek = yearWeek;
    }

    public String getYearWeek() {
        return yearWeek;
    }

    public void setYearWeek(String yearWeek) {
        this.yearWeek = yearWeek;
    }

}
