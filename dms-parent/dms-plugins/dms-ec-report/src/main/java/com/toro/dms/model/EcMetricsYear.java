package com.toro.dms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class EcMetricsYear extends EcType implements Serializable {

    private static final long serialVersionUID = -4873353309372877888L;
    @Id
    @Column(unique = true)
    private String year;

    public EcMetricsYear() {
        super();
    }

    public EcMetricsYear(String yearMonth) {
        super();
        this.year = yearMonth;
    }

    public EcMetricsYear(String yearMonth, int success, int failed) {
        super(success, failed);
        this.year = yearMonth;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

}
