package com.toro.dms.model;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.*;

@Entity
public class EcNumber extends EcType implements Serializable{

	private static final long serialVersionUID = -4873311119972872227L;
	@Id
    @Temporal(TemporalType.DATE)
	@Column(unique = true)
    private Calendar date;

	public EcNumber() {
		super();
	}

	public EcNumber(Calendar date, int success, int failed) {
		super(success, failed);
		this.date = date;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

}
