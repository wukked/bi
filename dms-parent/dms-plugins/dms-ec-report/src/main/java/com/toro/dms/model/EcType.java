package com.toro.dms.model;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class EcType {

    private int success;
    private int failed;

    public EcType() {

    }

    public EcType(int success, int failed) {
        this();
        this.success = success;
        this.failed = failed;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public int getFailed() {
        return failed;
    }

    public void setFailed(int failed) {
        this.failed = failed;
    }

    public long totalElgibilityCheck(){
        return this.success + this.failed;
    }

    public double successPercent(){
        return (double)this.getSuccess()/(double)totalElgibilityCheck();
    }

    public double failedPercent(){
        return (double)this.getFailed()/(double)totalElgibilityCheck();
    }

}
