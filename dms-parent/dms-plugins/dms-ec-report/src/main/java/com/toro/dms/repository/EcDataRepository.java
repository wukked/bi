package com.toro.dms.repository;

import org.springframework.data.repository.CrudRepository;

import com.toro.dms.model.EcData;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public interface EcDataRepository extends CrudRepository<EcData, String> {

    public List<EcData> findByUpdateDate(Calendar updateDate);
    public List<EcData> findByUpdateDateBetweenAndExecutedStatusEquals(Calendar start, Calendar end, String executedStatus);

}
