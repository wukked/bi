package com.toro.dms.repository;

import com.toro.dms.model.EcMetricsMonth;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface EcMetricsMonthsRepository extends CrudRepository<EcMetricsMonth, Long> {

    public List<EcMetricsMonth> findByYearMonth(String yearMonth);

}
