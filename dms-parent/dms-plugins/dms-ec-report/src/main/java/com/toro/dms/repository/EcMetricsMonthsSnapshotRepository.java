package com.toro.dms.repository;

import com.toro.dms.model.EcMetricsMonthSnapshot;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EcMetricsMonthsSnapshotRepository extends CrudRepository<EcMetricsMonthSnapshot, Long> {

    public List<EcMetricsMonthSnapshot> findByYearMonth(String yearMonth);

}
