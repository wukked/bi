package com.toro.dms.repository;

import java.util.List;

import com.toro.dms.model.EcMetricsWeek;
import org.springframework.data.repository.CrudRepository;


public interface EcMetricsWeekRepository extends CrudRepository<EcMetricsWeek, Long> {

    public List<EcMetricsWeek> findByYearWeek(String yearWeek);

}
