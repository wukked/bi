package com.toro.dms.repository;

import com.toro.dms.model.EcMetricsYear;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface EcMetricsYearsRepository extends CrudRepository<EcMetricsYear, Long> {

    public List<EcMetricsYear> findByYear(String year);

}
