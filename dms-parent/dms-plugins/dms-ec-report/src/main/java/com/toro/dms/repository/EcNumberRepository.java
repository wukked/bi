package com.toro.dms.repository;

import com.toro.dms.model.EcNumber;
import org.springframework.data.repository.CrudRepository;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public interface EcNumberRepository extends CrudRepository<EcNumber, String> {

    public List<EcNumber> findByDate(Calendar date);
    public List<EcNumber> findByDateBetween(Calendar start, Calendar end);

}
