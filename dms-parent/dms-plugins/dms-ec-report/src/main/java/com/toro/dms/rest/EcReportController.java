package com.toro.dms.rest;

import com.toro.dms.model.*;
import com.toro.dms.repository.*;
import com.toro.dms.utils.CalendarUtil;
import com.toro.dms.utils.CommonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;

@RequestMapping("/ecReport")
@RestController
public class EcReportController {

    private static final Logger logger = LoggerFactory.getLogger( EcReportController.class );

    @Inject
    EcNumberRepository ecNumberRepository;

    @Inject
    EcMetricsWeekRepository ecMetricsWeekRepository;

    @Inject
    EcMetricsMonthsRepository ecMetricsMonthsRepository;

    @Inject
    EcMetricsYearsRepository ecMetricsYearsRepository;

    @Inject
    EcMetricsMonthsSnapshotRepository ecMetricsMonthsSnapshotRepository;

    @RequestMapping(method = RequestMethod.GET, value="/getYesterday")
    public String getYesterday( HttpServletResponse response) {

        logger.debug("do get yesterday");
        logger.debug("caculated....");

        Calendar yesterday = CalendarUtil.getYesterday1();

        setCsvResponseHeader(response, "EcDayReport.csv");

        Iterable<EcNumber> rs = ecNumberRepository.findByDate(yesterday);

        String csv = createCsvHeaderTitles();

        for(EcNumber ec : rs){
            csv += createCsvRow( ec );
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value="/getDay")
    public String getDay( HttpServletResponse response, @RequestParam(required = true) Calendar date) {

        logger.debug("do get date");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "EcDayReport.csv");

        Iterable<EcNumber> rs = ecNumberRepository.findByDate(date);

        String csv = createCsvHeaderTitles();

        for(EcNumber ec : rs){
            csv += createCsvRow( ec );
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value="/getAllDays")
    public String getAllDays( HttpServletResponse response) {

        logger.debug("do get all days");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "EcAllDaysReport.csv");

        Iterable<EcNumber> rs = ecNumberRepository.findAll();

        String csv = createCsvHeaderTitles();

        for(EcNumber ec : rs){
            csv += createCsvRow( ec );
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value="/getCurrentWeek")
    public String getCurrentWeek( HttpServletResponse response) {

        logger.debug("do get current week");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "EcWeekReport.csv");

        //get current week/year
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int week = now.get(Calendar.WEEK_OF_YEAR);

        Iterable<EcMetricsWeek> rs = ecMetricsWeekRepository.findByYearWeek(Integer.toString(year) + "-" + Integer.toString(week));

        String csv = createCsvHeaderTitles();

        for(EcMetricsWeek ec : rs){
            csv += createCsvRow( ec );
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value="/getAllWeeks")
    public String getAllWeeks( HttpServletResponse response) {

        logger.debug("do get all weeks");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "EcAllWeekReport.csv");

        Iterable<EcMetricsWeek> rs = ecMetricsWeekRepository.findAll();

        String csv = createCsvHeaderTitles();

        for(EcMetricsWeek ec : rs){
            csv += createCsvRow( ec );
        }
        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value="/getCurrentMonth")
    public String getCurrentMonth( HttpServletResponse response) {

        logger.debug("do get month");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "EcMonthReport.csv");

        //get current week/year
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH) + 1;

        Iterable<EcMetricsMonth> rs = ecMetricsMonthsRepository.findByYearMonth(Integer.toString(year) + "-" + Integer.toString(month));

        String csv = createCsvHeaderTitles();

        for(EcMetricsMonth ec : rs){
            csv += createCsvRow( ec );
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value="/getAllMonths")
    public String getAllMonths( HttpServletResponse response) {

        logger.debug("do get all months");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "EcAllMonthsReport.csv");

        Iterable<EcMetricsMonth> rs = ecMetricsMonthsRepository.findAll();

        String csv = createCsvHeaderTitles();

        for(EcMetricsMonth ec : rs){
            csv += createCsvRow( ec );
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value="/getLastMonthSnapshot")
    public String getLastMonthSnapshot( HttpServletResponse response) {

        logger.debug("do get month snapshot");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "EcLastMonthSnapshotReport.csv");

        //get current week/year
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH) + 1;

        Iterable<EcMetricsMonthSnapshot> rs = ecMetricsMonthsSnapshotRepository.findByYearMonth(Integer.toString(year) + "-" + Integer.toString(month));

        String csv = createCsvHeaderTitles();

        for(EcMetricsMonthSnapshot ec : rs){
            csv += createCsvRow( ec );
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value="/getAllMonthSnapshots")
    public String getAllMonthSnapshots( HttpServletResponse response) {

        logger.debug("do get all month snapshot");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "EcAllMonthSnapshotReport.csv");

        Iterable<EcMetricsMonthSnapshot> rs = ecMetricsMonthsSnapshotRepository.findAll();

        String csv = createCsvHeaderTitles();

        for(EcMetricsMonthSnapshot ec : rs){
            csv += createCsvRow( ec );
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value="/getCurrentYear")
    public String getCurrentYear( HttpServletResponse response) {

        logger.debug("do get current year");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "EcYearReport.csv");

        //get current year
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);

        Iterable<EcMetricsYear> rs = ecMetricsYearsRepository.findByYear(Integer.toString(year));

        String csv = createCsvHeaderTitles();

        for(EcMetricsYear ec : rs){
            csv += createCsvRow( ec );
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value="/getAllYears")
    public String getAllYears( HttpServletResponse response) {

        logger.debug("do get all years");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "EcAllYearReport.csv");

        Iterable<EcMetricsYear> rs = ecMetricsYearsRepository.findAll();

        String csv = createCsvHeaderTitles();

        for(EcMetricsYear ec : rs){
            csv += createCsvRow( ec );
        }

        return csv;
    }

    private void setCsvResponseHeader( HttpServletResponse response, String fileName ){
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
        response.setContentType("application/x-download");
        response.addHeader( "Content-Disposition", "filename=\"" + fileName + "\"");
    }

    private String createCsvHeaderTitles(){
        return "Time,EC started,EC started %,EC failed,EC failed %, EC succeeded, EC succeeded %\r\n";
    }

    private String createCsvRow(EcType rs){
        logger.debug("Create CSV row");

        String time = "";

        if( rs instanceof EcNumber ){
            time = CalendarUtil.format(((EcNumber) rs).getDate());
        }else if( rs instanceof EcMetricsWeek){
            time = ((EcMetricsWeek) rs).getYearWeek().toString();
        }else if( rs instanceof EcMetricsMonth ){
            time = ((EcMetricsMonth) rs).getYearMonth().toString();
        }else if( rs instanceof EcMetricsYear ){
            time = ((EcMetricsYear) rs).getYear().toString();
        }else if( rs instanceof EcMetricsMonthSnapshot ){
            time = ((EcMetricsMonthSnapshot) rs).getYearMonth().toString();
        }else{
            logger.debug("Error finding result set class type");
        }

        return  time + "," +
                rs.totalElgibilityCheck() + "," +
                "-," +
                rs.getFailed() + "," +
                CommonUtil.percentageFormat(rs.failedPercent()) + "," +
                rs.getSuccess() + "," +
                CommonUtil.percentageFormat(rs.successPercent()) +
                "\r\n";

    }



}