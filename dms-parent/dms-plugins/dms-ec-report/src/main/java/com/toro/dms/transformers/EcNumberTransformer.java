package com.toro.dms.transformers;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import com.toro.dms.model.*;
import com.toro.dms.repository.EcNumberRepository;
import com.toro.dms.repository.EcDataRepository;
import com.toro.dms.utils.CalendarUtil;
import org.apache.spark.api.java.JavaSparkContext;
import org.springframework.stereotype.Component;

@Component
public class EcNumberTransformer {

    @Inject
    private EcNumberRepository ecNumberRepository;

    @Inject
    private EcDataRepository ecDataRepository;

    @Inject
    private JavaSparkContext sc;

    public EcMetricsWeek transformWeek( Calendar calDate ) {

        EcMetricsWeek ecMetricsWeek = new EcMetricsWeek(CalendarUtil.createYearWeekFormat(calDate));
        List<EcNumber> rs = ecNumberRepository.findByDateBetween(CalendarUtil.getStartDateOfWeek(calDate), CalendarUtil.getEndDateOfWeek(calDate));

        //accumulate
        for( EcNumber ec : rs ){
            ecMetricsWeek.setSuccess(ecMetricsWeek.getSuccess() + ec.getSuccess());
            ecMetricsWeek.setFailed(ecMetricsWeek.getFailed() + ec.getFailed());
        }

        return ecMetricsWeek;
    }

    public EcMetricsMonth transformMonth( Calendar calDate ){

        EcMetricsMonth ecMetricsMonth = new EcMetricsMonth(CalendarUtil.createYearMonthFormat(calDate));
        List<EcNumber> rs = ecNumberRepository.findByDateBetween(CalendarUtil.getStartDateOfMonth(calDate), CalendarUtil.getEndDateOfMonth(calDate));

        //accumulate
        for( EcNumber ec : rs ){
            ecMetricsMonth.setSuccess(ecMetricsMonth.getSuccess() + ec.getSuccess());
            ecMetricsMonth.setFailed(ecMetricsMonth.getFailed() + ec.getFailed());
        }

        return ecMetricsMonth;
    }

    public EcMetricsYear transformYear( Calendar calDate ) {

        EcMetricsYear ecMetricsYear = new EcMetricsYear(CalendarUtil.createYearFormat(calDate));
        List<EcNumber> rs = ecNumberRepository.findByDateBetween(CalendarUtil.getStartDateOfYear(calDate), CalendarUtil.getEndDateOfYear(calDate));

        //accumulate
        for( EcNumber ec : rs ){
            ecMetricsYear.setSuccess(ecMetricsYear.getSuccess() + ec.getSuccess());
            ecMetricsYear.setFailed(ecMetricsYear.getFailed() + ec.getFailed());
        }

        return ecMetricsYear;
    }

    public EcMetricsMonthSnapshot transformMonthSnapshot( Calendar calDate ) {

        EcMetricsMonthSnapshot ecMetricsMonthSnapshot = new EcMetricsMonthSnapshot(CalendarUtil.createYearMonthFormat(calDate));
        List<EcData> rsExecutedStatus = ecDataRepository.findByUpdateDateBetweenAndExecutedStatusEquals(CalendarUtil.getStartDateOfMonth(calDate), CalendarUtil.getEndDateOfMonth(calDate), "EXECUTED_SUCCESS");
        List<EcData> rsFailed = ecDataRepository.findByUpdateDateBetweenAndExecutedStatusEquals(CalendarUtil.getStartDateOfMonth(calDate), CalendarUtil.getEndDateOfMonth(calDate), "FAILED");

        //accumulate
        ecMetricsMonthSnapshot.setSuccess(rsExecutedStatus.size());
        ecMetricsMonthSnapshot.setFailed(rsFailed.size());

        return ecMetricsMonthSnapshot;
    }
}