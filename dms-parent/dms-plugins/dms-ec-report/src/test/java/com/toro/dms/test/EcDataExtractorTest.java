package com.toro.dms.test;

import java.util.Calendar;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.toro.dms.extractors.EcDataExtractor;
import com.toro.dms.model.EcData;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = { TestConfiguration.class })
@PropertySource(value = "classpath:spark.properties")
public class EcDataExtractorTest {

	@Autowired
	private EcDataExtractor ecExtractor;
	
	Logger logger = LoggerFactory.getLogger(getClass());

	@Test
	public void test() {

		Calendar calDate = Calendar.getInstance();
		calDate.set(Calendar.MONTH, 11);
		calDate.set(Calendar.DAY_OF_MONTH, 10);
		calDate.set(Calendar.YEAR, 2014);

		Map<String, Iterable<EcData>> result = ecExtractor.extract(calDate);
		for(String k : result.keySet()){
			logger.debug("==================================================="+k);
			
			result.get(k).forEach(l->logger.debug(l.toString()));
			
		}
		
		

	}


}
