package com.toro.dms.extractors;

import com.google.common.collect.Iterators;
import com.toro.dms.constants.LogIndex;
import com.toro.dms.model.PurchaseDistributionByActiveUser;
import com.toro.dms.repository.PurchaseDistributionByActiveUserDataRepository;
import com.toro.dms.utils.CalendarUtil;
import com.toro.dms.utils.LogFileUtil;
import org.apache.spark.api.java.JavaSparkContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Component
public class PurchaseDistributionActiveUserExtractor implements Serializable {

	Logger logger = LoggerFactory.getLogger(getClass());
	
	private static final long serialVersionUID = -8140981137231160781L;
	@Inject
	private JavaSparkContext sc;
	
	@Inject
	private LogFileUtil logFileUtil;

	@Inject
	PurchaseDistributionByActiveUserDataRepository purchaseDistributionByActiveUserDataRepository;

	public List<PurchaseDistributionByActiveUser> extract(Calendar date) {

		String path = logFileUtil.getLogFileName(CalendarUtil.format(CalendarUtil.getDatePlus1(date)));

		List<PurchaseDistributionByActiveUser> pdbauList = new ArrayList();

		sc.textFile(path)
		.map(l -> l.split(";"))
		.filter(l -> l[LogIndex.ACTION.value].equals("paymentEvent"))
		.groupBy(l -> l[LogIndex.SIR.value])
		.collect().forEach( l -> pdbauList.add( new PurchaseDistributionByActiveUser(date, l._1(), Iterators.size(l._2().iterator())) ));

		return pdbauList;
	}

}
