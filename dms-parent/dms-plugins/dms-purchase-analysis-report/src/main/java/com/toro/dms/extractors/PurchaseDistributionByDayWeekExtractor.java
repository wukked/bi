/*
not used due to day week data base numbers from hours extractor
 */

package com.toro.dms.extractors;

import com.toro.dms.constants.LogIndex;
import com.toro.dms.model.PurchaseDistributionByDayWeekMetricsWeek;
import com.toro.dms.utils.CalendarUtil;
import com.toro.dms.utils.LogFileUtil;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.io.Serializable;
import java.util.Calendar;

@Component
public class PurchaseDistributionByDayWeekExtractor implements Serializable {

    Logger logger = LoggerFactory.getLogger(getClass());

    private static final long serialVersionUID = -8940981123456780783L;
    @Inject
    private JavaSparkContext sc;
    
    @Inject
	private LogFileUtil logFileUtil;

	public PurchaseDistributionByDayWeekMetricsWeek extract(Calendar date) {
		String path = logFileUtil.getLogFileName(CalendarUtil.format(CalendarUtil.getDatePlus1(date)));

		JavaRDD<String[]> logs = sc.textFile(path).filter(f -> f.contains("paymentEvent")).map(f -> f.split(";")).cache();

		logs.foreach( f -> f[LogIndex.END_TIME.value] = Integer.toString(CalendarUtil.getDayOfWeek(f[LogIndex.END_TIME.value].substring(0, 11))));
		logs.groupBy( f-> f[LogIndex.END_TIME.value] );

		long sundayCount = logs.filter( f -> f[LogIndex.END_TIME.value].contains("1") ).count();
		long mondayCount = logs.filter( f -> f[LogIndex.END_TIME.value].contains("2") ).count();
		long tuesdayCount = logs.filter( f -> f[LogIndex.END_TIME.value].contains("3") ).count();
		long wednesdayCount = logs.filter( f -> f[LogIndex.END_TIME.value].contains("4") ).count();
		long thursdayCount = logs.filter( f -> f[LogIndex.END_TIME.value].contains("5") ).count();
		long fridayCount = logs.filter( f -> f[LogIndex.END_TIME.value].contains("6") ).count();
		long saturdayCount = logs.filter( f -> f[LogIndex.END_TIME.value].contains("7") ).count();

		PurchaseDistributionByDayWeekMetricsWeek pdbdwmw = new PurchaseDistributionByDayWeekMetricsWeek( CalendarUtil.createYearWeekFormat(date), sundayCount, mondayCount, tuesdayCount, wednesdayCount, thursdayCount, fridayCount, saturdayCount);

		return pdbdwmw;
		}

	}

