package com.toro.dms.extractors;

import com.toro.dms.constants.LogIndex;
import com.toro.dms.model.PurchaseDistributionByHour;
import com.toro.dms.utils.CalendarUtil;
import com.toro.dms.utils.LogFileUtil;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.io.Serializable;
import java.util.Calendar;

@Component
public class PurchaseDistributionByHourExtractor implements Serializable {

    Logger logger = LoggerFactory.getLogger(getClass());

    private static final long serialVersionUID = -8940981437234160783L;
    @Inject
    private JavaSparkContext sc;
    
    @Inject
	private LogFileUtil logFileUtil;

	public PurchaseDistributionByHour extract(Calendar date) {
		String path = logFileUtil.getLogFileName(CalendarUtil.format(CalendarUtil.getDatePlus1(date)));

		JavaRDD<String[]> logs = sc.textFile(path).filter(f -> f.contains("paymentEvent")).map(f -> f.split(";")).cache();

		long[] distributedHoursArray = new long[24];

		//TODO: do a groupby instead of a for loop to fix processing issues
		for(int i=0;i<24;i++){
			final int finalI = i;
			long hourCount = logs.filter( f->f[LogIndex.END_TIME.value].substring(11,13).contains(String.format("%02d", finalI))).count();
			distributedHoursArray[i] = hourCount;

			System.out.println( "Executing hour extraction - formatted string number: " + String.format("%02d", finalI));
		}

		return new PurchaseDistributionByHour(
						date,
						distributedHoursArray[0],
						distributedHoursArray[1],
						distributedHoursArray[2],
						distributedHoursArray[3],
						distributedHoursArray[4],
						distributedHoursArray[5],
						distributedHoursArray[6],
						distributedHoursArray[7],
						distributedHoursArray[8],
						distributedHoursArray[9],
						distributedHoursArray[10],
						distributedHoursArray[11],
						distributedHoursArray[12],
						distributedHoursArray[13],
						distributedHoursArray[14],
						distributedHoursArray[15],
						distributedHoursArray[16],
						distributedHoursArray[17],
						distributedHoursArray[18],
						distributedHoursArray[19],
						distributedHoursArray[20],
						distributedHoursArray[21],
						distributedHoursArray[22],
						distributedHoursArray[23]
				);
	}



}
