package com.toro.dms.extractors;

import com.toro.dms.model.PurchaseDistributionByType;
import com.toro.dms.utils.CalendarUtil;
import com.toro.dms.utils.LogFileUtil;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.io.Serializable;
import java.util.Calendar;

@Component
public class PurchaseDistributionByTypeExtractor implements Serializable {

    Logger logger = LoggerFactory.getLogger(getClass());

    private static final long serialVersionUID = -8940981437234160783L;
    @Inject
    private JavaSparkContext sc;
    
    @Inject
	private LogFileUtil logFileUtil;

	public PurchaseDistributionByType extract(Calendar date) {
		String path = logFileUtil.getLogFileName(CalendarUtil.format(CalendarUtil.getDatePlus1(date)));

		JavaRDD<String[]> logs = sc.textFile(path).filter(f -> f.contains("paymentEvent")).map(f -> f.split(";")).cache();

		long offlineApprovedCount = logs.filter( f->f[11].contains("off_app")).count();
		long offlineDeclinedCount = logs.filter( f->f[11].contains("off_dec")).count();
		long onlineCount = logs.filter( f->f[11].contains("online")).count();
		long tapPinTapCount = logs.filter( f->f[11].contains("tap_pin_tap")).count();

		System.out.println( "PurchaseDistributionByTypeExtractor " + date.getTime().toString() );

		return new PurchaseDistributionByType(date, offlineApprovedCount, offlineDeclinedCount, onlineCount, tapPinTapCount);
	}

}
