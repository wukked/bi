package com.toro.dms.info;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(604)
public class PurchaseDistributionByDayWeekDetail {
	//do not add until phase 2
//public class PurchaseDistributionByDayWeekDetail implements ReportDetailInterface {

	//@Override
	public String getName() {
		return "Purchase Analysis - Purchase Distribution by Day of Week";
	}

	//@Override
	public String getLink() {
		return "report-ui/analysis-report/purchase-analysis-by-day-week-report.html";
	}

}
