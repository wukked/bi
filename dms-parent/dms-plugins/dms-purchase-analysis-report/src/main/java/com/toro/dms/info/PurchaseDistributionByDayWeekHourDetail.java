package com.toro.dms.info;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(605)
public class PurchaseDistributionByDayWeekHourDetail {
	//do not add until phase 2
//public class PurchaseDistributionByDayWeekHourDetail implements ReportDetailInterface {

	//@Override
	public String getName() {
		return "Purchase Analysis - Purchase Distribution Per Hour of Each Day of Week";
	}

	//@Override
	public String getLink() {
		return "report-ui/analysis-report/purchase-analysis-by-day-week-hour-report.html";
	}

}
