package com.toro.dms.info;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(603)
public class PurchaseDistributionByHourDetail implements ReportDetailInterface {

	@Override
	public String getName() {
		return "Purchase Analysis - Purchase Distribution by Hour";
	}

	@Override
	public String getLink() {
		return "report-ui/analysis-report/purchase-analysis-by-hour-report.html";
	}

}
