package com.toro.dms.info;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(600)
public class PurchaseDistributionByTypeDetail {
	//do not add until phase 2
//public class PurchaseDistributionByTypeDetail implements ReportDetailInterface {

	//@Override
	public String getName() {
		return "Purchase Analysis - Purchase Distribution by Type";
	}

	//@Override
	public String getLink() {
		return "report-ui/analysis-report/purchase-analysis-by-type-report.html";
	}

}
