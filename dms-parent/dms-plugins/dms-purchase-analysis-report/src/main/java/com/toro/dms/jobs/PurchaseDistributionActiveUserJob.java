package com.toro.dms.jobs;

import com.toro.dms.extractors.PurchaseDistributionActiveUserExtractor;
import com.toro.dms.model.*;
import com.toro.dms.repository.*;
import com.toro.dms.transformers.PurchaseDistributionByFrequencyTransformer;
import com.toro.dms.utils.CalendarUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.*;

@Component
//Run first, no data reliance
@Order(600)
public class PurchaseDistributionActiveUserJob implements Job {

	@Inject
	private PurchaseDistributionActiveUserExtractor pdauExtractor;

	@Inject
	private PurchaseDistributionByActiveUserDataRepository purchaseDistributionByActiveUserDataRepository;

	@Inject
	private PurchaseDistributionByActiveUserMetricsWeekRepository purchaseDistributionByActiveUserMetricsWeekRepository;

	@Inject
	private PurchaseDistributionByActiveUserMetricsMonthRepository purchaseDistributionByActiveUserMetricsMonthRepository;

	@Inject
	private PurchaseDistributionByActiveUserMetricsYearRepository purchaseDistributionByActiveUserMetricsYearRepository;

	@Inject
	private PurchaseDistributionByFrequencyMetricsDayRepository purchaseDistributionByFrequencyMetricsDayRepository;

	@Inject
	private PurchaseDistributionByFrequencyTransformer purchaseDistributionByFrequencyTransformer;

	private final Iterable<PurchaseDistributionByActiveUser> empty = new Vector<PurchaseDistributionByActiveUser>();

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void executeWithCalDate(Calendar calDate) {

		logger.debug("do PurchaseDistributionActiveUserExtractor extractor" + calDate.getTime().toString());

		List<PurchaseDistributionByActiveUser> pdauResult = pdauExtractor.extract(calDate);

		logger.debug("extractors finished");

		logger.debug("saving PurchaseDistributionActiveUser");

		purchaseDistributionByActiveUserDataRepository.save(pdauResult);


		logger.debug("do transform PurchaseDistributionActiveUser metrics transformations");
		PurchaseDistributionByFrequencyMetricsDay pdbauMetricsday = purchaseDistributionByFrequencyTransformer.transformDay(calDate);
		logger.debug("saving PurchaseDistributionActiveUserMetricsDay");
		purchaseDistributionByFrequencyMetricsDayRepository.save(pdbauMetricsday);

		List<PurchaseDistributionByActiveUserMetricsWeek> pdbauMetricsWeekList = purchaseDistributionByFrequencyTransformer.transformWeek(calDate);
		//List<PurchaseDistributionByActiveUserMetricsMonth> pdbauMetricsMonthList = purchaseDistributionByActiveUserTransformer.transformMonth(calDate);
		//List<PurchaseDistributionByActiveUserMetricsYear> pdbauMetricsYearList = purchaseDistributionByActiveUserTransformer.transformYear(calDate);

		logger.debug("PurchaseDistributionByActiveUser transformed, now save them");

		purchaseDistributionByActiveUserMetricsWeekRepository.save(pdbauMetricsWeekList);
		//purchaseDistributionByActiveUserMetricsMonthRepository.save(pdbauMetricsMonthList);
		//purchaseDistributionByActiveUserMetricsYearRepository.save(pdbauMetricsYearList);

	}

}