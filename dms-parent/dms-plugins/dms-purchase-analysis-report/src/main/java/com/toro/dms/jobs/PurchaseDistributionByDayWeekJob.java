package com.toro.dms.jobs;

import com.toro.dms.extractors.PurchaseDistributionByDayWeekExtractor;
import com.toro.dms.extractors.PurchaseDistributionByHourExtractor;
import com.toro.dms.model.*;
import com.toro.dms.repository.*;
import com.toro.dms.transformers.PurchaseDistributionByDayWeekTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Calendar;

@Component
//Reliant on PuchaseDistributiobByHour data, must run after PuchaseDistributiobByHourJob
@Order(602)
public class PurchaseDistributionByDayWeekJob implements Job {
    @Inject
    private PurchaseDistributionByDayWeekExtractor purchaseDistributionByDayWeekExtractor;

    @Inject
    private PurchaseDistributionByHourExtractor purchaseDistributionByHourExtractor;

    @Inject
    private PurchaseDistributionByDayWeekMetricsWeekRepository purchaseDistributionByDayWeekMetricsWeekRepository;

    @Inject
    private PurchaseDistributionByDayWeekMetricsMonthRepository purchaseDistributionByDayWeekMetricsMonthRepository;

    @Inject
    private PurchaseDistributionByDayWeekMetricsYearRepository purchaseDistributionByDayWeekMetricsYearRepository;

    @Inject
    private PurchaseDistributionByDayWeekTransformer purchaseDistributionByDayWeekTransformer;

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void executeWithCalDate(Calendar calDate) {

        logger.debug("do transform PurchaseDistributionByDayWeekMetrics metrics transformations");

        PurchaseDistributionByDayWeekMetricsWeek pdbdwMetricsWeek = purchaseDistributionByDayWeekTransformer.transformWeek(calDate);
        PurchaseDistributionByDayWeekMetricsMonth pdbdwMetricsMonth = purchaseDistributionByDayWeekTransformer.transformMonth(calDate);
        PurchaseDistributionByDayWeekMetricsYear pdbdwMetricsYear = purchaseDistributionByDayWeekTransformer.transformYear(calDate);

        logger.debug("PurchaseDistributionByTypeMetrics transformed, now save them");

        purchaseDistributionByDayWeekMetricsWeekRepository.save(pdbdwMetricsWeek);
        purchaseDistributionByDayWeekMetricsMonthRepository.save(pdbdwMetricsMonth);
        purchaseDistributionByDayWeekMetricsYearRepository.save(pdbdwMetricsYear);

    }

}
