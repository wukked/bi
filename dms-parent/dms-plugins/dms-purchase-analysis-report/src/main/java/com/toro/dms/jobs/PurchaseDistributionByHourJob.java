package com.toro.dms.jobs;

import java.util.Calendar;

import javax.inject.Inject;

import com.toro.dms.model.*;
import com.toro.dms.repository.*;
import com.toro.dms.utils.CalendarUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.toro.dms.extractors.PurchaseDistributionByHourExtractor;
import com.toro.dms.transformers.PurchaseDistributionByHourTransformer;

@Component
//This job must be executed before PurchaseDistributionByDayWeekJob, due to PurchaseDistributionByDayWeekJob relying on this information
//Must also be done after widget data and active users reports
@Order(601)
public class PurchaseDistributionByHourJob implements Job {

    @Inject
    private PurchaseDistributionByHourExtractor purchaseDistributionByHourExtractor;

    @Inject
    private PurchaseDistributionByHourRepository purchaseDistributionByHourRepository;

    @Inject
    private PurchaseDistributionByHourMetricsWeekRepository purchaseDistributionByHourMetricsWeekRepository;

    @Inject
    private PurchaseDistributionByHourMetricsMonthRepository purchaseDistributionByHourMetricsMonthRepository;

    @Inject
    private PurchaseDistributionByHourMetricsYearRepository purchaseDistributionByHourMetricsYearRepository;

    @Inject
    private PurchaseDistributionByHourTransformer purchaseDistributionByHourTransformer;

    @Inject
    private PurchaseDistributionByDayWeekMetricsWeekRepository purchaseDistributionByDayWeekMetricsWeekRepository;

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void executeWithCalDate(Calendar calDate) {

        logger.debug("do purchase distribution by hour extractor" + calDate.getTime().toString());

        PurchaseDistributionByHour pdbhResult = purchaseDistributionByHourExtractor.extract(calDate);

        logger.debug("Get PurchaseDistributionByHour, now save them");

        if (logger.isDebugEnabled()) {
            logger.debug(pdbhResult.getDate().getTime() + "  " + pdbhResult.toString());
        }

        purchaseDistributionByHourRepository.save(pdbhResult);

        logger.debug("do transform PurchaseDistributionByHourMetrics metrics transformations");

        PurchaseDistributionByHourMetricsWeek pdbhMetricsWeek = purchaseDistributionByHourTransformer.transformWeek(calDate);
        PurchaseDistributionByHourMetricsMonth pdbhMetricsMonth = purchaseDistributionByHourTransformer.transformMonth(calDate);
        PurchaseDistributionByHourMetricsYear pdbhMetricsYear = purchaseDistributionByHourTransformer.transformYear(calDate);

        logger.debug("PurchaseDistributionByHourMetrics transformed, now save them");

        purchaseDistributionByHourMetricsWeekRepository.save(pdbhMetricsWeek);
        purchaseDistributionByHourMetricsMonthRepository.save(pdbhMetricsMonth);
        purchaseDistributionByHourMetricsYearRepository.save(pdbhMetricsYear);

    }

}
