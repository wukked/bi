package com.toro.dms.jobs;

import com.toro.dms.extractors.PurchaseDistributionByTypeExtractor;
import com.toro.dms.model.PurchaseDistributionByType;
import com.toro.dms.model.PurchaseDistributionByTypeMetricsMonth;
import com.toro.dms.model.PurchaseDistributionByTypeMetricsWeek;
import com.toro.dms.model.PurchaseDistributionByTypeMetricsYear;
import com.toro.dms.repository.PurchaseDistributionByTypeMetricsMonthRepository;
import com.toro.dms.repository.PurchaseDistributionByTypeMetricsWeekRepository;
import com.toro.dms.repository.PurchaseDistributionByTypeMetricsYearRepository;
import com.toro.dms.repository.PurchaseDistributionByTypeRepository;
import com.toro.dms.transformers.PurchaseDistributionByTypeTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.text.ParseException;
import java.util.Calendar;

@Component
@Order(600)
public class PurchaseDistributionByTypeJob implements Job {

    @Inject
    private PurchaseDistributionByTypeExtractor purchaseDistributionByTypeExtractor;

    @Inject
    private PurchaseDistributionByTypeRepository purchaseDistributionByTypeRepository;

    @Inject
    private PurchaseDistributionByTypeMetricsWeekRepository purchaseDistributionByTypeMetricsWeekRepository;

    @Inject
    private PurchaseDistributionByTypeMetricsMonthRepository purchaseDistributionByTypeMetricsMonthRepository;

    @Inject
    private PurchaseDistributionByTypeMetricsYearRepository purchaseDistributionByTypeMetricsYearRepository;

    @Inject
    private PurchaseDistributionByTypeTransformer purchaseDistributionByTypeTransformer;

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void executeWithCalDate(Calendar calDate) {

        logger.debug("do purchase distribution by type extractor" + calDate.getTime().toString());

        PurchaseDistributionByType pdbtResult = purchaseDistributionByTypeExtractor.extract(calDate);

        logger.debug("Get PurchaseDistributionByType, now save them");

        if (logger.isDebugEnabled()) {
            logger.debug(pdbtResult.getDate().getTime() + "  " + pdbtResult.toString());
        }

        purchaseDistributionByTypeRepository.save(pdbtResult);

        logger.debug("do transform PurchaseDistributionByTypeMetrics metrics transformations");

        PurchaseDistributionByTypeMetricsWeek pdbtMetricsWeek = purchaseDistributionByTypeTransformer.transformWeek(calDate);
        PurchaseDistributionByTypeMetricsMonth pdbtMetricsMonth = purchaseDistributionByTypeTransformer.transformMonth(calDate);
        PurchaseDistributionByTypeMetricsYear pdbtMetricsYear = purchaseDistributionByTypeTransformer.transformYear(calDate);

        logger.debug("PurchaseDistributionByTypeMetrics transformed, now save them");

        purchaseDistributionByTypeMetricsWeekRepository.save(pdbtMetricsWeek);
        purchaseDistributionByTypeMetricsMonthRepository.save(pdbtMetricsMonth);
        purchaseDistributionByTypeMetricsYearRepository.save(pdbtMetricsYear);

    }

}
