package com.toro.dms.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;

@Embeddable
class PurchaseDistributionByActiveUserId extends PurchaseDistributionByActiveUserTypeId implements Serializable {

    @Temporal(TemporalType.DATE)
    private Calendar date;

    PurchaseDistributionByActiveUserId(){
    }

    PurchaseDistributionByActiveUserId(Calendar date, String sir){
        super(sir);
        this.date = date;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

}

@Entity
public class PurchaseDistributionByActiveUser extends PurchaseDistributionByActiveUserType implements Serializable {

    private static final long serialVersionUID = -1113382555971337237L;

    @EmbeddedId PurchaseDistributionByActiveUserId id;

    public PurchaseDistributionByActiveUser() {
        super();
    }

    public PurchaseDistributionByActiveUser(Calendar date, String sir, long purchaseAttempts) {
        super(purchaseAttempts);
        this.id = new PurchaseDistributionByActiveUserId(date, sir);
    }

    public PurchaseDistributionByActiveUserId getId() {
        return id;
    }

    public void setId(PurchaseDistributionByActiveUserId id) {
        this.id = id;
    }

    public String getSir(){
        return this.getId().getSir();
    }

    public Calendar getDate(){
        return this.getId().getDate();
    }
}
