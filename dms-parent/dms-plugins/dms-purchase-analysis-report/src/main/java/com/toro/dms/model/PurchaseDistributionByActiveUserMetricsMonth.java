package com.toro.dms.model;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;

@Embeddable
class PurchaseDistributionByActiveUserMetricsMonthId extends PurchaseDistributionByActiveUserTypeId implements Serializable {

    private String yearMonth;

    PurchaseDistributionByActiveUserMetricsMonthId(){
        super();
    }

    PurchaseDistributionByActiveUserMetricsMonthId(String yearMonth, String sir){
        super(sir);
        this.yearMonth = yearMonth;
    }

    public String getYearMonth() {
        return yearMonth;
    }

    public void setYearMonth(String yearMonth) {
        this.yearMonth = yearMonth;
    }

}

@Entity
public class PurchaseDistributionByActiveUserMetricsMonth extends PurchaseDistributionByActiveUserType implements Serializable {

    private static final long serialVersionUID = 5451385943239894180L;

    @EmbeddedId
    PurchaseDistributionByActiveUserMetricsMonthId id;

    public PurchaseDistributionByActiveUserMetricsMonth() {
        super();
    }

    public PurchaseDistributionByActiveUserMetricsMonth(String yearMonth, String sir) {
        this();
        this.id = new PurchaseDistributionByActiveUserMetricsMonthId(yearMonth, sir);
    }

    public PurchaseDistributionByActiveUserMetricsMonth(String yearMonth, String sir, long purchaseAttempts) {
        super(purchaseAttempts);
        this.id = new PurchaseDistributionByActiveUserMetricsMonthId(yearMonth, sir);
    }

    public PurchaseDistributionByActiveUserMetricsMonthId getId() {
        return id;
    }

    public void setId(PurchaseDistributionByActiveUserMetricsMonthId id) {
        this.id = id;
    }

}
