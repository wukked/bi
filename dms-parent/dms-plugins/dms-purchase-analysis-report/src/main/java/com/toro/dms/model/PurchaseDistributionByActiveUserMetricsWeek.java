package com.toro.dms.model;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
class PurchaseDistributionByActiveUserMetricsWeekId extends PurchaseDistributionByActiveUserTypeId implements Serializable {

    private String yearWeek;

    PurchaseDistributionByActiveUserMetricsWeekId(){
        super();
    }

    PurchaseDistributionByActiveUserMetricsWeekId(String yearWeek, String sir){
        super(sir);
        this.yearWeek = yearWeek;
    }

    public String getYearWeek() {
        return yearWeek;
    }

    public void setYearWeek(String yearWeek) {
        this.yearWeek = yearWeek;
    }

}

@Entity
public class PurchaseDistributionByActiveUserMetricsWeek extends PurchaseDistributionByActiveUserType implements Serializable {

    private static final long serialVersionUID = -993703939619740552L;

    @EmbeddedId
    PurchaseDistributionByActiveUserMetricsWeekId id;

    public PurchaseDistributionByActiveUserMetricsWeek() {
        super();
    }

    public PurchaseDistributionByActiveUserMetricsWeek(String yearWeek, String sir) {
        this();
        this.id = new PurchaseDistributionByActiveUserMetricsWeekId(yearWeek, sir);
    }

    public PurchaseDistributionByActiveUserMetricsWeek(String yearWeek, String sir, long purchaseAttempts) {
        super(purchaseAttempts);
        this.id = new PurchaseDistributionByActiveUserMetricsWeekId(yearWeek, sir);
    }

    public PurchaseDistributionByActiveUserMetricsWeekId getId() {
        return id;
    }

    public void setId(PurchaseDistributionByActiveUserMetricsWeekId id) {
        this.id = id;
    }

}
