package com.toro.dms.model;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;

@Embeddable
class PurchaseDistributionByActiveUserMetricsYearId extends PurchaseDistributionByActiveUserTypeId implements Serializable {

    private String year;

    PurchaseDistributionByActiveUserMetricsYearId(){
        super();
    }

    PurchaseDistributionByActiveUserMetricsYearId(String year, String sir){
        super(sir);
        this.year = year;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

}

@Entity
public class PurchaseDistributionByActiveUserMetricsYear extends PurchaseDistributionByActiveUserType implements Serializable {

    private static final long serialVersionUID = 460268923336351544L;

    @EmbeddedId
    PurchaseDistributionByActiveUserMetricsYearId id;

    public PurchaseDistributionByActiveUserMetricsYear() {
        super();
    }

    public PurchaseDistributionByActiveUserMetricsYear(String year, String sir) {
        this();
        this.id = new PurchaseDistributionByActiveUserMetricsYearId(year, sir);
    }

    public PurchaseDistributionByActiveUserMetricsYear(String year, String sir, long purchaseAttempts) {
        super(purchaseAttempts);
        this.id = new PurchaseDistributionByActiveUserMetricsYearId(year, sir);
    }

    public PurchaseDistributionByActiveUserMetricsYearId getId() {
        return id;
    }

    public void setId(PurchaseDistributionByActiveUserMetricsYearId id) {
        this.id = id;
    }

}
