package com.toro.dms.model;

import javax.persistence.*;
import java.io.Serializable;

@MappedSuperclass
public abstract class PurchaseDistributionByActiveUserType implements Serializable {

    private long purchaseAttempts;

    public PurchaseDistributionByActiveUserType() {
    }

    public PurchaseDistributionByActiveUserType(long purchaseAttempts) {
        this();
        this.purchaseAttempts = purchaseAttempts;
    }

    public long getPurchaseAttempts() {
        return purchaseAttempts;
    }

    public void setPurchaseAttempts(long purchaseAttempts) {
        this.purchaseAttempts = purchaseAttempts;
    }



}
