package com.toro.dms.model;

import javax.persistence.Embeddable;
import javax.persistence.MappedSuperclass;

@Embeddable
@MappedSuperclass
public abstract class PurchaseDistributionByActiveUserTypeId {

    private String sir;

    PurchaseDistributionByActiveUserTypeId(){
    }

    PurchaseDistributionByActiveUserTypeId(String sir){
        this();
        this.sir = sir;
    }

    public String getSir() {
        return sir;
    }

    public void setSir(String sir) {
        this.sir = sir;
    }
}
