package com.toro.dms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class PurchaseDistributionByDayWeekMetricsMonth extends PurchaseDistributionByDayWeekType implements Serializable {

    private static final long serialVersionUID = -3873353333372222123L;
    @Id
    @Column(unique = true)
    private String yearMonth;


    public PurchaseDistributionByDayWeekMetricsMonth() {
        super();
    }

    public PurchaseDistributionByDayWeekMetricsMonth(String yearMonth) {
        this();
        this.yearMonth = yearMonth;
    }

    public PurchaseDistributionByDayWeekMetricsMonth(String yearMonth, long sunday, long monday, long tuesday, long wednesday, long thursday, long friday, long saturday) {
        super(sunday, monday, tuesday, wednesday, thursday, friday, saturday);
        this.yearMonth = yearMonth;
    }

    public String getYearMonth() {
        return this.yearMonth;
    }

    public void setYearMonth(String yearWeek) {
        this.yearMonth = yearMonth;
    }


}
