package com.toro.dms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class PurchaseDistributionByDayWeekMetricsWeek extends PurchaseDistributionByDayWeekType implements Serializable {

    private static final long serialVersionUID = -3873353355972123123L;
    @Id
    @Column(unique = true)
    private String yearWeek;

    public PurchaseDistributionByDayWeekMetricsWeek() {
        super();
    }

    public PurchaseDistributionByDayWeekMetricsWeek(String yearWeek) {
        this();
        this.yearWeek = yearWeek;
    }

    public PurchaseDistributionByDayWeekMetricsWeek(String yearWeek, long sunday, long monday, long tuesday, long wednesday, long thursday, long friday, long saturday) {
        super(sunday, monday, tuesday, wednesday, thursday, friday, saturday);
        this.yearWeek = yearWeek;
    }

    public String getYearWeek() {
        return this.yearWeek;
    }

    public void setYearWeek(String yearWeek) {
        this.yearWeek = yearWeek;
    }

}
