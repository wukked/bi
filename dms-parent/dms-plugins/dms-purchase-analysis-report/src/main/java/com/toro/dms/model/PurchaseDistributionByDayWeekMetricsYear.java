package com.toro.dms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class PurchaseDistributionByDayWeekMetricsYear extends PurchaseDistributionByDayWeekType implements Serializable {

    private static final long serialVersionUID = -3875553309972121121L;
    @Id
    @Column(unique = true)
    private String year;

    public PurchaseDistributionByDayWeekMetricsYear() {
        super();
    }

    public PurchaseDistributionByDayWeekMetricsYear(String year) {
        this();
        this.year = year;
    }

    public PurchaseDistributionByDayWeekMetricsYear(String year, long sunday, long monday, long tuesday, long wednesday, long thursday, long friday, long saturday) {
        super(sunday, monday, tuesday, wednesday, thursday, friday, saturday);
        this.year = year;
    }

    public String getYear() {
        return this.year;
    }

    public void setYear(String year) {
        this.year = year;
    }

}
