package com.toro.dms.model;

import javax.persistence.MappedSuperclass;
import java.util.Calendar;

@MappedSuperclass
public abstract class PurchaseDistributionByDayWeekType {

    private long sunday;
    private long[] sundayHourly;
    private long monday;
    private long[] mondayHourly;
    private long tuesday;
    private long[] tuesdayHourly;
    private long wednesday;
    private long[] wednesdayHourly;
    private long thursday;
    private long[] thursdayHourly;
    private long friday;
    private long[] fridayHourly;
    private long saturday;
    private long[] saturdayHourly;

    public PurchaseDistributionByDayWeekType(){
        sundayHourly = new long[24];
        mondayHourly = new long[24];
        tuesdayHourly = new long[24];
        wednesdayHourly = new long[24];
        thursdayHourly = new long[24];
        fridayHourly = new long[24];
        saturdayHourly = new long[24];
    }

    public PurchaseDistributionByDayWeekType(long sunday, long monday, long tuesday, long wednesday, long thursday, long friday, long saturday){
        this();
        this.sunday = sunday;
        this.monday = monday;
        this.tuesday = tuesday;
        this.wednesday = wednesday;
        this.thursday = thursday;
        this.friday = friday;
        this.saturday = saturday;
    }

    public long getSunday() {
        return sunday;
    }

    public void setSunday(long sunday) {
        this.sunday = sunday;
    }

    public long getMonday() {
        return monday;
    }

    public void setMonday(long monday) {
        this.monday = monday;
    }

    public long getTuesday() {
        return tuesday;
    }

    public void setTuesday(long tuesday) {
        this.tuesday = tuesday;
    }

    public long getWednesday() {
        return wednesday;
    }

    public void setWednesday(long wednesday) {
        this.wednesday = wednesday;
    }

    public long getThursday() {
        return thursday;
    }

    public void setThursday(long thursday) {
        this.thursday = thursday;
    }

    public long getFriday() {
        return friday;
    }

    public void setFriday(long friday) {
        this.friday = friday;
    }

    public long getSaturday() {
        return saturday;
    }

    public void setSaturday(long saturday) {
        this.saturday = saturday;
    }

    public long[] getSundayHourly() {
        return sundayHourly;
    }

    public void setSundayHourly(long[] sundayHourly) {
        this.sundayHourly = sundayHourly;
    }

    public long[] getMondayHourly() {
        return mondayHourly;
    }

    public void setMondayHourly(long[] mondayHourly) {
        this.mondayHourly = mondayHourly;
    }

    public long[] getTuesdayHourly() {
        return tuesdayHourly;
    }

    public void setTuesdayHourly(long[] tuesdayHourly) {
        this.tuesdayHourly = tuesdayHourly;
    }

    public long[] getWednesdayHourly() {
        return wednesdayHourly;
    }

    public void setWednesdayHourly(long[] wednesdayHourly) {
        this.wednesdayHourly = wednesdayHourly;
    }

    public long[] getThursdayHourly() {
        return thursdayHourly;
    }

    public void setThursdayHourly(long[] thursdayHourly) {
        this.thursdayHourly = thursdayHourly;
    }

    public long[] getFridayHourly() {
        return fridayHourly;
    }

    public void setFridayHourly(long[] fridayHourly) {
        this.fridayHourly = fridayHourly;
    }

    public long[] getSaturdayHourly() {
        return saturdayHourly;
    }

    public void setSaturdayHourly(long[] saturdayHourly) {
        this.saturdayHourly = saturdayHourly;
    }

    public long totalPurchaseAttempts(){
        return this.sunday + this.monday + this.tuesday + this.wednesday + this.thursday + this.friday + this.saturday;
    }

    public void setDayWeekPurchaseAttempts(int dayWeek, long purchaseAttempts){
        switch( dayWeek ){
            case Calendar.SUNDAY: this.setSunday(purchaseAttempts); break;
            case Calendar.MONDAY: this.setMonday(purchaseAttempts); break;
            case Calendar.TUESDAY: this.setTuesday(purchaseAttempts); break;
            case Calendar.WEDNESDAY: this.setWednesday(purchaseAttempts); break;
            case Calendar.THURSDAY: this.setThursday(purchaseAttempts); break;
            case Calendar.FRIDAY: this.setFriday(purchaseAttempts); break;
            case Calendar.SATURDAY: this.setSaturday(purchaseAttempts); break;
        }
    }

    public long getDayWeekPurchaseAttempts(int dayWeek){
        switch( dayWeek ){
            case Calendar.SUNDAY: return this.getSunday();
            case Calendar.MONDAY: return this.getMonday();
            case Calendar.TUESDAY: return this.getTuesday();
            case Calendar.WEDNESDAY: return this.getWednesday();
            case Calendar.THURSDAY: return this.getThursday();
            case Calendar.FRIDAY: return this.getFriday();
            case Calendar.SATURDAY: return this.getSaturday();
            default: return 0;
        }
    }

    public void setDayWeekHours(int dayWeek, long[] hours){
        switch( dayWeek ){
            case Calendar.SUNDAY: this.setSundayHourly(hours); break;
            case Calendar.MONDAY: this.setMondayHourly(hours); break;
            case Calendar.TUESDAY: this.setTuesdayHourly(hours); break;
            case Calendar.WEDNESDAY: this.setWednesdayHourly(hours); break;
            case Calendar.THURSDAY: this.setThursdayHourly(hours); break;
            case Calendar.FRIDAY: this.setFridayHourly(hours); break;
            case Calendar.SATURDAY: this.setSaturdayHourly(hours); break;
        }
    }

    public long[] getDayWeekHours(int dayWeek){
        switch( dayWeek ){
            case Calendar.SUNDAY: return this.getSundayHourly();
            case Calendar.MONDAY: return this.getMondayHourly();
            case Calendar.TUESDAY: return this.getTuesdayHourly();
            case Calendar.WEDNESDAY: return this.getWednesdayHourly();
            case Calendar.THURSDAY: return this.getThursdayHourly();
            case Calendar.FRIDAY: return this.getFridayHourly();
            case Calendar.SATURDAY: return this.getSaturdayHourly();
            default: return new long[0];
        }
    }

    public long[] dayOfWeekToArray(){
        long[] hoursArray = new long[7];
        hoursArray[0] = this.sunday;
        hoursArray[1] = this.monday;
        hoursArray[2] = this.tuesday;
        hoursArray[3] = this.wednesday;
        hoursArray[4] = this.thursday;
        hoursArray[5] = this.friday;
        hoursArray[6] = this.saturday;
        return hoursArray;
    }

    public double sundayPercent(){
        return (double)this.getSunday()/(double)totalPurchaseAttempts();
    }

    public double mondayPercent(){
        return (double)this.getMonday()/(double)totalPurchaseAttempts();
    }

    public double tuesdayPercent(){
        return (double)this.getTuesday()/(double)totalPurchaseAttempts();
    }

    public double wednesdayPercent(){
        return (double)this.getWednesday()/(double)totalPurchaseAttempts();
    }

    public double thursdayPercent(){
        return (double)this.getThursday()/(double)totalPurchaseAttempts();
    }

    public double fridayPercent(){
        return (double)this.getFriday()/(double)totalPurchaseAttempts();
    }

    public double saturdayPercent(){
        return (double)this.getSaturday()/(double)totalPurchaseAttempts();
    }

}
