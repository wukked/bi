package com.toro.dms.model;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Calendar;

@Entity
public class PurchaseDistributionByFrequencyMetricsDay implements Serializable {

    private static final long serialVersionUID = 7026887789607285606L;

    @Temporal(TemporalType.DATE)
    @Id
    @Column(unique = true)
    private Calendar date;
    private long totalActiveUsers;
    private long totalPurchaseAttempts;
    private long purchaseFrequency1;
    private long purchaseFrequency2;
    private long purchaseFrequency3;
    private long purchaseFrequency4;
    private long purchaseFrequency5;
    private long purchaseFrequency6;
    private long purchaseFrequency7;
    private long purchaseFrequency8;
    private long purchaseFrequency9;
    private long purchaseFrequency10;
    private long purchaseFrequencyGreaterThan10;

    public PurchaseDistributionByFrequencyMetricsDay() {
    }

    public PurchaseDistributionByFrequencyMetricsDay(Calendar date, long totalActiveUsers, long totalPurchaseAttempts, long[] purchaseFrequencyArray) {
        this();
        this.date = date;
        this.totalActiveUsers = totalActiveUsers;
        this.totalPurchaseAttempts = totalPurchaseAttempts;
        this.purchaseFrequency1 = purchaseFrequencyArray[0];
        this.purchaseFrequency2 = purchaseFrequencyArray[1];
        this.purchaseFrequency3 = purchaseFrequencyArray[2];
        this.purchaseFrequency4 = purchaseFrequencyArray[3];
        this.purchaseFrequency5 = purchaseFrequencyArray[4];
        this.purchaseFrequency6 = purchaseFrequencyArray[5];
        this.purchaseFrequency7 = purchaseFrequencyArray[6];
        this.purchaseFrequency8 = purchaseFrequencyArray[7];
        this.purchaseFrequency9 = purchaseFrequencyArray[8];
        this.purchaseFrequency10 = purchaseFrequencyArray[9];
        this.purchaseFrequencyGreaterThan10 = purchaseFrequencyArray[10];
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public long getTotalActiveUsers() {
        return totalActiveUsers;
    }

    public void setTotalActiveUsers(long totalActiveUsers) {
        this.totalActiveUsers = totalActiveUsers;
    }

    public long getTotalPurchaseAttempts() {
        return totalPurchaseAttempts;
    }

    public void setTotalPurchaseAttempts(long totalPurchaseAttempts) {
        this.totalPurchaseAttempts = totalPurchaseAttempts;
    }

    public long getPurchaseFrequency1() {
        return purchaseFrequency1;
    }

    public void setPurchaseFrequency1(long purchaseFrequency1) {
        this.purchaseFrequency1 = purchaseFrequency1;
    }

    public long getPurchaseFrequency2() {
        return purchaseFrequency2;
    }

    public void setPurchaseFrequency2(long purchaseFrequency2) {
        this.purchaseFrequency2 = purchaseFrequency2;
    }

    public long getPurchaseFrequency3() {
        return purchaseFrequency3;
    }

    public void setPurchaseFrequency3(long purchaseFrequency3) {
        this.purchaseFrequency3 = purchaseFrequency3;
    }

    public long getPurchaseFrequency4() {
        return purchaseFrequency4;
    }

    public void setPurchaseFrequency4(long purchaseFrequency4) {
        this.purchaseFrequency4 = purchaseFrequency4;
    }

    public long getPurchaseFrequency5() {
        return purchaseFrequency5;
    }

    public void setPurchaseFrequency5(long purchaseFrequency5) {
        this.purchaseFrequency5 = purchaseFrequency5;
    }

    public long getPurchaseFrequency6() {
        return purchaseFrequency6;
    }

    public void setPurchaseFrequency6(long purchaseFrequency6) {
        this.purchaseFrequency6 = purchaseFrequency6;
    }

    public long getPurchaseFrequency7() {
        return purchaseFrequency7;
    }

    public void setPurchaseFrequency7(long purchaseFrequency7) {
        this.purchaseFrequency7 = purchaseFrequency7;
    }

    public long getPurchaseFrequency8() {
        return purchaseFrequency8;
    }

    public void setPurchaseFrequency8(long purchaseFrequency8) {
        this.purchaseFrequency8 = purchaseFrequency8;
    }

    public long getPurchaseFrequency9() {
        return purchaseFrequency9;
    }

    public void setPurchaseFrequency9(long purchaseFrequency9) {
        this.purchaseFrequency9 = purchaseFrequency9;
    }

    public long getPurchaseFrequency10() {
        return purchaseFrequency10;
    }

    public void setPurchaseFrequency10(long purchaseFrequency10) {
        this.purchaseFrequency10 = purchaseFrequency10;
    }

    public long getPurchaseFrequencyGreaterThan10() {
        return purchaseFrequencyGreaterThan10;
    }

    public void setPurchaseFrequencyGreaterThan10(long purchaseFrequencyGreaterThan10) {
        this.purchaseFrequencyGreaterThan10 = purchaseFrequencyGreaterThan10;
    }

}
