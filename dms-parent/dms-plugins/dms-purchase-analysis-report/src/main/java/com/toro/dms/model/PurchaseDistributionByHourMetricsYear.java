package com.toro.dms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class PurchaseDistributionByHourMetricsYear extends PurchaseDistributionByHourType implements Serializable {

    private static final long serialVersionUID = -4113353304321877707L;
    @Id
    @Column(unique = true)
    private String year;

    public PurchaseDistributionByHourMetricsYear() {
        super();
    }

    public PurchaseDistributionByHourMetricsYear(String year) {
        this();
        this.year = year;
    }

    public PurchaseDistributionByHourMetricsYear(String year,
                                                 long hour00,
                                                 long hour01,
                                                 long hour02,
                                                 long hour03,
                                                 long hour04,
                                                 long hour05,
                                                 long hour06,
                                                 long hour07,
                                                 long hour08,
                                                 long hour09,
                                                 long hour10,
                                                 long hour11,
                                                 long hour12,
                                                 long hour13,
                                                 long hour14,
                                                 long hour15,
                                                 long hour16,
                                                 long hour17,
                                                 long hour18,
                                                 long hour19,
                                                 long hour20,
                                                 long hour21,
                                                 long hour22,
                                                 long hour23
    ) {
        super(hour00, hour01, hour02, hour03, hour04, hour05, hour06, hour07, hour08, hour09, hour10, hour11, hour12, hour13, hour14, hour15, hour16, hour17, hour18, hour19, hour20, hour21, hour22, hour23);
        this.year = year;
    }

    public String getYear() {
        return this.year;
    }

    public void setYear(String year) {
        this.year = year;
    }

}
