package com.toro.dms.model;

import javax.persistence.MappedSuperclass;
import java.util.Calendar;

@MappedSuperclass
public abstract class PurchaseDistributionByHourType {

    private long hour00;
    private long hour01;
    private long hour02;
    private long hour03;
    private long hour04;
    private long hour05;
    private long hour06;
    private long hour07;
    private long hour08;
    private long hour09;
    private long hour10;
    private long hour11;
    private long hour12;
    private long hour13;
    private long hour14;
    private long hour15;
    private long hour16;
    private long hour17;
    private long hour18;
    private long hour19;
    private long hour20;
    private long hour21;
    private long hour22;
    private long hour23;

    public PurchaseDistributionByHourType() {
    }

    public PurchaseDistributionByHourType(String test) {
    }

    public PurchaseDistributionByHourType(long hour00, long hour01, long hour02, long hour03, long hour04, long hour05, long hour06,long hour07,long hour08, long hour09, long hour10, long hour11, long hour12, long hour13, long hour14, long hour15, long hour16, long hour17, long hour18, long hour19, long hour20, long hour21, long hour22, long hour23) {
        this();
        this.hour00 = hour00;
        this.hour01 = hour01;
        this.hour02 = hour02;
        this.hour03 = hour03;
        this.hour04 = hour04;
        this.hour05 = hour05;
        this.hour06 = hour06;
        this.hour07 = hour07;
        this.hour08 = hour08;
        this.hour09 = hour09;
        this.hour10 = hour10;
        this.hour11 = hour11;
        this.hour12 = hour12;
        this.hour13 = hour13;
        this.hour14 = hour14;
        this.hour15 = hour15;
        this.hour16 = hour16;
        this.hour17 = hour17;
        this.hour18 = hour18;
        this.hour19 = hour19;
        this.hour20 = hour20;
        this.hour21 = hour21;
        this.hour22 = hour22;
        this.hour23 = hour23;
    }

    public long getHour00() {
        return hour00;
    }

    public void setHour00(long hour00) {
        this.hour00 = hour00;
    }

    public long getHour01() {
        return hour01;
    }

    public void setHour01(long hour01) {
        this.hour01 = hour01;
    }

    public long getHour02() {
        return hour02;
    }

    public void setHour02(long hour02) {
        this.hour02 = hour02;
    }

    public long getHour03() {
        return hour03;
    }

    public void setHour03(long hour03) {
        this.hour03 = hour03;
    }

    public long getHour04() {
        return hour04;
    }

    public void setHour04(long hour04) {
        this.hour04 = hour04;
    }

    public long getHour05() {
        return hour05;
    }

    public void setHour05(long hour05) {
        this.hour05 = hour05;
    }

    public long getHour06() {
        return hour06;
    }

    public void setHour06(long hour06) {
        this.hour06 = hour06;
    }

    public long getHour07() {
        return hour07;
    }

    public void setHour07(long hour07) {
        this.hour07 = hour07;
    }

    public long getHour08() {
        return hour08;
    }

    public void setHour08(long hour08) {
        this.hour08 = hour08;
    }

    public long getHour09() {
        return hour09;
    }

    public void setHour09(long hour09) {
        this.hour09 = hour09;
    }

    public long getHour10() {
        return hour10;
    }

    public void setHour10(long hour10) {
        this.hour10 = hour10;
    }

    public long getHour11() {
        return hour11;
    }

    public void setHour11(long hour11) {
        this.hour11 = hour11;
    }

    public long getHour12() {
        return hour12;
    }

    public void setHour12(long hour12) {
        this.hour12 = hour12;
    }

    public long getHour13() {
        return hour13;
    }

    public void setHour13(long hour13) {
        this.hour13 = hour13;
    }

    public long getHour14() {
        return hour14;
    }

    public void setHour14(long hour14) {
        this.hour14 = hour14;
    }

    public long getHour15() {
        return hour15;
    }

    public void setHour15(long hour15) {
        this.hour15 = hour15;
    }

    public long getHour16() {
        return hour16;
    }

    public void setHour16(long hour16) {
        this.hour16 = hour16;
    }

    public long getHour17() {
        return hour17;
    }

    public void setHour17(long hour17) {
        this.hour17 = hour17;
    }

    public long getHour18() {
        return hour18;
    }

    public void setHour18(long hour18) {
        this.hour18 = hour18;
    }

    public long getHour19() {
        return hour19;
    }

    public void setHour19(long hour19) {
        this.hour19 = hour19;
    }

    public long getHour20() {
        return hour20;
    }

    public void setHour20(long hour20) {
        this.hour20 = hour20;
    }

    public long getHour21() {
        return hour21;
    }

    public void setHour21(long hour21) {
        this.hour21 = hour21;
    }

    public long getHour22() {
        return hour22;
    }

    public void setHour22(long hour22) {
        this.hour22 = hour22;
    }

    public long getHour23() {
        return hour23;
    }

    public void setHour23(long hour23) {
        this.hour23 = hour23;
    }

    public double hour00Percent(){
        return (double)this.getHour00()/(double)totalPurchaseAttempts();
    }

    public double hour01Percent(){
        return (double)this.getHour01()/(double)totalPurchaseAttempts();
    }

    public double hour02Percent(){
        return (double)this.getHour02()/(double)totalPurchaseAttempts();
    }

    public double hour03Percent(){
        return (double)this.getHour03()/(double)totalPurchaseAttempts();
    }

    public double hour04Percent(){
        return (double)this.getHour04()/(double)totalPurchaseAttempts();
    }

    public double hour05Percent(){
        return (double)this.getHour05()/(double)totalPurchaseAttempts();
    }

    public double hour06Percent(){
        return (double)this.getHour06()/(double)totalPurchaseAttempts();
    }

    public double hour07Percent(){
        return (double)this.getHour07()/(double)totalPurchaseAttempts();
    }

    public double hour08Percent(){
        return (double)this.getHour08()/(double)totalPurchaseAttempts();
    }

    public double hour09Percent(){
        return (double)this.getHour09()/(double)totalPurchaseAttempts();
    }

    public double hour10Percent(){
        return (double)this.getHour10()/(double)totalPurchaseAttempts();
    }

    public double hour11Percent(){
        return (double)this.getHour11()/(double)totalPurchaseAttempts();
    }

    public double hour12Percent(){
        return (double)this.getHour12()/(double)totalPurchaseAttempts();
    }

    public double hour13Percent(){
        return (double)this.getHour13()/(double)totalPurchaseAttempts();
    }

    public double hour14Percent(){
        return (double)this.getHour14()/(double)totalPurchaseAttempts();
    }

    public double hour15Percent(){
        return (double)this.getHour15()/(double)totalPurchaseAttempts();
    }

    public double hour16Percent(){
        return (double)this.getHour16()/(double)totalPurchaseAttempts();
    }

    public double hour17Percent(){
        return (double)this.getHour17()/(double)totalPurchaseAttempts();
    }

    public double hour18Percent(){
        return (double)this.getHour18()/(double)totalPurchaseAttempts();
    }

    public double hour19Percent(){
        return (double)this.getHour19()/(double)totalPurchaseAttempts();
    }

    public double hour20Percent(){
        return (double)this.getHour20()/(double)totalPurchaseAttempts();
    }

    public double hour21Percent(){
        return (double)this.getHour21()/(double)totalPurchaseAttempts();
    }

    public double hour22Percent(){
        return (double)this.getHour22()/(double)totalPurchaseAttempts();
    }

    public double hour23Percent(){
        return (double)this.getHour23()/(double)totalPurchaseAttempts();
    }

    public long totalPurchaseAttempts() {
        return this.hour00 +
                this.hour01 +
                this.hour02 +
                this.hour03 +
                this.hour04 +
                this.hour05 +
                this.hour06 +
                this.hour07 +
                this.hour08 +
                this.hour09 +
                this.hour10 +
                this.hour11 +
                this.hour12 +
                this.hour13 +
                this.hour14 +
                this.hour15 +
                this.hour16 +
                this.hour17 +
                this.hour18 +
                this.hour19 +
                this.hour20 +
                this.hour21 +
                this.hour22 +
                this.hour23;
    }

    public long[] hoursToArray() {
        long[] hoursArray = new long[24];

        hoursArray[0] = this.hour00;
        hoursArray[1] = this.hour01;
        hoursArray[2] = this.hour02;
        hoursArray[3] = this.hour03;
        hoursArray[4] = this.hour04;
        hoursArray[5] = this.hour05;
        hoursArray[6] = this.hour06;
        hoursArray[7] = this.hour07;
        hoursArray[8] = this.hour08;
        hoursArray[9] = this.hour09;
        hoursArray[10] = this.hour10;
        hoursArray[11] = this.hour11;
        hoursArray[12] = this.hour12;
        hoursArray[13] = this.hour13;
        hoursArray[14] = this.hour14;
        hoursArray[15] = this.hour15;
        hoursArray[16] = this.hour16;
        hoursArray[17] = this.hour17;
        hoursArray[18] = this.hour18;
        hoursArray[19] = this.hour19;
        hoursArray[20] = this.hour20;
        hoursArray[21] = this.hour21;
        hoursArray[22] = this.hour22;
        hoursArray[23] = this.hour23;

        return hoursArray;
    }

    public double[] hoursPercentToArray() {
        double[] hoursPercentArray = new double[24];

        hoursPercentArray[0] = this.hour00Percent();
        hoursPercentArray[1] = this.hour01Percent();
        hoursPercentArray[2] = this.hour02Percent();
        hoursPercentArray[3] = this.hour03Percent();
        hoursPercentArray[4] = this.hour04Percent();
        hoursPercentArray[5] = this.hour05Percent();
        hoursPercentArray[6] = this.hour06Percent();
        hoursPercentArray[7] = this.hour07Percent();
        hoursPercentArray[8] = this.hour08Percent();
        hoursPercentArray[9] = this.hour09Percent();
        hoursPercentArray[10] = this.hour10Percent();
        hoursPercentArray[11] = this.hour11Percent();
        hoursPercentArray[12] = this.hour12Percent();
        hoursPercentArray[13] = this.hour13Percent();
        hoursPercentArray[14] = this.hour14Percent();
        hoursPercentArray[15] = this.hour15Percent();
        hoursPercentArray[16] = this.hour16Percent();
        hoursPercentArray[17] = this.hour17Percent();
        hoursPercentArray[18] = this.hour18Percent();
        hoursPercentArray[19] = this.hour19Percent();
        hoursPercentArray[20] = this.hour20Percent();
        hoursPercentArray[21] = this.hour21Percent();
        hoursPercentArray[22] = this.hour22Percent();
        hoursPercentArray[23] = this.hour23Percent();

        return hoursPercentArray;
    }


}
