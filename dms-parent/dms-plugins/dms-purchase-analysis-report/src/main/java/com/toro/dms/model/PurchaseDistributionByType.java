package com.toro.dms.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;

@Entity
public class PurchaseDistributionByType extends PurchaseDistributionByTypeType implements Serializable{

	private static final long serialVersionUID = -1234567809972877717L;
	@Id
	@Column(unique=true)
    @Temporal(TemporalType.DATE)
    private Calendar date;


	public PurchaseDistributionByType() {
		super();
	}

	public PurchaseDistributionByType(Calendar date, long offlineApproved, long offlineDeclined, long online, long tapPinTap){
		super(offlineApproved, offlineDeclined, online, tapPinTap);
		this.date = date;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

}
