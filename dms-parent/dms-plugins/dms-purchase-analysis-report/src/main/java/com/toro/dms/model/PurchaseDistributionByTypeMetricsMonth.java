package com.toro.dms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class PurchaseDistributionByTypeMetricsMonth extends PurchaseDistributionByTypeType implements Serializable {

    private static final long serialVersionUID = -4812312309972123707L;
    @Id
    @Column(unique = true)
    private String yearMonth;

    public PurchaseDistributionByTypeMetricsMonth() {
        super();
    }

    public PurchaseDistributionByTypeMetricsMonth(String yearMonth) {
        this();
        this.yearMonth = yearMonth;
    }

    public String getYearMonth() {
        return yearMonth;
    }

    public void setYearMonth(String yearMonth) {
        this.yearMonth = yearMonth;
    }

}
