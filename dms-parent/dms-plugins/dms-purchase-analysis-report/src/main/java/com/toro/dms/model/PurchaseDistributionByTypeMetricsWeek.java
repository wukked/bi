package com.toro.dms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class PurchaseDistributionByTypeMetricsWeek extends PurchaseDistributionByTypeType implements Serializable {

    private static final long serialVersionUID = -1871233123452877707L;
    @Id
    @Column(unique = true)
    private String yearWeek;

    public PurchaseDistributionByTypeMetricsWeek() {
        super();
    }

    public PurchaseDistributionByTypeMetricsWeek(String yearWeek) {
        this();
        this.yearWeek = yearWeek;
    }

    public String getYearWeek() {
        return yearWeek;
    }

    public void setYearWeek(String yearWeek) {
        this.yearWeek = yearWeek;
    }

}
