package com.toro.dms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class PurchaseDistributionByTypeMetricsYear extends PurchaseDistributionByTypeType implements Serializable {

    private static final long serialVersionUID = -1233312449972812307L;
    @Id
    @Column(unique = true)
    private String year;

    public PurchaseDistributionByTypeMetricsYear() {
        super();
    }

    public PurchaseDistributionByTypeMetricsYear(String year) {
        this();
        this.year = year;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

}
