package com.toro.dms.model;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class PurchaseDistributionByTypeType {

    private long offlineApproved;
    private long offlineDeclined;
    private long online;
    private long tapPinTap;

    public PurchaseDistributionByTypeType() {
    }

    public PurchaseDistributionByTypeType(long offlineApproved, long offlineDeclined, long online, long tapPinTap){
        this();
        this.offlineApproved = offlineApproved;
        this.offlineDeclined = offlineDeclined;
        this.online = online;
        this.tapPinTap = tapPinTap;
    }

    public long getOfflineApproved() {
        return offlineApproved;
    }

    public void setOfflineApproved(long offlineApproved) {
        this.offlineApproved = offlineApproved;
    }

    public long getOfflineDeclined() {
        return offlineDeclined;
    }

    public void setOfflineDeclined(long offlineDeclined) {
        this.offlineDeclined = offlineDeclined;
    }

    public long getOnline() {
        return online;
    }

    public void setOnline(long online) {
        this.online = online;
    }

    public long getTapPinTap() {
        return tapPinTap;
    }

    public void setTapPinTap(long tapPinTap) {
        this.tapPinTap = tapPinTap;
    }

    public long totalPurchaseAttempts(){
        return this.offlineApproved + this.offlineDeclined + this.online + this.tapPinTap;
    }

    public double offlineApprovedPercent(){
        return (double)this.getOfflineApproved()/(double)totalPurchaseAttempts();
    }

    public double offlineDeclinedPercent(){
        return (double)this.getOfflineDeclined()/(double)totalPurchaseAttempts();
    }

    public double onlinePercent(){
        return (double)this.getOnline()/(double)totalPurchaseAttempts();
    }

    public double tapPinTapPercent(){
        return (double)this.getTapPinTap()/(double)totalPurchaseAttempts();
    }



}
