package com.toro.dms.repository;

import com.toro.dms.model.PurchaseDistributionByActiveUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Calendar;
import java.util.List;

public interface PurchaseDistributionByActiveUserDataRepository extends CrudRepository<PurchaseDistributionByActiveUser, String> {


    @Query("SELECT au.id.sir, SUM(au.purchaseAttempts) FROM PurchaseDistributionByActiveUser AS au WHERE au.id.date >= :startDate AND au.id.date <= :endDate GROUP BY au.id.sir")
    public List<Object[]> accumulateActivity(@Param("startDate") Calendar startDate,
                                             @Param("endDate") Calendar endDate);

    //active users population
    @Query("SELECT COUNT(DISTINCT au.id.sir) FROM PurchaseDistributionByActiveUser au WHERE au.id.date >= :startDate AND au.id.date <= :endDate")
    public long activeUserPopulationBetweenDates(@Param("startDate") Calendar startDate,
                                                 @Param("endDate") Calendar endDate);

    //total purchase attempts
    @Query("SELECT SUM(au.purchaseAttempts) FROM PurchaseDistributionByActiveUser au WHERE au.id.date >= :startDate AND au.id.date <= :endDate")
    public long totalPurchaseAttempts(@Param("startDate") Calendar startDate,
                                      @Param("endDate") Calendar endDate);

    public List<PurchaseDistributionByActiveUser> findByDate(Calendar date);
    //public List<PurchaseDistributionByActiveUser> findByDateBetween(Calendar start, Calendar end);

}
