package com.toro.dms.repository;

import com.toro.dms.model.PurchaseDistributionByActiveUserMetricsMonth;
import org.springframework.data.repository.CrudRepository;

public interface PurchaseDistributionByActiveUserMetricsMonthRepository extends CrudRepository<PurchaseDistributionByActiveUserMetricsMonth, String> {

}
