package com.toro.dms.repository;

import com.toro.dms.model.PurchaseDistributionByActiveUserMetricsWeek;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Calendar;
import java.util.List;

public interface PurchaseDistributionByActiveUserMetricsWeekRepository extends CrudRepository<PurchaseDistributionByActiveUserMetricsWeek, String> {

}
