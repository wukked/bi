package com.toro.dms.repository;

import com.toro.dms.model.PurchaseDistributionByActiveUserMetricsYear;
import org.springframework.data.repository.CrudRepository;

public interface PurchaseDistributionByActiveUserMetricsYearRepository extends CrudRepository<PurchaseDistributionByActiveUserMetricsYear, String> {

}
