package com.toro.dms.repository;

import com.toro.dms.model.PurchaseDistributionByDayWeekMetricsMonth;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PurchaseDistributionByDayWeekMetricsMonthRepository extends CrudRepository<PurchaseDistributionByDayWeekMetricsMonth, Long> {

    public List<PurchaseDistributionByDayWeekMetricsMonth> findByYearMonth(String yearMonth);

}
