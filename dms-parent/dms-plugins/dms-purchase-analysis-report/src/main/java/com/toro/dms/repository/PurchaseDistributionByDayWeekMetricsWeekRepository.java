package com.toro.dms.repository;

import com.toro.dms.model.PurchaseDistributionByDayWeekMetricsWeek;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PurchaseDistributionByDayWeekMetricsWeekRepository extends CrudRepository<PurchaseDistributionByDayWeekMetricsWeek, Long> {

    public List<PurchaseDistributionByDayWeekMetricsWeek> findByYearWeek(String yearWeek);

}
