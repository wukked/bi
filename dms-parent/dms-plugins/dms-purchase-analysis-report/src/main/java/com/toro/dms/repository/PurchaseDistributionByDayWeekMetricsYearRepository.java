package com.toro.dms.repository;

import com.toro.dms.model.PurchaseDistributionByDayWeekMetricsYear;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PurchaseDistributionByDayWeekMetricsYearRepository extends CrudRepository<PurchaseDistributionByDayWeekMetricsYear, Long> {

    public List<PurchaseDistributionByDayWeekMetricsYear> findByYear(String year);

}
