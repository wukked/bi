package com.toro.dms.repository;

import com.toro.dms.model.PurchaseDistributionByFrequencyMetricsDay;
import org.springframework.data.repository.CrudRepository;

public interface PurchaseDistributionByFrequencyMetricsDayRepository extends CrudRepository<PurchaseDistributionByFrequencyMetricsDay, String> {


}
