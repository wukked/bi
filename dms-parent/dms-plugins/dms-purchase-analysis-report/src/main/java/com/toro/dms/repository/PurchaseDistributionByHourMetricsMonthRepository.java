package com.toro.dms.repository;

import com.toro.dms.model.PurchaseDistributionByHourMetricsMonth;
import com.toro.dms.model.PurchaseDistributionByHourMetricsWeek;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PurchaseDistributionByHourMetricsMonthRepository extends CrudRepository<PurchaseDistributionByHourMetricsMonth, Long> {

    public List<PurchaseDistributionByHourMetricsMonth> findByYearMonth(String yearMonth);

}
