package com.toro.dms.repository;

import com.toro.dms.model.PurchaseDistributionByHour;
import com.toro.dms.model.PurchaseDistributionByHourMetricsWeek;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface PurchaseDistributionByHourMetricsWeekRepository extends CrudRepository<PurchaseDistributionByHourMetricsWeek, Long> {

    public List<PurchaseDistributionByHourMetricsWeek> findByYearWeek(String yearWeek);

}
