package com.toro.dms.repository;

import com.toro.dms.model.PurchaseDistributionByHourMetricsYear;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PurchaseDistributionByHourMetricsYearRepository extends CrudRepository<PurchaseDistributionByHourMetricsYear, Long> {

    public List<PurchaseDistributionByHourMetricsYear> findByYear(String year);

}
