package com.toro.dms.repository;

import com.toro.dms.model.PurchaseDistributionByHour;
import org.springframework.data.repository.CrudRepository;

import java.util.Calendar;
import java.util.List;

public interface PurchaseDistributionByHourRepository extends CrudRepository<PurchaseDistributionByHour, String> {

    public List<PurchaseDistributionByHour> findByDate(Calendar date);
    public List<PurchaseDistributionByHour> findByDateBetween(Calendar start, Calendar end);

}
