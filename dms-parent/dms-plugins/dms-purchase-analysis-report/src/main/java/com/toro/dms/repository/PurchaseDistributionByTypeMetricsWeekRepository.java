package com.toro.dms.repository;

import com.toro.dms.model.PurchaseDistributionByTypeMetricsWeek;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PurchaseDistributionByTypeMetricsWeekRepository extends CrudRepository<PurchaseDistributionByTypeMetricsWeek, Long> {

    public List<PurchaseDistributionByTypeMetricsWeek> findByYearWeek(String yearWeek);

}
