package com.toro.dms.repository;

import com.toro.dms.model.PurchaseDistributionByTypeMetricsYear;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PurchaseDistributionByTypeMetricsYearRepository extends CrudRepository<PurchaseDistributionByTypeMetricsYear, Long> {

    public List<PurchaseDistributionByTypeMetricsYear> findByYear(String year);

}
