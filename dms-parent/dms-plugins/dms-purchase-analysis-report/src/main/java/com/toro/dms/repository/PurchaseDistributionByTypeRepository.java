package com.toro.dms.repository;

import com.toro.dms.model.PurchaseDistributionByType;
import org.springframework.data.repository.CrudRepository;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public interface PurchaseDistributionByTypeRepository extends CrudRepository<PurchaseDistributionByType, String> {

    public List<PurchaseDistributionByType> findByDate(Calendar date);
    public List<PurchaseDistributionByType> findByDateBetween(Calendar start, Calendar end);

}
