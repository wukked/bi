package com.toro.dms.rest;

import com.toro.dms.model.*;
import com.toro.dms.repository.*;
import com.toro.dms.utils.CalendarUtil;
import com.toro.dms.utils.CommonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;
import java.util.List;

@RequestMapping("/purchaseAnalysis/purchaseDistributionByDayWeek")
@RestController
public class PurchaseDistributionByDayWeekController {

    private static final Logger logger = LoggerFactory.getLogger(PurchaseDistributionByDayWeekController.class);

    @Inject
    PurchaseDistributionByDayWeekMetricsWeekRepository purchaseDistributionByDayWeekMetricsWeekRepository;

    @Inject
    PurchaseDistributionByDayWeekMetricsMonthRepository purchaseDistributionByDayWeekMetricsMonthRepository;

    @Inject
    PurchaseDistributionByDayWeekMetricsYearRepository purchaseDistributionByDayWeekMetricsYearsRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getCurrentWeek")
    public String getCurrentWeek(HttpServletResponse response) {

        logger.debug("do get current week");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByDayWeek_WeekReport.csv");
        Calendar now = Calendar.getInstance();
        List<PurchaseDistributionByDayWeekMetricsWeek> rs = purchaseDistributionByDayWeekMetricsWeekRepository.findByYearWeek(CalendarUtil.createYearWeekFormat(now));
        String csv = createCsvHeaderTitles();
        for (PurchaseDistributionByDayWeekMetricsWeek pdbdwMetricWeek : rs) {
            csv += createCsvRow(pdbdwMetricWeek);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllWeeks")
    public String getAllWeeks(HttpServletResponse response) {

        logger.debug("do get all weeks");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByDayWeek_AllWeekReport.csv");
        Iterable<PurchaseDistributionByDayWeekMetricsWeek> rs = purchaseDistributionByDayWeekMetricsWeekRepository.findAll();
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByDayWeekMetricsWeek pdbdwMetricWeek : rs) {
            csv += createCsvRow(pdbdwMetricWeek);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getCurrentMonth")
    public String getCurrentMonth(HttpServletResponse response) {

        logger.debug("do get month");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByDayWeek_MonthReport.csv");
        Calendar now = Calendar.getInstance();
        List<PurchaseDistributionByDayWeekMetricsMonth> rs = purchaseDistributionByDayWeekMetricsMonthRepository.findByYearMonth(CalendarUtil.createYearMonthFormat(now));
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByDayWeekMetricsMonth pdbdwMetricMonth : rs) {
            csv += createCsvRow(pdbdwMetricMonth);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllMonths")
    public String getAllMonths(HttpServletResponse response) {

        logger.debug("do get all months");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByDayWeek_AllMonthsReport.csv");
        Iterable<PurchaseDistributionByDayWeekMetricsMonth> rs = purchaseDistributionByDayWeekMetricsMonthRepository.findAll();
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByDayWeekMetricsMonth pdbdwMetricMonth : rs) {
            csv += createCsvRow(pdbdwMetricMonth);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getCurrentYear")
    public String getCurrentYear(HttpServletResponse response) {

        logger.debug("do get current year");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByDayWeek_YearReport.csv");
        Calendar now = Calendar.getInstance();
        List<PurchaseDistributionByDayWeekMetricsYear> rs = purchaseDistributionByDayWeekMetricsYearsRepository.findByYear(CalendarUtil.createYearFormat(now));
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByDayWeekMetricsYear pdbdwMetricYear : rs) {
            csv += createCsvRow(pdbdwMetricYear);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllYears")
    public String getAllYears(HttpServletResponse response) {

        logger.debug("do get all years");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByDayWeek_AllYearReport.csv");
        Iterable<PurchaseDistributionByDayWeekMetricsYear> rs = purchaseDistributionByDayWeekMetricsYearsRepository.findAll();
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByDayWeekMetricsYear pdbdwMetricYear : rs) {
            csv += createCsvRow(pdbdwMetricYear);
        }

        return csv;
    }

    private void setCsvResponseHeader(HttpServletResponse response, String fileName) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
        response.setContentType("application/x-download");
        response.addHeader("Content-Disposition", "filename=\"" + fileName + "\"");
    }

    private String createCsvHeaderTitles() {
        return "Time," +
                "Purchase attempts," +
                "Monday," +
                "Monday %," +
                "Tuesday," +
                "Tuesday %," +
                "Wednesday," +
                "Wednesday %," +
                "Thursday," +
                "Thursday %," +
                "Friday," +
                "Friday %," +
                "Saturday," +
                "Saturday %," +
                "Sunday," +
                "Sunday %" +
                "\r\n";
    }

    private String createCsvRow(PurchaseDistributionByDayWeekType rs) {
        logger.debug("Create CSV row");

        String csvRow = "";
        String time = "";

        if (rs instanceof PurchaseDistributionByDayWeekMetricsWeek) {
            time = ((PurchaseDistributionByDayWeekMetricsWeek) rs).getYearWeek().toString();
        } else if (rs instanceof PurchaseDistributionByDayWeekMetricsMonth) {
            time = ((PurchaseDistributionByDayWeekMetricsMonth) rs).getYearMonth().toString();
        } else if (rs instanceof PurchaseDistributionByDayWeekMetricsYear) {
            time = ((PurchaseDistributionByDayWeekMetricsYear) rs).getYear().toString();
        } else {
            logger.debug("Error finding result set class type");
        }

        csvRow += time;
        csvRow += "," + rs.totalPurchaseAttempts();
        csvRow += "," + rs.getMonday();
        csvRow += "," + CommonUtil.percentageFormat(rs.mondayPercent());
        csvRow += "," + rs.getTuesday();
        csvRow += "," + CommonUtil.percentageFormat(rs.tuesdayPercent());
        csvRow += "," + rs.getWednesday();
        csvRow += "," + CommonUtil.percentageFormat(rs.wednesdayPercent());
        csvRow += "," + rs.getThursday();
        csvRow += "," + CommonUtil.percentageFormat(rs.thursdayPercent());
        csvRow += "," + rs.getFriday();
        csvRow += "," + CommonUtil.percentageFormat(rs.fridayPercent());
        csvRow += "," + rs.getSaturday();
        csvRow += "," + CommonUtil.percentageFormat(rs.saturdayPercent());
        csvRow += "," + rs.getSunday();
        csvRow += "," + CommonUtil.percentageFormat(rs.sundayPercent());
        return csvRow += "\r\n";

    }

}