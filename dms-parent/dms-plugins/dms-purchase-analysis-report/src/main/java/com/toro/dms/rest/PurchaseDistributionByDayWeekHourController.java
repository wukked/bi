package com.toro.dms.rest;

import com.toro.dms.model.PurchaseDistributionByDayWeekMetricsMonth;
import com.toro.dms.model.PurchaseDistributionByDayWeekMetricsWeek;
import com.toro.dms.model.PurchaseDistributionByDayWeekMetricsYear;
import com.toro.dms.model.PurchaseDistributionByDayWeekType;
import com.toro.dms.repository.PurchaseDistributionByDayWeekMetricsMonthRepository;
import com.toro.dms.repository.PurchaseDistributionByDayWeekMetricsWeekRepository;
import com.toro.dms.repository.PurchaseDistributionByDayWeekMetricsYearRepository;
import com.toro.dms.utils.CalendarUtil;
import com.toro.dms.utils.CommonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;
import java.util.List;

@RequestMapping("/purchaseAnalysis/purchaseDistributionByDayWeekHour")
@RestController
public class PurchaseDistributionByDayWeekHourController {

    private static final Logger logger = LoggerFactory.getLogger(PurchaseDistributionByDayWeekHourController.class);

    @Inject
    PurchaseDistributionByDayWeekMetricsWeekRepository purchaseDistributionByDayWeekMetricsWeekRepository;

    @Inject
    PurchaseDistributionByDayWeekMetricsMonthRepository purchaseDistributionByDayWeekMetricsMonthRepository;

    @Inject
    PurchaseDistributionByDayWeekMetricsYearRepository purchaseDistributionByDayWeekMetricsYearsRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getCurrentWeek")
    public String getCurrentWeek(HttpServletResponse response) {

        logger.debug("do get current week");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByDayWeekHour_WeekReport.csv");
        Calendar now = Calendar.getInstance();
        List<PurchaseDistributionByDayWeekMetricsWeek> rs = purchaseDistributionByDayWeekMetricsWeekRepository.findByYearWeek(CalendarUtil.createYearWeekFormat(now));
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByDayWeekMetricsWeek pdbdwMetricWeek : rs) {
            csv += createCsvRow(pdbdwMetricWeek);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllWeeks")
    public String getAllWeeks(HttpServletResponse response) {

        logger.debug("do get all weeks");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByDayWeekHour_AllWeekReport.csv");
        Iterable<PurchaseDistributionByDayWeekMetricsWeek> rs = purchaseDistributionByDayWeekMetricsWeekRepository.findAll();
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByDayWeekMetricsWeek pdbdwMetricWeek : rs) {
            csv += createCsvRow(pdbdwMetricWeek);
        }
        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getCurrentMonth")
    public String getCurrentMonth(HttpServletResponse response) {

        logger.debug("do get month");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByDayWeekHour_MonthReport.csv");
        Calendar now = Calendar.getInstance();
        List<PurchaseDistributionByDayWeekMetricsMonth> rs = purchaseDistributionByDayWeekMetricsMonthRepository.findByYearMonth(CalendarUtil.createYearMonthFormat(now));
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByDayWeekMetricsMonth pdbdwMetricMonth : rs) {
            csv += createCsvRow(pdbdwMetricMonth);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllMonths")
    public String getAllMonths(HttpServletResponse response) {

        logger.debug("do get all months");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByDayWeekHour_AllMonthsReport.csv");
        Iterable<PurchaseDistributionByDayWeekMetricsMonth> rs = purchaseDistributionByDayWeekMetricsMonthRepository.findAll();
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByDayWeekMetricsMonth pdbdwMetricMonth : rs) {
            csv += createCsvRow(pdbdwMetricMonth);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getCurrentYear")
    public String getCurrentYear(HttpServletResponse response) {

        logger.debug("do get current year");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByDayWeekHour_YearReport.csv");
        Calendar now = Calendar.getInstance();
        List<PurchaseDistributionByDayWeekMetricsYear> rs = purchaseDistributionByDayWeekMetricsYearsRepository.findByYear(CalendarUtil.createYearFormat(now));
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByDayWeekMetricsYear pdbdwMetricYear : rs) {
            csv += createCsvRow(pdbdwMetricYear);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllYears")
    public String getAllYears(HttpServletResponse response) {

        logger.debug("do get all years");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByDayWeekHour_AllYearReport.csv");
        Iterable<PurchaseDistributionByDayWeekMetricsYear> rs = purchaseDistributionByDayWeekMetricsYearsRepository.findAll();
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByDayWeekMetricsYear pdbdwMetricYear : rs) {
            csv += createCsvRow(pdbdwMetricYear);
        }

        return csv;
    }

    private void setCsvResponseHeader(HttpServletResponse response, String fileName) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
        response.setContentType("application/x-download");
        response.addHeader("Content-Disposition", "filename=\"" + fileName + "\"");
    }

    private String createCsvHeaderTitles() {
        String header = "Time, Purchase attempts,";
        String[] dayWeek = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

        for(String day:dayWeek){
            for(int i=0;i<24;i++){
                String hoursFormat = day + " Between" + String.format("%02d", i) + ":00" + " and " + String.format("%02d", i) + ":59";
                header += hoursFormat + ",";
                header += hoursFormat + " %,";
            }
        }

        header = header.substring(0, header.length()-1);
        header += "\r\n";

        return header;
    }

    private String createCsvRow(PurchaseDistributionByDayWeekType rs) {
        logger.debug("Create CSV row");

        String csvRow = "";
        String time = "";

        if (rs instanceof PurchaseDistributionByDayWeekMetricsWeek) {
            time = ((PurchaseDistributionByDayWeekMetricsWeek) rs).getYearWeek().toString();
        } else if (rs instanceof PurchaseDistributionByDayWeekMetricsMonth) {
            time = ((PurchaseDistributionByDayWeekMetricsMonth) rs).getYearMonth().toString();
        } else if (rs instanceof PurchaseDistributionByDayWeekMetricsYear) {
            time = ((PurchaseDistributionByDayWeekMetricsYear) rs).getYear().toString();
        } else {
            logger.debug("Error finding result set class type");
        }

        csvRow += time;
        csvRow += "," + rs.totalPurchaseAttempts();

        int[] dayWeek ={ Calendar.MONDAY, Calendar.TUESDAY, Calendar.WEDNESDAY, Calendar.THURSDAY, Calendar.FRIDAY, Calendar.SATURDAY, Calendar.SUNDAY };

        for(int j=0;j<7;j++){
            for(int i=0;i<24;i++){
                long temp = rs.getDayWeekHours(dayWeek[j])[i];
                csvRow += "," + rs.getDayWeekHours(dayWeek[j])[i];
                csvRow += "," + CommonUtil.percentageFormat((double) rs.getDayWeekHours(dayWeek[j])[i] / (double) rs.totalPurchaseAttempts());
            }
        }

        return csvRow += "\r\n";

    }

}