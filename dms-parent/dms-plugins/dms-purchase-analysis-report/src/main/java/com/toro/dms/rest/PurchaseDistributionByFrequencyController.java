package com.toro.dms.rest;

import com.toro.dms.model.*;
import com.toro.dms.repository.PurchaseDistributionByActiveUserMetricsMonthRepository;
import com.toro.dms.repository.PurchaseDistributionByActiveUserMetricsWeekRepository;
import com.toro.dms.repository.PurchaseDistributionByActiveUserMetricsYearRepository;
import com.toro.dms.repository.PurchaseDistributionByActiveUserDataRepository;
import com.toro.dms.utils.CalendarUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;

@RequestMapping("/purchaseAnalysis/purchaseDistributionByFrequency")
@RestController
public class PurchaseDistributionByFrequencyController {

    private static final Logger logger = LoggerFactory.getLogger(PurchaseDistributionByFrequencyController.class);

    @Inject
    PurchaseDistributionByActiveUserDataRepository purchaseDistributionByActiveUserDataRepository;

    @Inject
    PurchaseDistributionByActiveUserMetricsWeekRepository purchaseDistributionByActiveUserMetricsWeekRepository;

    @Inject
    PurchaseDistributionByActiveUserMetricsMonthRepository purchaseDistributionByActiveUserMetricsMonthRepository;

    @Inject
    PurchaseDistributionByActiveUserMetricsYearRepository purchaseDistributionByActiveUserMetricsYearsRepository;

//
//    @RequestMapping(method = RequestMethod.GET, value = "/getYesterday")
//    public String getYesterday(HttpServletResponse response) {
//
//        logger.debug("do get yesterday");
//        logger.debug("caculated....");
//
//        setCsvResponseHeader(response, "PurchaseDistributionByActiveUser_DayReport.csv");
//        Iterable<PurchaseDistributionByActiveUser> rs = purchaseDistributionByActiveUserDataRepository.findByDate(CalendarUtil.getYesterday1());
//        String csv = createCsvHeaderTitles();
//
//        for (PurchaseDistributionByActiveUser pdbt : rs) {
//            csv += createCsvRow(pdbt);
//        }
//
//        return csv;
//    }
//
//    @RequestMapping(method = RequestMethod.GET, value = "/getDay")
//    public String getDay(HttpServletResponse response, @RequestParam(required = true) Calendar calDate) {
//
//        logger.debug("do get date");
//        logger.debug("caculated....");
//
//        setCsvResponseHeader(response, "PurchaseDistributionByActiveUser_DayReport.csv");
//        Iterable<PurchaseDistributionByActiveUser> rs = purchaseDistributionByActiveUserDataRepository.findByDate(calDate);
//        String csv = createCsvHeaderTitles();
//
//        for (PurchaseDistributionByActiveUser pdbt : rs) {
//            csv += createCsvRow(pdbt);
//        }
//
//        return csv;
//    }
//
//    @RequestMapping(method = RequestMethod.GET, value = "/getAllDays")
//    public String getAllDays(HttpServletResponse response) {
//
//        logger.debug("do get all days");
//        logger.debug("caculated....");
//
//        setCsvResponseHeader(response, "PurchaseDistributionByActiveUser_AllDaysReport.csv");
//        Iterable<PurchaseDistributionByActiveUser> rs = purchaseDistributionByActiveUserDataRepository.findAll();
//        String csv = createCsvHeaderTitles();
//
//        for (PurchaseDistributionByActiveUser pdbt : rs) {
//            csv += createCsvRow(pdbt);
//        }
//
//        return csv;
//    }
//
//    @RequestMapping(method = RequestMethod.GET, value = "/getCurrentWeek")
//    public String getCurrentWeek(HttpServletResponse response) {
//
//        logger.debug("do get current week");
//        logger.debug("caculated....");
//
//        setCsvResponseHeader(response, "PurchaseDistributionByActiveUser_WeekReport.csv");
//        Calendar now = Calendar.getInstance();
//        Iterable<PurchaseDistributionByActiveUserMetricsWeek> rs = purchaseDistributionByActiveUserMetricsWeekRepository.findByYearWeek(CalendarUtil.createYearWeekFormat(now));
//        String csv = createCsvHeaderTitles();
//
//        for (PurchaseDistributionByActiveUserMetricsWeek pdbtMetricsWeek : rs) {
//            csv += createCsvRow(pdbtMetricsWeek);
//        }
//
//        return csv;
//    }
//
//    @RequestMapping(method = RequestMethod.GET, value = "/getAllWeeks")
//    public String getAllWeeks(HttpServletResponse response) {
//
//        logger.debug("do get all weeks");
//        logger.debug("caculated....");
//
//        setCsvResponseHeader(response, "PurchaseDistributionByActiveUser_AllWeekReport.csv");
//        Iterable<PurchaseDistributionByActiveUserMetricsWeek> rs = purchaseDistributionByActiveUserMetricsWeekRepository.findAll();
//        String csv = createCsvHeaderTitles();
//
//        for (PurchaseDistributionByActiveUserMetricsWeek pdbtMetricWeek : rs) {
//            csv += createCsvRow(pdbtMetricWeek);
//        }
//        return csv;
//    }
//
//    @RequestMapping(method = RequestMethod.GET, value = "/getCurrentMonth")
//    public String getCurrentMonth(HttpServletResponse response) {
//
//        logger.debug("do get month");
//        logger.debug("caculated....");
//
//        setCsvResponseHeader(response, "PurchaseDistributionByActiveUser_MonthReport.csv");
//        Calendar now = Calendar.getInstance();
//        Iterable<PurchaseDistributionByActiveUserMetricsMonth> rs = purchaseDistributionByActiveUserMetricsMonthRepository.findByYearMonth(CalendarUtil.createYearMonthFormat(now));
//        String csv = createCsvHeaderTitles();
//
//        for (PurchaseDistributionByActiveUserMetricsMonth pdbtMetricMonth : rs) {
//            csv += createCsvRow(pdbtMetricMonth);
//        }
//
//        return csv;
//    }
//
//    @RequestMapping(method = RequestMethod.GET, value = "/getAllMonths")
//    public String getAllMonths(HttpServletResponse response) {
//
//        logger.debug("do get all months");
//        logger.debug("caculated....");
//
//        setCsvResponseHeader(response, "PurchaseDistributionByActiveUser_AllMonthsReport.csv");
//        Iterable<PurchaseDistributionByActiveUserMetricsMonth> rs = purchaseDistributionByActiveUserMetricsMonthRepository.findAll();
//        String csv = createCsvHeaderTitles();
//
//        for (PurchaseDistributionByActiveUserMetricsMonth pdbtMetricMonth : rs) {
//            csv += createCsvRow(pdbtMetricMonth);
//        }
//
//        return csv;
//    }
//
//    @RequestMapping(method = RequestMethod.GET, value = "/getCurrentYear")
//    public String getCurrentYear(HttpServletResponse response) {
//
//        logger.debug("do get current year");
//        logger.debug("caculated....");
//
//        setCsvResponseHeader(response, "PurchaseDistributionByActiveUser_YearReport.csv");
//
//        Calendar now = Calendar.getInstance();
//        Iterable<PurchaseDistributionByActiveUserMetricsYear> rs = purchaseDistributionByActiveUserMetricsYearsRepository.findByYear(CalendarUtil.createYearFormat(now));
//        String csv = createCsvHeaderTitles();
//
//        for (PurchaseDistributionByActiveUserMetricsYear pdbtMetricYear : rs) {
//            csv += createCsvRow(pdbtMetricYear);
//        }
//
//        return csv;
//    }
//
//    @RequestMapping(method = RequestMethod.GET, value = "/getAllYears")
//    public String getAllYears(HttpServletResponse response) {
//
//        logger.debug("do get all years");
//        logger.debug("caculated....");
//
//        setCsvResponseHeader(response, "PurchaseDistributionByActiveUser_AllYearReport.csv");
//        Iterable<PurchaseDistributionByActiveUserMetricsYear> rs = purchaseDistributionByActiveUserMetricsYearsRepository.findAll();
//        String csv = createCsvHeaderTitles();
//
//        for (PurchaseDistributionByActiveUserMetricsYear pdbtMetricYear : rs) {
//            csv += createCsvRow(pdbtMetricYear);
//        }
//
//        return csv;
//    }
//
//    private void setCsvResponseHeader(HttpServletResponse response, String fileName) {
//        response.addHeader("Access-Control-Allow-Origin", "*");
//        response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
//        response.setContentType("application/x-download");
//        response.addHeader("Content-Disposition", "filename=\"" + fileName + "\"");
//    }
//
//    private String createCsvHeaderTitles() {
//        return "Time," +
//                "Purchase attempts," +
//                "Average # of purchase by Equipped customer," +
//
//                "\r\n";
//    }
//
//    private String createCsvRow(PurchaseDistributionByActiveUserType rs) {
//        logger.debug("Create CSV row");
//
//        String csvRow = "";
//        String time = "";
//
//        if (rs instanceof PurchaseDistributionByActiveUser) {
//            time = CalendarUtil.format(((PurchaseDistributionByActiveUser) rs).getDate());
//        } else if (rs instanceof PurchaseDistributionByActiveUserMetricsWeek) {
//            time = ((PurchaseDistributionByActiveUserMetricsWeek) rs).getYearWeek().toString();
//        } else if (rs instanceof PurchaseDistributionByActiveUserMetricsMonth) {
//            time = ((PurchaseDistributionByActiveUserMetricsMonth) rs).getYearMonth().toString();
//        } else if (rs instanceof PurchaseDistributionByActiveUserMetricsYear) {
//            time = ((PurchaseDistributionByActiveUserMetricsYear) rs).getYear().toString();
//        } else {
//            logger.debug("Error finding result set class type");
//        }
//
//        csvRow += time;
//        csvRow += "," + rs.totalPurchaseAttempts();
//
//        return csvRow += "\r\n";
//
//    }

}