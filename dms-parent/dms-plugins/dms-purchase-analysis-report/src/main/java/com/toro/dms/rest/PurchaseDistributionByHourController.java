package com.toro.dms.rest;

import com.toro.dms.model.*;
import com.toro.dms.repository.PurchaseDistributionByHourMetricsMonthRepository;
import com.toro.dms.repository.PurchaseDistributionByHourMetricsWeekRepository;
import com.toro.dms.repository.PurchaseDistributionByHourMetricsYearRepository;
import com.toro.dms.repository.PurchaseDistributionByHourRepository;
import com.toro.dms.utils.CalendarUtil;
import com.toro.dms.utils.CommonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;

@RequestMapping("/purchaseAnalysis/purchaseDistributionByHour")
@RestController
public class PurchaseDistributionByHourController {

    private static final Logger logger = LoggerFactory.getLogger(PurchaseDistributionByHourController.class);

    @Inject
    PurchaseDistributionByHourRepository purchaseDistributionByHourRepository;

    @Inject
    PurchaseDistributionByHourMetricsWeekRepository purchaseDistributionByHourMetricsWeekRepository;

    @Inject
    PurchaseDistributionByHourMetricsMonthRepository purchaseDistributionByHourMetricsMonthRepository;

    @Inject
    PurchaseDistributionByHourMetricsYearRepository purchaseDistributionByHourMetricsYearsRepository;


    @RequestMapping(method = RequestMethod.GET, value = "/getYesterday")
    public String getYesterday(HttpServletResponse response) {

        logger.debug("do get yesterday");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByHour_DayReport.csv");
        Iterable<PurchaseDistributionByHour> rs = purchaseDistributionByHourRepository.findByDate(CalendarUtil.getYesterday1());
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByHour pdbh : rs) {
            csv += createCsvRow(pdbh);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getDay")
    public String getDay(HttpServletResponse response, @RequestParam(required = true) Calendar calDate) {

        logger.debug("do get date");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByHour_DayReport.csv");
        Iterable<PurchaseDistributionByHour> rs = purchaseDistributionByHourRepository.findByDate(calDate);
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByHour pdbh : rs) {
            csv += createCsvRow(pdbh);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllDays")
    public String getAllDays(HttpServletResponse response) {

        logger.debug("do get all days");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByHour_AllDaysReport.csv");
        Iterable<PurchaseDistributionByHour> rs = purchaseDistributionByHourRepository.findAll();
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByHour pdbh : rs) {
            csv += createCsvRow(pdbh);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getCurrentWeek")
    public String getCurrentWeek(HttpServletResponse response) {

        logger.debug("do get current week");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByHour_WeekReport.csv");
        Calendar now = Calendar.getInstance();
        Iterable<PurchaseDistributionByHourMetricsWeek> rs = purchaseDistributionByHourMetricsWeekRepository.findByYearWeek(CalendarUtil.createYearWeekFormat(now));
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByHourMetricsWeek pdbhMetricsWeek : rs) {
            csv += createCsvRow(pdbhMetricsWeek);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllWeeks")
    public String getAllWeeks(HttpServletResponse response) {

        logger.debug("do get all weeks");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByHour_AllWeekReport.csv");
        Iterable<PurchaseDistributionByHourMetricsWeek> rs = purchaseDistributionByHourMetricsWeekRepository.findAll();
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByHourMetricsWeek pdbhMetricWeek : rs) {
            csv += createCsvRow(pdbhMetricWeek);
        }
        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getCurrentMonth")
    public String getCurrentMonth(HttpServletResponse response) {

        logger.debug("do get month");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByHour_MonthReport.csv");
        Calendar now = Calendar.getInstance();
        Iterable<PurchaseDistributionByHourMetricsMonth> rs = purchaseDistributionByHourMetricsMonthRepository.findByYearMonth(CalendarUtil.createYearMonthFormat(now));
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByHourMetricsMonth pdbhMetricMonth : rs) {
            csv += createCsvRow(pdbhMetricMonth);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllMonths")
    public String getAllMonths(HttpServletResponse response) {

        logger.debug("do get all months");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByHour_AllMonthsReport.csv");
        Iterable<PurchaseDistributionByHourMetricsMonth> rs = purchaseDistributionByHourMetricsMonthRepository.findAll();
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByHourMetricsMonth pdbhMetricMonth : rs) {
            csv += createCsvRow(pdbhMetricMonth);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getCurrentYear")
    public String getCurrentYear(HttpServletResponse response) {

        logger.debug("do get current year");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByHour_YearReport.csv");

        Calendar now = Calendar.getInstance();
        Iterable<PurchaseDistributionByHourMetricsYear> rs = purchaseDistributionByHourMetricsYearsRepository.findByYear(CalendarUtil.createYearFormat(now));
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByHourMetricsYear pdbhMetricYear : rs) {
            csv += createCsvRow(pdbhMetricYear);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllYears")
    public String getAllYears(HttpServletResponse response) {

        logger.debug("do get all years");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByHour_AllYearReport.csv");
        Iterable<PurchaseDistributionByHourMetricsYear> rs = purchaseDistributionByHourMetricsYearsRepository.findAll();
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByHourMetricsYear pdbhMetricYear : rs) {
            csv += createCsvRow(pdbhMetricYear);
        }

        return csv;
    }

    private void setCsvResponseHeader(HttpServletResponse response, String fileName) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
        response.setContentType("application/x-download");
        response.addHeader("Content-Disposition", "filename=\"" + fileName + "\"");
    }

    private String createCsvHeaderTitles() {
        return "Time," +
                "Purchase attempts," +
                "Between 00:00 and 00:59," +
                "Between 00:00 and 00:59 %," +
                "Between 01:00 and 01:59," +
                "Between 01:00 and 01:59 %," +
                "Between 02:00 and 02:59," +
                "Between 02:00 and 02:59 %," +
                "Between 03:00 and 03:59," +
                "Between 03:00 and 03:59 %," +
                "Between 04:00 and 04:59," +
                "Between 04:00 and 04:59 %," +
                "Between 05:00 and 05:59," +
                "Between 05:00 and 05:59 %," +
                "Between 06:00 and 06:59," +
                "Between 06:00 and 06:59 %," +
                "Between 07:00 and 07:59," +
                "Between 07:00 and 07:59 %," +
                "Between 08:00 and 08:59," +
                "Between 08:00 and 08:59 %," +
                "Between 09:00 and 09:59," +
                "Between 09:00 and 09:59 %," +
                "Between 10:00 and 10:59," +
                "Between 10:00 and 10:59 %," +
                "Between 11:00 and 11:59," +
                "Between 11:00 and 11:59 %," +
                "Between 12:00 and 12:59," +
                "Between 12:00 and 12:59 %," +
                "Between 13:00 and 13:59," +
                "Between 13:00 and 13:59 %," +
                "Between 14:00 and 14:59," +
                "Between 14:00 and 14:59 %," +
                "Between 15:00 and 15:59," +
                "Between 15:00 and 15:59 %," +
                "Between 16:00 and 16:59," +
                "Between 16:00 and 16:59 %," +
                "Between 17:00 and 17:59," +
                "Between 17:00 and 17:59 %," +
                "Between 18:00 and 18:59," +
                "Between 18:00 and 18:59 %," +
                "Between 19:00 and 19:59," +
                "Between 19:00 and 19:59 %," +
                "Between 20:00 and 20:59," +
                "Between 20:00 and 20:59 %," +
                "Between 21:00 and 21:59," +
                "Between 21:00 and 21:59 %," +
                "Between 22:00 and 22:59," +
                "Between 22:00 and 22:59 %," +
                "Between 23:00 and 23:59," +
                "Between 23:00 and 23:59 %," +
                "\r\n";
    }

    private String createCsvRow(PurchaseDistributionByHourType rs) {
        logger.debug("Create CSV row");

        long[] hoursArray = rs.hoursToArray();
        double[] hoursPercentArray = rs.hoursPercentToArray();
        String csvRow = "";
        String time = "";

        if (rs instanceof PurchaseDistributionByHour) {
            time = CalendarUtil.format(((PurchaseDistributionByHour) rs).getDate());
        } else if (rs instanceof PurchaseDistributionByHourMetricsWeek) {
            time = ((PurchaseDistributionByHourMetricsWeek) rs).getYearWeek().toString();
        } else if (rs instanceof PurchaseDistributionByHourMetricsMonth) {
            time = ((PurchaseDistributionByHourMetricsMonth) rs).getYearMonth().toString();
        } else if (rs instanceof PurchaseDistributionByHourMetricsYear) {
            time = ((PurchaseDistributionByHourMetricsYear) rs).getYear().toString();
        } else {
            logger.debug("Error finding result set class type");
        }

        csvRow += time;
        csvRow += "," + rs.totalPurchaseAttempts();

        for (int i = 0; i < hoursArray.length; i++) {
            csvRow += "," + hoursArray[i];
            csvRow += "," + CommonUtil.percentageFormat(hoursPercentArray[i]);
        }

        return csvRow += "\r\n";
    }

}