package com.toro.dms.rest;

import com.toro.dms.model.*;
import com.toro.dms.repository.PurchaseDistributionByTypeMetricsMonthRepository;
import com.toro.dms.repository.PurchaseDistributionByTypeMetricsWeekRepository;
import com.toro.dms.repository.PurchaseDistributionByTypeMetricsYearRepository;
import com.toro.dms.repository.PurchaseDistributionByTypeRepository;
import com.toro.dms.utils.CalendarUtil;
import com.toro.dms.utils.CommonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;

@RequestMapping("/purchaseAnalysis/purchaseDistributionByType")
@RestController
public class PurchaseDistributionByTypeController {

    private static final Logger logger = LoggerFactory.getLogger(PurchaseDistributionByTypeController.class);

    @Inject
    PurchaseDistributionByTypeRepository purchaseDistributionByTypeRepository;

    @Inject
    PurchaseDistributionByTypeMetricsWeekRepository purchaseDistributionByTypeMetricsWeekRepository;

    @Inject
    PurchaseDistributionByTypeMetricsMonthRepository purchaseDistributionByTypeMetricsMonthRepository;

    @Inject
    PurchaseDistributionByTypeMetricsYearRepository purchaseDistributionByTypeMetricsYearsRepository;


    @RequestMapping(method = RequestMethod.GET, value = "/getYesterday")
    public String getYesterday(HttpServletResponse response) {

        logger.debug("do get yesterday");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByType_DayReport.csv");
        Iterable<PurchaseDistributionByType> rs = purchaseDistributionByTypeRepository.findByDate(CalendarUtil.getYesterday1());
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByType pdbt : rs) {
            csv += createCsvRow(pdbt);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getDay")
    public String getDay(HttpServletResponse response, @RequestParam(required = true) Calendar calDate) {

        logger.debug("do get date");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByType_DayReport.csv");
        Iterable<PurchaseDistributionByType> rs = purchaseDistributionByTypeRepository.findByDate(calDate);
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByType pdbt : rs) {
            csv += createCsvRow(pdbt);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllDays")
    public String getAllDays(HttpServletResponse response) {

        logger.debug("do get all days");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByType_AllDaysReport.csv");
        Iterable<PurchaseDistributionByType> rs = purchaseDistributionByTypeRepository.findAll();
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByType pdbt : rs) {
            csv += createCsvRow(pdbt);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getCurrentWeek")
    public String getCurrentWeek(HttpServletResponse response) {

        logger.debug("do get current week");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByType_WeekReport.csv");
        Calendar now = Calendar.getInstance();
        Iterable<PurchaseDistributionByTypeMetricsWeek> rs = purchaseDistributionByTypeMetricsWeekRepository.findByYearWeek(CalendarUtil.createYearWeekFormat(now));
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByTypeMetricsWeek pdbtMetricsWeek : rs) {
            csv += createCsvRow(pdbtMetricsWeek);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllWeeks")
    public String getAllWeeks(HttpServletResponse response) {

        logger.debug("do get all weeks");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByType_AllWeekReport.csv");
        Iterable<PurchaseDistributionByTypeMetricsWeek> rs = purchaseDistributionByTypeMetricsWeekRepository.findAll();
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByTypeMetricsWeek pdbtMetricWeek : rs) {
            csv += createCsvRow(pdbtMetricWeek);
        }
        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getCurrentMonth")
    public String getCurrentMonth(HttpServletResponse response) {

        logger.debug("do get month");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByType_MonthReport.csv");
        Calendar now = Calendar.getInstance();
        Iterable<PurchaseDistributionByTypeMetricsMonth> rs = purchaseDistributionByTypeMetricsMonthRepository.findByYearMonth(CalendarUtil.createYearMonthFormat(now));
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByTypeMetricsMonth pdbtMetricMonth : rs) {
            csv += createCsvRow(pdbtMetricMonth);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllMonths")
    public String getAllMonths(HttpServletResponse response) {

        logger.debug("do get all months");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByType_AllMonthsReport.csv");
        Iterable<PurchaseDistributionByTypeMetricsMonth> rs = purchaseDistributionByTypeMetricsMonthRepository.findAll();
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByTypeMetricsMonth pdbtMetricMonth : rs) {
            csv += createCsvRow(pdbtMetricMonth);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getCurrentYear")
    public String getCurrentYear(HttpServletResponse response) {

        logger.debug("do get current year");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByType_YearReport.csv");

        Calendar now = Calendar.getInstance();
        Iterable<PurchaseDistributionByTypeMetricsYear> rs = purchaseDistributionByTypeMetricsYearsRepository.findByYear(CalendarUtil.createYearFormat(now));
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByTypeMetricsYear pdbtMetricYear : rs) {
            csv += createCsvRow(pdbtMetricYear);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllYears")
    public String getAllYears(HttpServletResponse response) {

        logger.debug("do get all years");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "PurchaseDistributionByType_AllYearReport.csv");
        Iterable<PurchaseDistributionByTypeMetricsYear> rs = purchaseDistributionByTypeMetricsYearsRepository.findAll();
        String csv = createCsvHeaderTitles();

        for (PurchaseDistributionByTypeMetricsYear pdbtMetricYear : rs) {
            csv += createCsvRow(pdbtMetricYear);
        }

        return csv;
    }

    private void setCsvResponseHeader(HttpServletResponse response, String fileName) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
        response.setContentType("application/x-download");
        response.addHeader("Content-Disposition", "filename=\"" + fileName + "\"");
    }

    private String createCsvHeaderTitles() {
        return "Time," +
                "Purchase attempts," +
                "Purchases Offline approved," +
                "Purchases Offline approved %," +
                "Purchases Offline declined," +
                "Purchases Offline declined %," +
                "Purchases Online," +
                "Purchases Online %," +
                "Purchases TapPinTap," +
                "Purchases TapPinTap %" +
                "\r\n";
    }

    private String createCsvRow(PurchaseDistributionByTypeType rs) {
        logger.debug("Create CSV row");

        String csvRow = "";
        String time = "";

        if (rs instanceof PurchaseDistributionByType) {
            time = CalendarUtil.format(((PurchaseDistributionByType) rs).getDate());
        } else if (rs instanceof PurchaseDistributionByTypeMetricsWeek) {
            time = ((PurchaseDistributionByTypeMetricsWeek) rs).getYearWeek().toString();
        } else if (rs instanceof PurchaseDistributionByTypeMetricsMonth) {
            time = ((PurchaseDistributionByTypeMetricsMonth) rs).getYearMonth().toString();
        } else if (rs instanceof PurchaseDistributionByTypeMetricsYear) {
            time = ((PurchaseDistributionByTypeMetricsYear) rs).getYear().toString();
        } else {
            logger.debug("Error finding result set class type");
        }

        csvRow += time;
        csvRow += "," + rs.totalPurchaseAttempts();
        csvRow += "," + rs.getOfflineApproved();
        csvRow += "," + CommonUtil.percentageFormat(rs.offlineApprovedPercent());
        csvRow += "," + rs.getOfflineDeclined();
        csvRow += "," + CommonUtil.percentageFormat(rs.offlineDeclinedPercent());
        csvRow += "," + rs.getOnline();
        csvRow += "," + CommonUtil.percentageFormat(rs.onlinePercent());
        csvRow += "," + rs.getTapPinTap();
        csvRow += "," + CommonUtil.percentageFormat(rs.tapPinTapPercent());
        return csvRow += "\r\n";

    }

}