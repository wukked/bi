package com.toro.dms.transformers;

import com.toro.dms.model.*;
import com.toro.dms.repository.PurchaseDistributionByDayWeekMetricsMonthRepository;
import com.toro.dms.repository.PurchaseDistributionByDayWeekMetricsWeekRepository;
import com.toro.dms.repository.PurchaseDistributionByDayWeekMetricsYearRepository;
import com.toro.dms.repository.PurchaseDistributionByHourRepository;
import com.toro.dms.utils.CalendarUtil;
import com.toro.dms.utils.CommonUtil;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Calendar;
import java.util.List;

@Component
public class PurchaseDistributionByDayWeekTransformer {

    @Inject
    private PurchaseDistributionByHourRepository purchaseDistributionByHourRepository;

    @Inject
    private PurchaseDistributionByDayWeekMetricsWeekRepository purchaseDistributionByDayWeekMetricsWeekRepository;

    @Inject
    private PurchaseDistributionByDayWeekMetricsMonthRepository purchaseDistributionByDayWeekMetricsMonthRepository;

    @Inject
    private PurchaseDistributionByDayWeekMetricsYearRepository purchaseDistributionByDayWeekMetricsYearRepository;

    public PurchaseDistributionByDayWeekMetricsWeek transformWeek(Calendar calDate) {
        List<PurchaseDistributionByDayWeekMetricsWeek> pdbdwMetricsWeekList = purchaseDistributionByDayWeekMetricsWeekRepository.findByYearWeek(CalendarUtil.createYearWeekFormat(calDate));
        PurchaseDistributionByDayWeekMetricsWeek pdbdwMetricsWeek = null;
        if (pdbdwMetricsWeekList.isEmpty()) {
            //create a new week
            pdbdwMetricsWeek = new PurchaseDistributionByDayWeekMetricsWeek(CalendarUtil.createYearWeekFormat(calDate));
        }else{
            pdbdwMetricsWeek = pdbdwMetricsWeekList.get(0);
        }
        List<PurchaseDistributionByHour> rsPdbh = purchaseDistributionByHourRepository.findByDate(calDate);

        //accumulate
        for (PurchaseDistributionByHour pdbh : rsPdbh) {
            pdbdwMetricsWeek.setDayWeekPurchaseAttempts(pdbh.getDate().get(Calendar.DAY_OF_WEEK), pdbh.totalPurchaseAttempts());
            pdbdwMetricsWeek.setDayWeekHours(pdbh.getDate().get(Calendar.DAY_OF_WEEK), pdbh.hoursToArray());
        }

        return pdbdwMetricsWeek;
    }

    public PurchaseDistributionByDayWeekMetricsMonth transformMonth(Calendar calDate) {
        List<PurchaseDistributionByDayWeekMetricsMonth> pdbdwMetricsMonthList = purchaseDistributionByDayWeekMetricsMonthRepository.findByYearMonth(CalendarUtil.createYearMonthFormat(calDate));
        PurchaseDistributionByDayWeekMetricsMonth pdbdwMetricsMonth = null;
        if (pdbdwMetricsMonthList.isEmpty()) {
            //create a new month
            pdbdwMetricsMonth = new PurchaseDistributionByDayWeekMetricsMonth(CalendarUtil.createYearMonthFormat(calDate));
        }else{
            pdbdwMetricsMonth = pdbdwMetricsMonthList.get(0);
        }
        List<PurchaseDistributionByHour> rsPdbh = purchaseDistributionByHourRepository.findByDate(calDate);

        //accumulate
        for (PurchaseDistributionByHour pdbh : rsPdbh) {
            pdbdwMetricsMonth.setDayWeekPurchaseAttempts(pdbh.getDate().get(Calendar.DAY_OF_WEEK), pdbh.totalPurchaseAttempts() + pdbdwMetricsMonth.getDayWeekPurchaseAttempts(pdbh.getDate().get(Calendar.DAY_OF_WEEK)));
            pdbdwMetricsMonth.setDayWeekHours(pdbh.getDate().get(Calendar.DAY_OF_WEEK), CommonUtil.addLongArrays(pdbh.hoursToArray(), pdbdwMetricsMonth.getDayWeekHours(pdbh.getDate().get(Calendar.DAY_OF_WEEK))));
        }
        return pdbdwMetricsMonth;
    }

    public PurchaseDistributionByDayWeekMetricsYear transformYear(Calendar calDate) {
        List<PurchaseDistributionByDayWeekMetricsYear> pdbdwMetricsYearList = purchaseDistributionByDayWeekMetricsYearRepository.findByYear(CalendarUtil.createYearFormat(calDate));
        PurchaseDistributionByDayWeekMetricsYear pdbdwMetricsYear = null;
        if (pdbdwMetricsYearList.isEmpty()) {
            //create a new year
            pdbdwMetricsYear = new PurchaseDistributionByDayWeekMetricsYear(CalendarUtil.createYearFormat(calDate));
        }else{
            pdbdwMetricsYear = pdbdwMetricsYearList.get(0);
        }
        List<PurchaseDistributionByHour> rsPdbh = purchaseDistributionByHourRepository.findByDate(calDate);

        //accumulate
        for (PurchaseDistributionByHour pdbh : rsPdbh) {
            pdbdwMetricsYear.setDayWeekPurchaseAttempts(pdbh.getDate().get(Calendar.DAY_OF_WEEK), pdbh.totalPurchaseAttempts() + pdbdwMetricsYear.getDayWeekPurchaseAttempts(pdbh.getDate().get(Calendar.DAY_OF_WEEK)));
            pdbdwMetricsYear.setDayWeekHours(pdbh.getDate().get(Calendar.DAY_OF_WEEK), CommonUtil.addLongArrays(pdbh.hoursToArray(), pdbdwMetricsYear.getDayWeekHours(pdbh.getDate().get(Calendar.DAY_OF_WEEK))));
        }
        return pdbdwMetricsYear;
    }

}