package com.toro.dms.transformers;

import com.toro.dms.model.*;
import com.toro.dms.repository.PurchaseDistributionByActiveUserMetricsWeekRepository;
import com.toro.dms.repository.PurchaseDistributionByActiveUserDataRepository;
import com.toro.dms.utils.CalendarUtil;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Component
public class PurchaseDistributionByFrequencyTransformer {

    @Inject
    private PurchaseDistributionByActiveUserDataRepository purchaseDistributionByActiveUserDataRepository;

    @Inject
    private PurchaseDistributionByActiveUserMetricsWeekRepository purchaseDistributionByActiveUserMetricsWeekRepository;
    
    public PurchaseDistributionByFrequencyMetricsDay transformDay(Calendar calDate) {

        List<PurchaseDistributionByActiveUser> pdauResult = purchaseDistributionByActiveUserDataRepository.findByDate(calDate);

        long[] userPurchaseFrequecnyArray = new long[11];

        for(PurchaseDistributionByActiveUser au:pdauResult){
            switch( (int)au.getPurchaseAttempts() ){
                case 1: userPurchaseFrequecnyArray[0]++; break;
                case 2: userPurchaseFrequecnyArray[1]++; break;
                case 3: userPurchaseFrequecnyArray[2]++; break;
                case 4: userPurchaseFrequecnyArray[3]++; break;
                case 5: userPurchaseFrequecnyArray[4]++; break;
                case 6: userPurchaseFrequecnyArray[5]++; break;
                case 7: userPurchaseFrequecnyArray[6]++; break;
                case 8: userPurchaseFrequecnyArray[7]++; break;
                case 9: userPurchaseFrequecnyArray[8]++; break;
                case 10: userPurchaseFrequecnyArray[9]++; break;
                default: userPurchaseFrequecnyArray[10]++; break;
            }
        }

        PurchaseDistributionByFrequencyMetricsDay pdbauMetricsDay = new PurchaseDistributionByFrequencyMetricsDay(calDate, purchaseDistributionByActiveUserDataRepository.activeUserPopulationBetweenDates(CalendarUtil.getDateMinus30(calDate), calDate), purchaseDistributionByActiveUserDataRepository.totalPurchaseAttempts(calDate, calDate), userPurchaseFrequecnyArray);

        return pdbauMetricsDay;
    }

    public List<PurchaseDistributionByActiveUserMetricsWeek> transformWeek(Calendar calDate) {
        List<Object[]> rsAccumulatedActiveUsers = purchaseDistributionByActiveUserDataRepository.accumulateActivity(CalendarUtil.getStartDateOfWeek(calDate), CalendarUtil.getEndDateOfWeek(calDate));

        List<PurchaseDistributionByActiveUserMetricsWeek> pdbauMetricsWeekList = new ArrayList();

        for (Object[] activeUser : rsAccumulatedActiveUsers) {
            PurchaseDistributionByActiveUserMetricsWeek pdbauMetricsWeek = new PurchaseDistributionByActiveUserMetricsWeek(
                    CalendarUtil.createYearWeekFormat(calDate),
                    activeUser[0].toString(),
                    (long) activeUser[1]
            );
            pdbauMetricsWeekList.add(pdbauMetricsWeek);
        }

        return pdbauMetricsWeekList;
    }


    public List<PurchaseDistributionByActiveUserMetricsMonth> transformMonth(Calendar calDate) {
        List<Object[]> rsAccumulatedActiveUsers = purchaseDistributionByActiveUserDataRepository.accumulateActivity(CalendarUtil.getStartDateOfMonth(calDate), CalendarUtil.getEndDateOfMonth(calDate));

        List<PurchaseDistributionByActiveUserMetricsMonth> pdbauMetricsMonthList = new ArrayList();

        for (Object[] activeUser : rsAccumulatedActiveUsers) {
            PurchaseDistributionByActiveUserMetricsMonth pdbauMetricsMonth = new PurchaseDistributionByActiveUserMetricsMonth(
                    CalendarUtil.createYearMonthFormat(calDate),
                    activeUser[0].toString(),
                    (long) activeUser[1]
            );

            pdbauMetricsMonthList.add(pdbauMetricsMonth);
        }

        return pdbauMetricsMonthList;
    }

    public List<PurchaseDistributionByActiveUserMetricsYear> transformYear(Calendar calDate) {
        List<Object[]> rsAccumulatedActiveUsers = purchaseDistributionByActiveUserDataRepository.accumulateActivity(CalendarUtil.getStartDateOfYear(calDate), CalendarUtil.getEndDateOfYear(calDate));

        List<PurchaseDistributionByActiveUserMetricsYear> pdbauMetricsYearList = new ArrayList();

        for (Object[] activeUser : rsAccumulatedActiveUsers) {
            PurchaseDistributionByActiveUserMetricsYear pdbauMetricsYear = new PurchaseDistributionByActiveUserMetricsYear(
                    CalendarUtil.createYearFormat(calDate),
                    activeUser[0].toString(),
                    (long) activeUser[1]
            );

            pdbauMetricsYearList.add(pdbauMetricsYear);
        }

        return pdbauMetricsYearList;
    }

}