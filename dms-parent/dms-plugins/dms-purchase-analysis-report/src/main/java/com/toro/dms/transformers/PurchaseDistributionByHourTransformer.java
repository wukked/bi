package com.toro.dms.transformers;

import com.toro.dms.model.PurchaseDistributionByHour;
import com.toro.dms.model.PurchaseDistributionByHourMetricsMonth;
import com.toro.dms.model.PurchaseDistributionByHourMetricsWeek;
import com.toro.dms.model.PurchaseDistributionByHourMetricsYear;
import com.toro.dms.repository.PurchaseDistributionByHourRepository;
import com.toro.dms.utils.CalendarUtil;
import org.apache.spark.api.java.JavaSparkContext;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import javax.inject.Inject;
import java.util.Calendar;
import java.util.List;

@Component
public class PurchaseDistributionByHourTransformer {

    @Inject
    private PurchaseDistributionByHourRepository purchaseDistributionByHourRepository;

    public PurchaseDistributionByHourMetricsWeek transformWeek(Calendar calDate) {
        PurchaseDistributionByHourMetricsWeek pdbhMetricsWeek = new PurchaseDistributionByHourMetricsWeek(CalendarUtil.createYearWeekFormat(calDate));
        List<PurchaseDistributionByHour> rs = purchaseDistributionByHourRepository.findByDateBetween(CalendarUtil.getStartDateOfWeek(calDate), CalendarUtil.getEndDateOfWeek(calDate));

        //accumulate
        for (PurchaseDistributionByHour pdbh : rs) {
            pdbhMetricsWeek.setHour00(pdbhMetricsWeek.getHour00() + pdbh.getHour00());
            pdbhMetricsWeek.setHour01(pdbhMetricsWeek.getHour01() + pdbh.getHour01());
            pdbhMetricsWeek.setHour02(pdbhMetricsWeek.getHour02() + pdbh.getHour02());
            pdbhMetricsWeek.setHour03(pdbhMetricsWeek.getHour03() + pdbh.getHour03());
            pdbhMetricsWeek.setHour04(pdbhMetricsWeek.getHour04() + pdbh.getHour04());
            pdbhMetricsWeek.setHour05(pdbhMetricsWeek.getHour05() + pdbh.getHour05());
            pdbhMetricsWeek.setHour06(pdbhMetricsWeek.getHour06() + pdbh.getHour06());
            pdbhMetricsWeek.setHour07(pdbhMetricsWeek.getHour07() + pdbh.getHour07());
            pdbhMetricsWeek.setHour08(pdbhMetricsWeek.getHour08() + pdbh.getHour08());
            pdbhMetricsWeek.setHour09(pdbhMetricsWeek.getHour09() + pdbh.getHour09());
            pdbhMetricsWeek.setHour10(pdbhMetricsWeek.getHour10() + pdbh.getHour10());
            pdbhMetricsWeek.setHour11(pdbhMetricsWeek.getHour11() + pdbh.getHour11());
            pdbhMetricsWeek.setHour12(pdbhMetricsWeek.getHour12() + pdbh.getHour12());
            pdbhMetricsWeek.setHour13(pdbhMetricsWeek.getHour13() + pdbh.getHour13());
            pdbhMetricsWeek.setHour14(pdbhMetricsWeek.getHour14() + pdbh.getHour14());
            pdbhMetricsWeek.setHour15(pdbhMetricsWeek.getHour15() + pdbh.getHour15());
            pdbhMetricsWeek.setHour16(pdbhMetricsWeek.getHour16() + pdbh.getHour16());
            pdbhMetricsWeek.setHour17(pdbhMetricsWeek.getHour17() + pdbh.getHour17());
            pdbhMetricsWeek.setHour18(pdbhMetricsWeek.getHour18() + pdbh.getHour18());
            pdbhMetricsWeek.setHour19(pdbhMetricsWeek.getHour19() + pdbh.getHour19());
            pdbhMetricsWeek.setHour20(pdbhMetricsWeek.getHour20() + pdbh.getHour20());
            pdbhMetricsWeek.setHour21(pdbhMetricsWeek.getHour21() + pdbh.getHour21());
            pdbhMetricsWeek.setHour22(pdbhMetricsWeek.getHour22() + pdbh.getHour22());
            pdbhMetricsWeek.setHour23(pdbhMetricsWeek.getHour23() + pdbh.getHour23());
        }

        return pdbhMetricsWeek;
    }

    public PurchaseDistributionByHourMetricsMonth transformMonth(Calendar calDate) {
        PurchaseDistributionByHourMetricsMonth pdbhMetricsMonth = new PurchaseDistributionByHourMetricsMonth(CalendarUtil.createYearMonthFormat(calDate));
        List<PurchaseDistributionByHour> rs = purchaseDistributionByHourRepository.findByDateBetween(CalendarUtil.getStartDateOfMonth(calDate), CalendarUtil.getEndDateOfMonth(calDate));

        //accumulate
        for (PurchaseDistributionByHour pdbh : rs) {
            pdbhMetricsMonth.setHour00(pdbhMetricsMonth.getHour00() + pdbh.getHour00());
            pdbhMetricsMonth.setHour01(pdbhMetricsMonth.getHour01() + pdbh.getHour01());
            pdbhMetricsMonth.setHour02(pdbhMetricsMonth.getHour02() + pdbh.getHour02());
            pdbhMetricsMonth.setHour03(pdbhMetricsMonth.getHour03() + pdbh.getHour03());
            pdbhMetricsMonth.setHour04(pdbhMetricsMonth.getHour04() + pdbh.getHour04());
            pdbhMetricsMonth.setHour05(pdbhMetricsMonth.getHour05() + pdbh.getHour05());
            pdbhMetricsMonth.setHour06(pdbhMetricsMonth.getHour06() + pdbh.getHour06());
            pdbhMetricsMonth.setHour07(pdbhMetricsMonth.getHour07() + pdbh.getHour07());
            pdbhMetricsMonth.setHour08(pdbhMetricsMonth.getHour08() + pdbh.getHour08());
            pdbhMetricsMonth.setHour09(pdbhMetricsMonth.getHour09() + pdbh.getHour09());
            pdbhMetricsMonth.setHour10(pdbhMetricsMonth.getHour10() + pdbh.getHour10());
            pdbhMetricsMonth.setHour11(pdbhMetricsMonth.getHour11() + pdbh.getHour11());
            pdbhMetricsMonth.setHour12(pdbhMetricsMonth.getHour12() + pdbh.getHour12());
            pdbhMetricsMonth.setHour13(pdbhMetricsMonth.getHour13() + pdbh.getHour13());
            pdbhMetricsMonth.setHour14(pdbhMetricsMonth.getHour14() + pdbh.getHour14());
            pdbhMetricsMonth.setHour15(pdbhMetricsMonth.getHour15() + pdbh.getHour15());
            pdbhMetricsMonth.setHour16(pdbhMetricsMonth.getHour16() + pdbh.getHour16());
            pdbhMetricsMonth.setHour17(pdbhMetricsMonth.getHour17() + pdbh.getHour17());
            pdbhMetricsMonth.setHour18(pdbhMetricsMonth.getHour18() + pdbh.getHour18());
            pdbhMetricsMonth.setHour19(pdbhMetricsMonth.getHour19() + pdbh.getHour19());
            pdbhMetricsMonth.setHour20(pdbhMetricsMonth.getHour20() + pdbh.getHour20());
            pdbhMetricsMonth.setHour21(pdbhMetricsMonth.getHour21() + pdbh.getHour21());
            pdbhMetricsMonth.setHour22(pdbhMetricsMonth.getHour22() + pdbh.getHour22());
            pdbhMetricsMonth.setHour23(pdbhMetricsMonth.getHour23() + pdbh.getHour23());
        }

        return pdbhMetricsMonth;
    }

    public PurchaseDistributionByHourMetricsYear transformYear(Calendar calDate) {
        PurchaseDistributionByHourMetricsYear pdbhMetricsYear = new PurchaseDistributionByHourMetricsYear(CalendarUtil.createYearFormat(calDate));
        List<PurchaseDistributionByHour> rs = purchaseDistributionByHourRepository.findByDateBetween(CalendarUtil.getStartDateOfYear(calDate), CalendarUtil.getEndDateOfYear(calDate));

        //accumulate
        for (PurchaseDistributionByHour pdbh : rs) {
            pdbhMetricsYear.setHour00(pdbhMetricsYear.getHour00() + pdbh.getHour00());
            pdbhMetricsYear.setHour01(pdbhMetricsYear.getHour01() + pdbh.getHour01());
            pdbhMetricsYear.setHour02(pdbhMetricsYear.getHour02() + pdbh.getHour02());
            pdbhMetricsYear.setHour03(pdbhMetricsYear.getHour03() + pdbh.getHour03());
            pdbhMetricsYear.setHour04(pdbhMetricsYear.getHour04() + pdbh.getHour04());
            pdbhMetricsYear.setHour05(pdbhMetricsYear.getHour05() + pdbh.getHour05());
            pdbhMetricsYear.setHour06(pdbhMetricsYear.getHour06() + pdbh.getHour06());
            pdbhMetricsYear.setHour07(pdbhMetricsYear.getHour07() + pdbh.getHour07());
            pdbhMetricsYear.setHour08(pdbhMetricsYear.getHour08() + pdbh.getHour08());
            pdbhMetricsYear.setHour09(pdbhMetricsYear.getHour09() + pdbh.getHour09());
            pdbhMetricsYear.setHour10(pdbhMetricsYear.getHour10() + pdbh.getHour10());
            pdbhMetricsYear.setHour11(pdbhMetricsYear.getHour11() + pdbh.getHour11());
            pdbhMetricsYear.setHour12(pdbhMetricsYear.getHour12() + pdbh.getHour12());
            pdbhMetricsYear.setHour13(pdbhMetricsYear.getHour13() + pdbh.getHour13());
            pdbhMetricsYear.setHour14(pdbhMetricsYear.getHour14() + pdbh.getHour14());
            pdbhMetricsYear.setHour15(pdbhMetricsYear.getHour15() + pdbh.getHour15());
            pdbhMetricsYear.setHour16(pdbhMetricsYear.getHour16() + pdbh.getHour16());
            pdbhMetricsYear.setHour17(pdbhMetricsYear.getHour17() + pdbh.getHour17());
            pdbhMetricsYear.setHour18(pdbhMetricsYear.getHour18() + pdbh.getHour18());
            pdbhMetricsYear.setHour19(pdbhMetricsYear.getHour19() + pdbh.getHour19());
            pdbhMetricsYear.setHour20(pdbhMetricsYear.getHour20() + pdbh.getHour20());
            pdbhMetricsYear.setHour21(pdbhMetricsYear.getHour21() + pdbh.getHour21());
            pdbhMetricsYear.setHour22(pdbhMetricsYear.getHour22() + pdbh.getHour22());
            pdbhMetricsYear.setHour23(pdbhMetricsYear.getHour23() + pdbh.getHour23());
        }

        return pdbhMetricsYear;
    }

}