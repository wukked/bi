package com.toro.dms.transformers;

import com.toro.dms.model.PurchaseDistributionByType;
import com.toro.dms.model.PurchaseDistributionByTypeMetricsMonth;
import com.toro.dms.model.PurchaseDistributionByTypeMetricsWeek;
import com.toro.dms.model.PurchaseDistributionByTypeMetricsYear;
import com.toro.dms.repository.PurchaseDistributionByTypeRepository;
import com.toro.dms.utils.CalendarUtil;
import org.apache.spark.api.java.JavaSparkContext;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Calendar;
import java.util.List;

@Component
public class PurchaseDistributionByTypeTransformer {

    @Inject
    private PurchaseDistributionByTypeRepository purchaseDistributionByTypeRepository;

    @Inject
    private JavaSparkContext sc;

    public PurchaseDistributionByTypeMetricsWeek transformWeek(Calendar calDate) {
        PurchaseDistributionByTypeMetricsWeek pdbtMetricsWeek = new PurchaseDistributionByTypeMetricsWeek(CalendarUtil.createYearWeekFormat(calDate));
        List<PurchaseDistributionByType> rs = purchaseDistributionByTypeRepository.findByDateBetween(CalendarUtil.getStartDateOfWeek(calDate), CalendarUtil.getEndDateOfWeek(calDate));

        //accumulate
        for (PurchaseDistributionByType pdbt : rs) {
            pdbtMetricsWeek.setOfflineApproved(pdbt.getOfflineApproved() + pdbtMetricsWeek.getOfflineApproved());
            pdbtMetricsWeek.setOfflineDeclined(pdbt.getOfflineDeclined() + pdbtMetricsWeek.getOfflineDeclined());
            pdbtMetricsWeek.setOnline(pdbt.getOnline() + pdbtMetricsWeek.getOnline());
            pdbtMetricsWeek.setTapPinTap(pdbt.getTapPinTap() + pdbtMetricsWeek.getTapPinTap());
        }

        return pdbtMetricsWeek;
    }

    public PurchaseDistributionByTypeMetricsMonth transformMonth(Calendar calDate) {
        PurchaseDistributionByTypeMetricsMonth pdbtMetricsMonth = new PurchaseDistributionByTypeMetricsMonth(CalendarUtil.createYearMonthFormat(calDate));
        List<PurchaseDistributionByType> rs = purchaseDistributionByTypeRepository.findByDateBetween(CalendarUtil.getStartDateOfMonth(calDate), CalendarUtil.getEndDateOfMonth(calDate));

        //accumulate
        for (PurchaseDistributionByType pdbt : rs) {
            pdbtMetricsMonth.setOfflineApproved(pdbt.getOfflineApproved() + pdbtMetricsMonth.getOfflineApproved());
            pdbtMetricsMonth.setOfflineDeclined(pdbt.getOfflineDeclined() + pdbtMetricsMonth.getOfflineDeclined());
            pdbtMetricsMonth.setOnline(pdbt.getOnline() + pdbtMetricsMonth.getOnline());
            pdbtMetricsMonth.setTapPinTap(pdbt.getTapPinTap() + pdbtMetricsMonth.getTapPinTap());
        }

        return pdbtMetricsMonth;
    }

    public PurchaseDistributionByTypeMetricsYear transformYear(Calendar calDate) {
        PurchaseDistributionByTypeMetricsYear pdbtMetricsYear = new PurchaseDistributionByTypeMetricsYear(CalendarUtil.createYearFormat(calDate));
        List<PurchaseDistributionByType> rs = purchaseDistributionByTypeRepository.findByDateBetween(CalendarUtil.getStartDateOfYear(calDate), CalendarUtil.getEndDateOfYear(calDate));

        //accumulate
        for (PurchaseDistributionByType pdbt : rs) {
            pdbtMetricsYear.setOfflineApproved(pdbt.getOfflineApproved() + pdbtMetricsYear.getOfflineApproved());
            pdbtMetricsYear.setOfflineDeclined(pdbt.getOfflineDeclined() + pdbtMetricsYear.getOfflineDeclined());
            pdbtMetricsYear.setOnline(pdbt.getOnline() + pdbtMetricsYear.getOnline());
            pdbtMetricsYear.setTapPinTap(pdbt.getTapPinTap() + pdbtMetricsYear.getTapPinTap());
        }

        return pdbtMetricsYear;
    }

}