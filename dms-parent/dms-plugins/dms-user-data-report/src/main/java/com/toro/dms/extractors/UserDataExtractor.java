package com.toro.dms.extractors;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import com.toro.dms.constants.LogIndex;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.toro.dms.model.UserData;
import com.toro.dms.utils.CalendarUtil;
import com.toro.dms.utils.LogFileUtil;

@Component
public class UserDataExtractor implements Serializable {
	
	Logger logger = LoggerFactory.getLogger(getClass());

	private static final long serialVersionUID = -8940981437234160783L;
	@Inject
	private JavaSparkContext sc;
	
	@Inject
	private LogFileUtil logFileUtil;

	public List<UserData> extract(Calendar calDate) {

		String path = logFileUtil.getLogFileName(CalendarUtil.format(CalendarUtil.getDatePlus1(calDate)));
		
		logger.debug("starting caculation");

		JavaRDD<UserData> logData = 
				sc.textFile(path)
				.filter(l -> l.startsWith("HandleEndServiceStateChangeNotification;WM;"))
				.map(l -> l.split(";"))
				.groupBy(l -> l[LogIndex.SIR.value])
				.mapValues(v -> UserDataExtractorUtil.getLast(v.iterator()))
				.map(v -> UserDataExtractorUtil.createUserData(v._2(), calDate));
		logger.debug("done");

		return logData.collect();

	}

	

}
