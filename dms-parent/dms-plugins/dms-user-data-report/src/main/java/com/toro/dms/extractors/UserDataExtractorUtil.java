package com.toro.dms.extractors;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Iterator;

import com.toro.dms.constants.LogIndex;
import com.toro.dms.model.UserData;

public class UserDataExtractorUtil implements Serializable {

	private static final long serialVersionUID = -2091205821638494985L;

	protected static UserData createUserData(String[] data, Calendar calDate) {
		String userId = data[LogIndex.SIR.value];
		String[] extraInfo = data[LogIndex.EXTRA_INFO.value].split("=");
		int operation = Integer.parseInt(extraInfo[1].substring(0, 1));
		String executedStatus = extraInfo[2];
		return new UserData(userId, operation, executedStatus, calDate);
	}

	protected static String[] getLast(Iterator<String[]> i) {
		String[] result = null;
		String r = "";
		while (i.hasNext()) {
			String[] tmp = i.next();
			String d = tmp[LogIndex.END_TIME.value];

			if (d.compareTo(r) > 0) {
				r = d;
				result = tmp;
			}

		}
		return result;
	}
}
