package com.toro.dms.info;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class UserDataReportDetail implements ReportDetailInterface {

	@Override
	public String getName() {
		return "User Data Report";
	}

	@Override
	public String getLink() {
		return "report-ui/user-data-report/user-data-report.html";
	}

}
