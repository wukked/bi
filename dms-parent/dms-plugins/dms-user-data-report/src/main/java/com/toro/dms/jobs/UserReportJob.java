package com.toro.dms.jobs;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.toro.dms.extractors.UserDataExtractor;
import com.toro.dms.model.UserData;
import com.toro.dms.model.UserMetrics;
import com.toro.dms.repository.UserDataRepository;
import com.toro.dms.repository.UserMetricsRepository;
import com.toro.dms.transformers.UserDataTransformer;

@Component
public class UserReportJob implements Job {

	@Inject
	private UserDataExtractor userDataExtractor;
	
	@Inject
	private UserDataRepository userDataRepository;
	
	@Inject
	private UserDataTransformer userDataTransformer;
	
	@Inject
	private UserMetricsRepository userMetricsRepository;
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void executeWithCalDate(Calendar calDate) {
		
		logger.debug("do UserReportJob extractor" + calDate.getTime().toString());
		
		List<UserData> uds = userDataExtractor.extract(calDate);
		
		logger.debug("Get "+uds.size()+" UserData, now save them");
		
		if(logger.isDebugEnabled()){
			for(UserData ud :uds){
				logger.debug(ud.getUserId()+"  "+ ud.getOperation()+"  "+ ud.getExecutedStatus());
			}
		}
		
		userDataRepository.save(uds);
		
		logger.debug("do transform");
		
		List<UserMetrics> ums = userDataTransformer.transform(calDate);
		
		logger.debug("Get "+ums.size()+" UserMetrics, now save them");
		
		userMetricsRepository.save(ums);
	}


}
