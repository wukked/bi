package com.toro.dms.model;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class UserData implements Serializable{
	 /**
	 * 
	 */
	private static final long serialVersionUID = -4873353309972877707L;
	@Id
    private String userId;
    private int operation;
    private String executedStatus;
    @Temporal(TemporalType.DATE)
    private Calendar updateDate;
    
    public UserData() {
    }

    public UserData(String userId, int operation, String executedStatus, Calendar updateDate) {
        this.userId = userId;
        this.operation = operation;
        this.executedStatus = executedStatus;
        this.updateDate = updateDate;
    }

    public String getUserId(){
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getOperation(){
        return this.operation;
    }

    public void setOperation(int operation) {
        this.operation = operation;
    }

    public String getExecutedStatus(){
        return this.executedStatus;
    }

    public void setExecutedStatus(String executedStatus) {
        this.executedStatus = executedStatus;
    }

    public Calendar getUpdateDate(){
        return this.updateDate;
    }

    public void setUpdateDate(Calendar updateDate) {
        this.updateDate = updateDate;
    }
    @Override
    public String toString() {
        return String.format(
                "UserData [userId=%d, operation='%d', executedStatus='%d, updateDate='%s]",
                userId, operation, executedStatus, updateDate);
    }

}
