package com.toro.dms.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class UserMetrics implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 2884179412773349435L;
	@Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;
	@Temporal(TemporalType.DATE)
    private Date date;
    private int operation;
    private String status;
    private int count;

    public UserMetrics() {
		super();
	}

    public UserMetrics(Date date, int operation, String status, int count) {
		super();
		this.date = date;
		this.operation = operation;
		this.status = status;
		this.count = count;
	}



	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public Date getDate() {
		return date;
	}



	public void setDate(Date date) {
		this.date = date;
	}



	public int getOperation() {
		return operation;
	}



	public void setOperation(int operation) {
		this.operation = operation;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	public int getCount() {
		return count;
	}



	public void setCount(int count) {
		this.count = count;
	}



	@Override
	public String toString() {
		return "UserMetrics [id=" + id + ", date=" + date + ", operation=" + operation + ", status=" + status + ", count=" + count + "]";
	}



	
}
