package com.toro.dms.repository;

import org.springframework.data.repository.CrudRepository;

import com.toro.dms.model.UserData;


public interface UserDataRepository extends CrudRepository<UserData, String> {

}
