package com.toro.dms.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.toro.dms.model.UserMetrics;


public interface UserMetricsRepository extends CrudRepository<UserMetrics, Long> {
	
	public List<UserMetrics> findByDate(Date date);

}
