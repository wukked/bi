package com.toro.dms.rest.userreport;

import java.sql.Date;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.toro.dms.jobs.UserReportJob;
import com.toro.dms.model.UserMetrics;
import com.toro.dms.repository.UserMetricsRepository;
import com.toro.dms.retrivers.DataRetriver;

@RequestMapping("/userReport/**")
@RestController
public class UserReportController {

    private static final Logger logger = LoggerFactory.getLogger( UserReportController.class );

    @Inject
    UserMetricsRepository userMetricsRepository;
    @Inject
    UserReportJob userReportJob;

    public enum Operation{
        DEPLOYED,
        RESUMED,
        SUSPENDED,
        TERMINATED,
        UPDATED,
        UPGRADED
    }


    @RequestMapping(method = RequestMethod.GET)
    public Iterable<UserMetrics> getMetrics( HttpServletResponse response, @RequestParam(required = false) Date date) throws ParseException, Exception {
    	
    	logger.debug("do getMetrics");
//    	dataRetriver.retrieve();
//    	userReportJob.execute();
    	logger.debug("caculated....");
    	
    	
    	
    	
    	

        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");

        return userMetricsRepository.findAll();
    }

}
