package com.toro.dms.retrivers;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.LoggerFactory;

import com.toro.dms.utils.CalendarUtil;
import com.toro.dms.utils.LogFileUtil;

public class DataRetriver {

	/*
	@Inject
	private Environment environment;

	@Inject
	private LogFileUtil logFileUtil;
	*/

	LogFileUtil logFileUtil;

	public DataRetriver(){
		logFileUtil = new LogFileUtil("");
	}

	// @Value("#{abc}")
	// public final String server=null;

	private final org.slf4j.Logger logger = LoggerFactory.getLogger(getClass());

	/*
	 * public static void main(String[] args) throws Exception {
	 * 
	 * UserDataRetriver udr = new UserDataRetriver(); List<JSONArray> result =
	 * udr.getAll(); for (JSONArray r : result) { for (Object a : r)
	 * logger.debug(a + " , ");logger.debug(""); } }
	 */

	/*
	public static void main(String[] args) throws Exception {
		DataRetriver udr = new DataRetriver();
		udr.retrieve();
	}
	*/

	public void retrieve( Calendar calDate ) throws ParseException, Exception {

		FileWriter fstream = new FileWriter(logFileUtil.getLogFileName(CalendarUtil.format(calDate)), true);
		BufferedWriter out = new BufferedWriter(fstream);

		List<JSONArray> result = getAll( calDate );
		for (JSONArray r : result) {
			for (Object a : r)
				out.write(a.toString() + ";");

			out.newLine();
		}

		IOUtils.closeQuietly(fstream);
		IOUtils.closeQuietly(out);
	}

	@SuppressWarnings("unchecked")
	private List<JSONArray> getAll( Calendar calDate ) throws ParseException, Exception {
		int length = 1000;
		List<JSONArray> result = new ArrayList<JSONArray>();
		int from = 0;

		Calendar startTime = Calendar.getInstance();
		Calendar endTime = Calendar.getInstance();
		startTime.setTime(calDate.getTime());
		calDate.add(Calendar.DATE, 1);
		endTime.setTime(calDate.getTime());

		while (true) {
			JSONArray logs = this.getLogs(from, length, startTime, endTime);

			if (logs == null || logs.size() > 0) {
				from = from + length;
				result.addAll(logs);
			} else {
				return result;
			}

		}
	}

	private JSONArray getLogs(int from, int length, Calendar startTime, Calendar endTime) throws ParseException, Exception {

		String body = "{\"from\":" + from + ",\"resLength\":" + length + ",\"start\":\"" + CalendarUtil.format(startTime) + " 00:00:00\",\"end\":\"" + CalendarUtil.format(endTime)
				+ " 00:00:00\",\"filters\":{}}";
		logger.debug(body);
		JSONParser parser = new JSONParser();
		JSONObject jsonObject = (JSONObject) parser.parse(Handshake.getLogs("http://valyou-national-launch-server-qa.toro-intl.com/adminplatform-ui/services/logs/user", body));
		JSONArray logs = (JSONArray) jsonObject.get("logs");
		logs.remove(0);
		return logs;
	}

}
