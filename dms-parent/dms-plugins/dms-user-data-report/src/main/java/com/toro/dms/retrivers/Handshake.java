package com.toro.dms.retrivers;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class Handshake {
	static String getLogs(String urlStr, String body) throws Exception {
		
		// my certificate and password
		System.setProperty("javax.net.ssl.keyStore", "./tours.telenordnb.staging.toro.p12");
		System.setProperty("javax.net.ssl.keyStorePassword", "rQ2NcOXAcjD0DLzy");
		System.setProperty("javax.net.ssl.keyStoreType", "PKCS12");
		URL url = new URL(urlStr);

		HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
//		HttpsURLConnection urlConn = (HttpsURLConnection) url.openConnection();

		
//		urlConn.setRequestProperty("Content-Length", "" + 
//                Integer.toString(getBody().length));
		urlConn.setRequestMethod("POST");
    	urlConn.setRequestProperty("Content-Type", 
    	           "application/json");
    	   
    	urlConn.setRequestProperty("Content-Language", "en-US");  
    	   
    	urlConn.setUseCaches (false);
          
          
    	urlConn.setDoOutput(true);
    	urlConn.setDoInput(true);
    	
    	  OutputStreamWriter out = new OutputStreamWriter(urlConn.getOutputStream());
          out.write(body);
    	    
          out.flush();
          out.close();
    	
    	
		
		BufferedReader in = new BufferedReader(new InputStreamReader(
				urlConn.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		
		
		in.close();
		
//		urlConn.disconnect();
//		logger.debug(response.toString());
		return response.toString();
		
				
	}
	
//	 private static byte[] getBody() {
//	        ByteArrayOutputStream bao = new ByteArrayOutputStream();
//	        DataOutputStream dos = new DataOutputStream(bao);
//	        
//	       	        
//	        try {
//	            dos.writeUTF(body);
//	            
//	        } catch(IOException ioe) {
//
//	        } finally {
//	            try {
//	            if (dos != null) dos.close();
//	            } catch(IOException ioe) { ioe.printStackTrace(); }
//	        }
//	        return bao.toByteArray();
//	    }

	 
	
}
