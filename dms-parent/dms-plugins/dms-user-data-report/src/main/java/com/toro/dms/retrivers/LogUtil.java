package com.toro.dms.retrivers;


import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.slf4j.LoggerFactory;

public class LogUtil {
	
	static String startTime = "";
	
	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(LogUtil.class);

	static void writeLogs(JSONArray logs, boolean isFilter, String filter){
		for (Object log : logs) {
			JSONArray j = (JSONArray)log;
			
			String l = ""+ 
					format(j.get(0),45) +  "| "+
					format((j.get(3).toString()).substring(11),13) + "| "+
					format(j.get(5),37) + "| "+
					format(j.get(6),20) +"| "+
					format(j.get(8),40) + "| "+
					format(j.get(12),7)+"| "+
//					format(j.get(13),30)+"| "+
					format(j.get(14),0);
			
			if(isFilter && !l.contains(filter))
				continue;
			else
				logger.debug(l);
		}
	}
	
	static String format(Object o,int size){
		String a = "";
		if(o!=null)a=o.toString();
		
		return StringUtils.rightPad(a, size);
		
	}
	
}
