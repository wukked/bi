package com.toro.dms.transformers;

import java.util.AbstractMap.SimpleEntry;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.springframework.stereotype.Component;

import com.clearspring.analytics.util.Lists;
import com.toro.dms.model.UserData;
import com.toro.dms.model.UserMetrics;
import com.toro.dms.repository.UserDataRepository;
import com.toro.dms.utils.CalendarUtil;

@Component
public class UserDataTransformer {

	@Inject
	private UserDataRepository userDataRepository;

	@Inject
	private JavaSparkContext sc;

	public List<UserMetrics> transform(Calendar calDate) {
		List<UserData> uds = Lists.newArrayList(userDataRepository.findAll());

		Date date = calDate.getTime();

		JavaRDD<UserData> udRDD = sc.parallelize(uds);
		return udRDD.groupBy(ud -> new SimpleEntry<Integer, String>(ud.getOperation(), ud.getExecutedStatus()))
				.map(f -> new UserMetrics(date, f._1().getKey(), f._1().getValue(), Lists.newArrayList(f._2()).size())).collect();

	}

}
