
$( document ).ready(function() {

    var pieChart, barChart, pieChartOptions, barChartOptions; // splineChart, splineChartOptions;

    $("li.li-bar").click(function( event ) {
        loadChartInPage('report-ui/user-data-report/userDataBarChart.html', 'bar');
    });

    $("li.li-pie").click(function( event ) {
        loadChartInPage('report-ui/user-data-report/userDataPieChart.html', 'pie');
    });

    /*$("li.li-spline").click(function( event ) {
        loadChartInPage('', 'spline');
    });*/

    function loadChartInPage(url, id) {
        $("li.li-generic").each( function() {
            $(this).removeClass("active");
        });
        $("div.content-chart").each( function() {
            $(this).addClass("hidden");
        });
        $("li.li-"+id).addClass("active");
        $("#content-"+id+"-chart").removeClass("hidden");

        if(id === 'pie') {
            pieChart.destroy();
            pieChart = new Highcharts.Chart(pieChartOptions);
        }
        else {
            /*if(id === 'spline') {
                splineChart.destroy();
                splineChart = new Highcharts.Chart(splineChartOptions);
            }
            else {*/
                barChart.destroy();
                barChart = new Highcharts.Chart(barChartOptions);
            //}
        }

        return;
    }




  var statusClass = function (total_y, data, categories, name) {
    this.total_y = total_y;
    this.data = data;
    this.categories = categories;
    this.name = name;
    this.getY = function () {
      return this.total_y;
    };
    this.getCategories = function () {
      return this.categories;
    };
    this.getData = function () {
      return this.data;
    };
    this.getName = function () {
      return this.name;
    };
  };

  var getIdFromArray = function (array, id) {
    var result = -1;
    for( var i = 0; i < array.length; i++ ) {
      if(array[i].category == id) {
        result = i;
        break;
      }
    }
    return result;
  };

  var getIdFromArray2 = function (array, id) {
    var result = -1;
    for( var i = 0; i < array.length; i++ ) {
      if(array[i].category == id) {
        result = i;
        break;
      }
    }
    return result;
  };


   //$.get("userReport/getMetrics", function(obj,status) {
   $.ajax({
       url: "userReport/getMetrics", // "report-ui/user-data-report/entry.json",
       dataType: 'json',
       cache: false,

       beforeSend: function () {
           console.log("Loading");
       },

       error: function (jqXHR, textStatus, errorThrown) {
           console.log(jqXHR);
           console.log(textStatus);
           console.log(errorThrown);
       },

       success: function(obj) {

    //************** pieChart
    var categoriesList = [];

    _.forEach(obj, function(data) {

      var pointer = getIdFromArray2(categoriesList, data.status);
      var num = parseInt(data.count);
      if(pointer == -1) {
        actualCat = {"category": data.status, "total": num, "operations": [] };
        actualCat.operations.push({"category": data.operation, "count": num});
        categoriesList.push(actualCat);
      }
      else {
        categoriesList[pointer].total += num;
        var pointer2 = getIdFromArray(categoriesList[pointer].operations, data.operation);
        if(pointer2 == -1) {
          categoriesList[pointer].operations.push({"category": data.operation, "count": num});
        }
        else {
          categoriesList[pointer].operations[pointer2].count += num;
        }
      }
    });

    var countColor = 0;

    var colors = Highcharts.getOptions().colors,
        categories = _.pluck(categoriesList, 'category'),
        data = _.map(categoriesList, function (actCat) {
          var result = {
            y: actCat.total,
            color: colors[countColor],
            drilldown: {
                name: actCat.category,
                categories: _.pluck(actCat.operations, "category"),
                data: _.pluck(actCat.operations, "count"),
                color: colors[countColor]
            }
          };
          countColor = countColor + 1;
          return result;
        }),
        mainData = [],
        subData = [],
        i,
        j,
        dataLen = data.length,
        drillDataLen,
        brightness;

    // Build the data arrays
    for (i = 0; i < dataLen; i += 1) {

        // add main data
        mainData.push({
            name: categories[i],
            y: data[i].y,
            color: data[i].color
        });

        // add sub data
        drillDataLen = data[i].drilldown.data.length;
        for (j = 0; j < drillDataLen; j += 1) {
            brightness = 0.2 - (j / drillDataLen) / 5;
            subData.push({
                name: data[i].drilldown.categories[j],
                y: data[i].drilldown.data[j],
                color: Highcharts.Color(data[i].color).brighten(brightness).get()
            });
        }
    }

    // Create the chart
    pieChartOptions = {
        chart: {
            type: 'pie',
            renderTo: 'content-pie-chart'
        },
        title: {
            text: ''
        },
        yAxis: {
            title: {
                text: 'Total Success/Fail'
            }
        },
        plotOptions: {
            pie: {
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    },
                    connectorColor: 'silver'
                },
                showInLegend: true,
                shadow: false

            }
        },
        tooltip: {
            valueSuffix: ' Instances'
        },
        series: [{
            name: 'User',
            data: mainData,
            size: '60%',
            dataLabels: {
                formatter: function () {
                    return this.y > 5 ? this.point.name : null;
                },
                color: 'white',
                distance: -50
            }
        }, {
            name: 'Service',
            data: subData,
            size: '85%',
            innerSize: '60%',
            dataLabels: {
                formatter: function () {
                    // display only if larger than 1
                    return this.y > 1 ? '<b>' + this.point.name + ':</b> ' + this.y + '%'  : null;
                }
            }
        }]
    };
    setTimeout(function(){
      pieChart = new Highcharts.Chart(pieChartOptions);
    }, 1000);

    //************** barChart
    var datesCat = [];
    var statusArr = [];

    _.forEach(obj, function(data) {

      var existStatus = _.findIndex(statusArr, function(d) {
        return d.name == data.status;
      });
      if(existStatus == -1) {
        statusArr.push({ "name": data.status , "data": []});
      }

      var pointer1 = _.findIndex(datesCat, function(d) {
        return d.date == data.date;
      });

      if(pointer1 == -1) {
        //alert("ERROR -> Not possible!");
        var category = data.status;
        var num = parseInt(data.count);
        datesCat.push({
          "date": data.date,
          "array": [
            {"category":category, "count": num}
          ],
          "total": num
        });
      }
      else {
        var category = data.status;
        var num = parseInt(data.count);
        datesCat[pointer1].total += num;
        var pointer2 = getIdFromArray(datesCat[pointer1].array, category);
        if(pointer2 == -1) {
          datesCat[pointer1].array.push({"category":category, "count": num});
        }
        else {
          datesCat[pointer1].array[pointer2].count += num;
        }
      }
    });


    _.forEach(statusArr, function(data) {
        for(var i = 0; i < _.pluck(datesCat, 'date').length; i++) {
            data.data.push(0);
        }
    });

    _.forEach(statusArr, function(data) {
        // data -> { name: "STATUS", data: [] }
        var counter = 0;
        _.forEach(datesCat, function (data2) {
          // data2 -> { array: [], date: "DD-MM-YYYY", total: XX }
          _.forEach(data2.array, function (data3) {
            // data3 -> { category: "STATUS", count: XX }
            if(data3.category == data.name) {
              data.data[counter] = data3.count;
            }
          });
          counter++;
      });
    });


    /*[{
        name: 'Success',
        data: [5, 3, 4, 7, 2]
    }, {
        name: 'Fail',
        data: [2, 2, 3, 2, 1]
    }, {
        name: 'Expire',
        data: [3, 4, 4, 2, 5]
    }]*/

    barChartOptions = {
        chart: {
            type: 'column',
            renderTo: 'content-bar-chart'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: _.pluck(datesCat, 'date')
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: -70,
            verticalAlign: 'top',
            y: 20,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
            }
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black, 0 0 3px black'
                    }
                }
            }
        },
        series: statusArr
    };
    barChart = new Highcharts.Chart(barChartOptions);

  }

  });

  //************** AreaSpline Chart
      /*splineChartOptions = {
          chart: {
              type: 'areaspline',
              renderTo: 'content-spline-chart'
          },
          title: {
              text: 'Average payment during last week'
          },
          legend: {
              layout: 'vertical',
              align: 'left',
              verticalAlign: 'top',
              x: 150,
              y: 100,
              floating: true,
              borderWidth: 1,
              backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
          },
          xAxis: {
              categories: [
                  'Monday',
                  'Tuesday',
                  'Wednesday',
                  'Thursday',
                  'Friday',
                  'Saturday',
                  'Sunday'
              ],
              plotBands: [{ // visualize the weekend
                  from: 4.5,
                  to: 6.5,
                  color: 'rgba(68, 170, 213, .2)'
              }]
          },
          yAxis: {
              title: {
                  text: 'Units'
              }
          },
          tooltip: {
              shared: true,
              valueSuffix: ' units'
          },
          credits: {
              enabled: false
          },
          plotOptions: {
              areaspline: {
                  fillOpacity: 0.5
              }
          },
          series: [{
              name: 'Online Payment',
              data: [3, 4, 3, 5, 4, 10, 12]
          }, {
              name: 'Offline Payment',
              data: [1, 3, 4, 3, 3, 5, 4]
          }]
      };
      splineChart = new Highcharts.Chart(splineChartOptions);*/

});