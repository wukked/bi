package com.toro.dms.extractors;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Map;

import javax.inject.Inject;

import org.apache.spark.api.java.JavaSparkContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.google.common.collect.Iterables;
import com.toro.dms.constants.LogIndex;
import com.toro.dms.model.WidgetData;
import com.toro.dms.utils.CalendarUtil;
import com.toro.dms.utils.LogFileUtil;

@Component
public class WidgetDataExtractor implements Serializable {

	Logger logger = LoggerFactory.getLogger(getClass());

	private static final long serialVersionUID = -8940981437234160783L;
	@Inject
	private JavaSparkContext sc;

	@Inject
	private LogFileUtil logFileUtil;

	/**
	 * Filter logs to get the extracted data for widget events. The return value
	 * is a Map as: key = operation_executedStatus , e.g Install_FAILED value =
	 * list of WidgetData
	 * 
	 * @param date
	 * @return Map<String, Iterable<WidgetData>>
	 */
	public Map<String, Iterable<WidgetData>> extract(Calendar date) {

		String path = logFileUtil.getLogFileName(CalendarUtil.format(CalendarUtil.getDatePlus1(date)));
		Map<String, Iterable<WidgetData>> result = sc.textFile(path)
				.filter(l -> (l.startsWith("HandleEndServiceStateChangeNotification;MNO-TSM;") && (l.contains("operation=1") || l.contains("operation=6")))).map(l -> l.split(";"))
				.sortBy(l -> l[LogIndex.END_TIME.value], true, 1).groupBy(l -> l[LogIndex.SIR.value]).map(r -> Iterables.getLast(r._2()))
				.map(l -> new WidgetData(l[LogIndex.SIR.value], l[LogIndex.EXTRA_INFO.value].substring(20), date, Integer.parseInt(l[LogIndex.EXTRA_INFO.value].substring(10, 11))))
				.groupBy(w -> new StringBuffer(w.getOperation() + "_" + w.getExecutedStatus()).toString()).collectAsMap();

		return result;

	}

}
