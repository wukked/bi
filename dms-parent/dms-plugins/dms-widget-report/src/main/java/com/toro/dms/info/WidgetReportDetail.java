package com.toro.dms.info;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(501)
public class WidgetReportDetail implements ReportDetailInterface {

	@Override
	public String getName() {
		return "Widget Download Analysis";
	}

	@Override
	public String getLink() {
		return "report-ui/widget-download-analysis-report.html";
	}

}
