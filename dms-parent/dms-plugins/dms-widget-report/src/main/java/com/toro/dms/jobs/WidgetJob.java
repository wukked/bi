package com.toro.dms.jobs;

import com.google.common.collect.Iterables;
import com.toro.dms.extractors.WidgetDataExtractor;
import com.toro.dms.model.*;
import com.toro.dms.repository.*;
import com.toro.dms.transformers.WidgetNumberTransformer;
import com.toro.dms.utils.CalendarUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Calendar;
import java.util.Map;
import java.util.Vector;

@Component
//Run after DeployService, reliance on DeployService data
@Order(300)
public class WidgetJob implements Job {

	@Inject
	private WidgetDataExtractor widgetDataExtractor;

	@Inject
	private WidgetDataRepository widgetDataRepository;

	@Inject
	private WidgetNumberRepository widgetNumberRepository;

	@Inject
	private WidgetNumberTransformer widgetNumberTransformer;

	@Inject
	private WidgetMetricsWeekRepository widgetMetricsWeekRepository;

	@Inject
	private WidgetMetricsMonthsRepository widgetMetricsMonthsRepository;

	@Inject
	private WidgetMetricsMonthsSnapshotRepository widgetMetricsMonthsSnapshotRepository;

	@Inject
	private WidgetMetricsYearsRepository widgetMetricsYearsRepository;


	private final Iterable<WidgetData> empty = new Vector<WidgetData>();

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void executeWithCalDate(Calendar calDate) {

		logger.debug("do WidgetJob extractor " + calDate.getTime().toString());

		Map<String, Iterable<WidgetData>> wdResult = widgetDataExtractor.extract(calDate);

		logger.debug("extractors finished");

		logger.debug("saving WidgetData");

		wdResult.values().forEach(l -> widgetDataRepository.save(l));

		WidgetNumber widgetNumber = new WidgetNumber(calDate, Iterables.size(wdResult.getOrDefault("Install_EXECUTED_SUCCESS", empty)), Iterables.size(wdResult.getOrDefault("Install_FAILED", empty)), Iterables.size(wdResult.getOrDefault("Delete_EXECUTED_SUCCESS", empty)));

		logger.debug("Get mapped WidgetNumber, now save them");

		if (logger.isDebugEnabled()) {
			logger.debug(widgetNumber.getDate().getTime() + "  " + widgetNumber.getSuccess() + "  " + widgetNumber.getFailed() + "  " + widgetNumber.getDeleted());
		}

		widgetNumberRepository.save(widgetNumber);

		logger.debug("do transform deploy service metrics transformations");

		WidgetMetricsWeek widgetMetricsWeek = widgetNumberTransformer.transformWeek(calDate);
		WidgetMetricsMonth widgetMetricsMonth = widgetNumberTransformer.transformMonth(calDate);
		WidgetMetricsYear widgetMetricsYear = widgetNumberTransformer.transformYear(calDate);
		WidgetMetricsMonthSnapshot widgetMetricsMonthSnapshot = widgetNumberTransformer.transformMonthSnapshot(calDate);

		logger.debug("WidgetMetrics transformed, now save them");

		widgetMetricsWeekRepository.save(widgetMetricsWeek);
		widgetMetricsMonthsRepository.save(widgetMetricsMonth);
		widgetMetricsYearsRepository.save(widgetMetricsYear);
		widgetMetricsMonthsSnapshotRepository.save(widgetMetricsMonthSnapshot);

	}

}