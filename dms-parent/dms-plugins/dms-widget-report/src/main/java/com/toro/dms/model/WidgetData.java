package com.toro.dms.model;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.*;

@Entity
public class WidgetData implements Serializable {

    private static final long serialVersionUID = -1000353319287711107L;
    @Id
    @Column(unique = true)
    private String sir;
    private String executedStatus;
    @Temporal(TemporalType.DATE)
    private Calendar updateDate;
    private String operation;

    public WidgetData() {
    }

    public WidgetData(String sir, String executedStatus, Calendar updateDate, int operation) {
        this.sir = sir;
        this.executedStatus = executedStatus;
        this.updateDate = updateDate;
        if (operation == 1) {
            this.operation = "Install";
        } else if (operation == 6) {
            this.operation = "Delete";
        } else
            throw new RuntimeException("Operation " + operation + " not reconized");
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getSir() {
        return sir;
    }

    public void setSir(String sir) {
        this.sir = sir;
    }

    public String getExecutedStatus() {
        return this.executedStatus;
    }

    public void setExecutedStatus(String executedStatus) {
        this.executedStatus = executedStatus;
    }

    public Calendar getUpdateDate() {
        return this.updateDate;
    }

    public void setUpdateDate(Calendar updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        return "WidgetData [sir=" + sir + ", executedStatus=" + executedStatus + ", updateDate=" + updateDate + ", operation=" + operation + "]";
    }


}
