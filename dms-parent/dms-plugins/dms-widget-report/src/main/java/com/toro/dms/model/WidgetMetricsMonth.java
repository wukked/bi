package com.toro.dms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class WidgetMetricsMonth extends WidgetType implements Serializable {

    private static final long serialVersionUID = -3112533099755771707L;
    @Id
    @Column(unique = true)
    private String yearMonth;

    public WidgetMetricsMonth() {
        super();
    }

    public WidgetMetricsMonth(String yearMonth) {
        super();
        this.yearMonth = yearMonth;
    }

    public WidgetMetricsMonth(String yearMonth, int success, int failed, int deleted) {
        super(success, failed, deleted);
        this.yearMonth = yearMonth;
    }

    public String getYearMonth() {
        return yearMonth;
    }

    public void setYearMonth(String yearMonth) {
        this.yearMonth = yearMonth;
    }

}
