package com.toro.dms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class WidgetMetricsMonthSnapshot extends WidgetType implements Serializable {

    private static final long serialVersionUID = -2173353302972817717L;
    @Id
    @Column(unique = true)
    private String yearMonth;

    public WidgetMetricsMonthSnapshot() {
        super();
    }

    public WidgetMetricsMonthSnapshot(String yearMonth) {
        super();
        this.yearMonth = yearMonth;
    }

    public WidgetMetricsMonthSnapshot(String yearMonth, int success, int failed, int deleted) {
        super(success, failed, deleted);
        this.yearMonth = yearMonth;
    }

    public String getYearMonth() {
        return yearMonth;
    }

    public void setYearMonth(String yearMonth) {
        this.yearMonth = yearMonth;
    }

}
