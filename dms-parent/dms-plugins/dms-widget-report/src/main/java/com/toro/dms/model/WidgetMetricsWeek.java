package com.toro.dms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class WidgetMetricsWeek extends WidgetType implements Serializable {

    private static final long serialVersionUID = -4813323309272877707L;
    @Id
    @Column(unique = true)
    private String yearWeek;

    public WidgetMetricsWeek() {
        super();
    }

    public WidgetMetricsWeek(String yearWeek) {
        super();
        this.yearWeek = yearWeek;
    }

    public WidgetMetricsWeek(String yearWeek, int success, int failed, int deleted) {
        super(success, failed, deleted);
        this.yearWeek = yearWeek;
    }

    public String getYearWeek() {
        return yearWeek;
    }

    public void setYearWeek(String yearWeek) {
        this.yearWeek = yearWeek;
    }

}
