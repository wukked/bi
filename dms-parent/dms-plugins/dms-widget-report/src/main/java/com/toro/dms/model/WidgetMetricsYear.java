package com.toro.dms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class WidgetMetricsYear extends WidgetType implements Serializable {

    private static final long serialVersionUID = -4878888309972822207L;
    @Id
    @Column(unique = true)
    private String year;

    public WidgetMetricsYear() {
        super();
    }

    public WidgetMetricsYear(String yearMonth) {
        super();
        this.year = yearMonth;
    }

    public WidgetMetricsYear(String yearMonth, int success, int failed, int deleted) {
        super(success, failed, deleted);
        this.year = yearMonth;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

}
