package com.toro.dms.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;

@Entity
public class WidgetNumber extends WidgetType implements Serializable {

    private static final long serialVersionUID = -1873313322972117707L;
    @Id
    @Temporal(TemporalType.DATE)
    @Column(unique = true)
    private Calendar date;

    public WidgetNumber() {
        super();
    }

    public WidgetNumber(Calendar date, int success, int failed, int deleted) {
        super(success, failed, deleted);
        this.date = date;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

}
