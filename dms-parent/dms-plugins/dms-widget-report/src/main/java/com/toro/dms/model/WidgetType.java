package com.toro.dms.model;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class WidgetType {

    private int success;
    private int failed;
    private int deleted;

    public WidgetType(){

    }

    public WidgetType(int success, int failed, int deleted) {
        this();
        this.success = success;
        this.failed = failed;
        this.deleted = deleted;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public int getFailed() {
        return failed;
    }

    public void setFailed(int failed) {
        this.failed = failed;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public long totalWidget(){
        return this.success + this.failed + this.deleted;
    }

    public double successPercent(){
        return (double)this.getSuccess()/(double)totalWidget();
    }

    public double failedPercent(){
        return (double)this.getFailed()/(double)totalWidget();
    }

    public double deletedPercent(){
        return (double)this.getDeleted()/(double)totalWidget();
    }
}
