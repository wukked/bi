package com.toro.dms.repository;

import com.toro.dms.model.WidgetData;
import org.springframework.data.repository.CrudRepository;

import java.util.Calendar;
import java.util.List;

public interface WidgetDataRepository extends CrudRepository<WidgetData, String> {

    public List<WidgetData> findByUpdateDate(Calendar updateDate);
    public List<WidgetData> findByUpdateDateBetweenAndOperationEqualsAndExecutedStatusEquals(Calendar start, Calendar end, String operation, String executedStatus);

}
