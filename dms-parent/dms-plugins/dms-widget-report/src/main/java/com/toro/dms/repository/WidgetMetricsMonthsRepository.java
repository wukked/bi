package com.toro.dms.repository;

import com.toro.dms.model.WidgetMetricsMonth;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface WidgetMetricsMonthsRepository extends CrudRepository<WidgetMetricsMonth, Long> {

    public List<WidgetMetricsMonth> findByYearMonth(String yearMonth);

}
