package com.toro.dms.repository;

import com.toro.dms.model.WidgetMetricsMonthSnapshot;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface WidgetMetricsMonthsSnapshotRepository extends CrudRepository<WidgetMetricsMonthSnapshot, Long> {

    public List<WidgetMetricsMonthSnapshot> findByYearMonth(String yearMonth);

}
