package com.toro.dms.repository;

import com.toro.dms.model.WidgetMetricsWeek;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface WidgetMetricsWeekRepository extends CrudRepository<WidgetMetricsWeek, Long> {

    public List<WidgetMetricsWeek> findByYearWeek(String yearWeek);

}
