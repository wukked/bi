package com.toro.dms.repository;

import com.toro.dms.model.WidgetMetricsYear;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface WidgetMetricsYearsRepository extends CrudRepository<WidgetMetricsYear, Long> {

    public List<WidgetMetricsYear> findByYear(String year);

}
