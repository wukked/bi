package com.toro.dms.repository;

import com.toro.dms.model.WidgetNumber;
import org.springframework.data.repository.CrudRepository;

import java.util.Calendar;
import java.util.List;

public interface WidgetNumberRepository extends CrudRepository<WidgetNumber, String> {

    public List<WidgetNumber> findByDate(Calendar date);
    public List<WidgetNumber> findByDateBetween(Calendar start, Calendar end);

}
