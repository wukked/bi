package com.toro.dms.rest;

import java.util.Calendar;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import com.toro.dms.utils.CommonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.toro.dms.model.DeployServiceMetricsMonth;
import com.toro.dms.model.DeployServiceMetricsMonthSnapshot;
import com.toro.dms.model.DeployServiceMetricsWeek;
import com.toro.dms.model.DeployServiceMetricsYear;
import com.toro.dms.model.DeployServiceNumber;
import com.toro.dms.model.WidgetMetricsMonth;
import com.toro.dms.model.WidgetMetricsMonthSnapshot;
import com.toro.dms.model.WidgetMetricsWeek;
import com.toro.dms.model.WidgetMetricsYear;
import com.toro.dms.model.WidgetNumber;
import com.toro.dms.model.WidgetType;
import com.toro.dms.repository.DeployServiceMetricsMonthsRepository;
import com.toro.dms.repository.DeployServiceMetricsMonthsSnapshotRepository;
import com.toro.dms.repository.DeployServiceMetricsWeekRepository;
import com.toro.dms.repository.DeployServiceMetricsYearsRepository;
import com.toro.dms.repository.DeployServiceNumberRepository;
import com.toro.dms.repository.WidgetMetricsMonthsRepository;
import com.toro.dms.repository.WidgetMetricsMonthsSnapshotRepository;
import com.toro.dms.repository.WidgetMetricsWeekRepository;
import com.toro.dms.repository.WidgetMetricsYearsRepository;
import com.toro.dms.repository.WidgetNumberRepository;
import com.toro.dms.utils.CalendarUtil;

@RequestMapping("/widgetReport")
@RestController
public class WidgetReportController {

    private static final Logger logger = LoggerFactory.getLogger(WidgetReportController.class);

    @Inject
    WidgetNumberRepository widgetNumberRepository;

    @Inject
    WidgetMetricsWeekRepository widgetMetricsWeekRepository;

    @Inject
    WidgetMetricsMonthsRepository widgetMetricsMonthsRepository;

    @Inject
    WidgetMetricsYearsRepository widgetMetricsYearsRepository;

    @Inject
    WidgetMetricsMonthsSnapshotRepository widgetMetricsMonthsSnapshotRepository;

    @Inject
    DeployServiceNumberRepository deployServiceNumberRepository;

    @Inject
    DeployServiceMetricsWeekRepository deployServiceMetricsWeekRepository;

    @Inject
    DeployServiceMetricsMonthsRepository deployServiceMetricsMonthsRepository;

    @Inject
    DeployServiceMetricsMonthsSnapshotRepository deployServiceMetricsMonthsSnapshotRepository;

    @Inject
    DeployServiceMetricsYearsRepository deployServiceMetricsYearsRepository;


    @RequestMapping(method = RequestMethod.GET, value = "/getYesterday")
    public String getYesterday(HttpServletResponse response) {

        logger.debug("do get yesterday");
        logger.debug("caculated....");

        Calendar yesterday = CalendarUtil.getYesterday1();

        setCsvResponseHeader(response, "Widget_DayReport.csv");

        Iterable<WidgetNumber> rs = widgetNumberRepository.findByDate(yesterday);

        String csv = createCsvHeaderTitles();

        for (WidgetNumber wn : rs) {
            csv += createCsvRow(wn);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getDay")
    public String getDay(HttpServletResponse response, @RequestParam(required = true) Calendar calDate) {

        logger.debug("do get date");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "Widget_DayReport.csv");

        Iterable<WidgetNumber> rs = widgetNumberRepository.findByDate(calDate);

        String csv = createCsvHeaderTitles();

        for (WidgetNumber wn : rs) {
            csv += createCsvRow(wn);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllDays")
    public String getAllDays(HttpServletResponse response) {

        logger.debug("do get all days");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "Widget_AllDaysReport.csv");

        Iterable<WidgetNumber> rs = widgetNumberRepository.findAll();

        String csv = createCsvHeaderTitles();

        for (WidgetNumber wn : rs) {
            csv += createCsvRow(wn);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getCurrentWeek")
    public String getCurrentWeek(HttpServletResponse response) {

        logger.debug("do get current week");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "Widget_WeekReport.csv");

        //get current week/year
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int week = now.get(Calendar.WEEK_OF_YEAR);

        Iterable<WidgetMetricsWeek> rs = widgetMetricsWeekRepository.findByYearWeek(Integer.toString(year) + "-" + Integer.toString(week));

        String csv = createCsvHeaderTitles();

        for (WidgetMetricsWeek wn : rs) {
            csv += createCsvRow(wn);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllWeeks")
    public String getAllWeeks(HttpServletResponse response) {

        logger.debug("do get all weeks");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "Widget_AllWeekReport.csv");

        Iterable<WidgetMetricsWeek> rs = widgetMetricsWeekRepository.findAll();

        String csv = createCsvHeaderTitles();

        for (WidgetMetricsWeek wn : rs) {
            csv += createCsvRow(wn);
        }
        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getCurrentMonth")
    public String getCurrentMonth(HttpServletResponse response) {

        logger.debug("do get month");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "Widget_MonthReport.csv");

        //get current week/year
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH) + 1;

        Iterable<WidgetMetricsMonth> rs = widgetMetricsMonthsRepository.findByYearMonth(Integer.toString(year) + "-" + Integer.toString(month));

        String csv = createCsvHeaderTitles();

        for (WidgetMetricsMonth wn : rs) {
            csv += createCsvRow(wn);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllMonths")
    public String getAllMonths(HttpServletResponse response) {

        logger.debug("do get all months");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "Widget_AllMonthsReport.csv");

        Iterable<WidgetMetricsMonth> rs = widgetMetricsMonthsRepository.findAll();

        String csv = createCsvHeaderTitles();

        for (WidgetMetricsMonth wn : rs) {
            csv += createCsvRow(wn);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getLastMonthSnapshot")
    public String getLastMonthSnapshot(HttpServletResponse response) {

        logger.debug("do get month snapshot");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "Widget_LastMonthSnapshotReport.csv");

        //get current week/year
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH) + 1;

        Iterable<WidgetMetricsMonthSnapshot> rs = widgetMetricsMonthsSnapshotRepository.findByYearMonth(Integer.toString(year) + "-" + Integer.toString(month));

        String csv = createCsvHeaderTitles();

        for (WidgetMetricsMonthSnapshot wn : rs) {
            csv += createCsvRow(wn);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllMonthSnapshots")
    public String getAllMonthSnapshots(HttpServletResponse response) {

        logger.debug("do get all month snapshot");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "Widget_AllMonthSnapshotReport.csv");

        Iterable<WidgetMetricsMonthSnapshot> rs = widgetMetricsMonthsSnapshotRepository.findAll();

        String csv = createCsvHeaderTitles();

        for (WidgetMetricsMonthSnapshot wn : rs) {
            csv += createCsvRow(wn);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getCurrentYear")
    public String getCurrentYear(HttpServletResponse response) {

        logger.debug("do get current year");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "Widget_YearReport.csv");

        //get current year
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);

        Iterable<WidgetMetricsYear> rs = widgetMetricsYearsRepository.findByYear(Integer.toString(year));

        String csv = createCsvHeaderTitles();

        for (WidgetMetricsYear wn : rs) {
            csv += createCsvRow(wn);
        }

        return csv;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllYears")
    public String getAllYears(HttpServletResponse response) {

        logger.debug("do get all years");
        logger.debug("caculated....");

        setCsvResponseHeader(response, "Widget_AllYearReport.csv");

        Iterable<WidgetMetricsYear> rs = widgetMetricsYearsRepository.findAll();

        String csv = createCsvHeaderTitles();

        for (WidgetMetricsYear wn : rs) {
            csv += createCsvRow(wn);
        }

        return csv;
    }

    private void setCsvResponseHeader(HttpServletResponse response, String fileName) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
        response.setContentType("application/x-download");
        response.addHeader("Content-Disposition", "filename=\"" + fileName + "\"");
    }

    private String createCsvHeaderTitles() {
        return "Time,Widget Install started,Widget Install started %,Widget Install failed,Widget Install failed %,Widget Install succeeded,Widget Install succeeded %,Widget deleted,Widget deleted %\r\n";
    }

    private String createCsvRow(WidgetType rs) {
        logger.debug("Create CSV row");

        Calendar date;
        String time = "";
        double wStartedPer = 0;
        double totalDcSucceeded = 0;

        if (rs instanceof WidgetNumber) {
            date = ((WidgetNumber) rs).getDate();
            time = CalendarUtil.format(((WidgetNumber) rs).getDate());

            Iterable<DeployServiceNumber> dsNumberResult = deployServiceNumberRepository.findByDate(date);
            for( DeployServiceNumber ds:dsNumberResult){
                totalDcSucceeded += ds.getSuccess();
            }
            wStartedPer = (double)(rs.getSuccess() + rs.getFailed() + rs.getDeleted()) / totalDcSucceeded;

        } else if (rs instanceof WidgetMetricsWeek) {
            time = ((WidgetMetricsWeek) rs).getYearWeek();

            Iterable<DeployServiceMetricsWeek> dsMetricWeekResult = deployServiceMetricsWeekRepository.findByYearWeek(time);
            for( DeployServiceMetricsWeek ds:dsMetricWeekResult){
                totalDcSucceeded += ds.getSuccess();
            }
            wStartedPer = (double)(rs.getSuccess() + rs.getFailed() + rs.getDeleted()) / totalDcSucceeded;

        } else if (rs instanceof WidgetMetricsMonth) {
            time = ((WidgetMetricsMonth) rs).getYearMonth();

            Iterable<DeployServiceMetricsMonth> dsMetricMonthResult = deployServiceMetricsMonthsRepository.findByYearMonth(time);
            for( DeployServiceMetricsMonth ds:dsMetricMonthResult){
                totalDcSucceeded += ds.getSuccess();
            }
            wStartedPer = (double)(rs.getSuccess() + rs.getFailed() + rs.getDeleted()) / totalDcSucceeded;

        } else if (rs instanceof WidgetMetricsYear) {
            time = ((WidgetMetricsYear) rs).getYear();

            Iterable<DeployServiceMetricsYear> dsMetricYearResult = deployServiceMetricsYearsRepository.findByYear(time);
            for( DeployServiceMetricsYear ds:dsMetricYearResult){
                totalDcSucceeded += ds.getSuccess();
            }
            wStartedPer = (double)(rs.getSuccess() + rs.getFailed() + rs.getDeleted()) / totalDcSucceeded;

        } else if (rs instanceof WidgetMetricsMonthSnapshot) {
            time = ((WidgetMetricsMonthSnapshot) rs).getYearMonth();

            Iterable<DeployServiceMetricsMonthSnapshot> dsMetricMonthSnapshotResult = deployServiceMetricsMonthsSnapshotRepository.findByYearMonth(time);
            for( DeployServiceMetricsMonthSnapshot ds:dsMetricMonthSnapshotResult){
                totalDcSucceeded += ds.getSuccess();
            }
            wStartedPer = (double)(rs.getSuccess() + rs.getFailed() + rs.getDeleted()) / totalDcSucceeded;

        } else {
            logger.debug("Error finding result set class type");
        }

        return time + "," +
                rs.totalWidget() + "," +
                CommonUtil.percentageFormat(wStartedPer) + "," +
                rs.getFailed() + "," +
                CommonUtil.percentageFormat(rs.failedPercent()) + "," +
                rs.getSuccess() + "," +
                CommonUtil.percentageFormat(rs.successPercent()) + "," +
                rs.getDeleted() + "," +
                CommonUtil.percentageFormat(rs.deletedPercent()) +
                "\r\n";
    }

}