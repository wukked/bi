package com.toro.dms.transformers;

import com.toro.dms.model.*;
import com.toro.dms.repository.WidgetDataRepository;
import com.toro.dms.repository.WidgetNumberRepository;
import com.toro.dms.utils.CalendarUtil;
import org.apache.spark.api.java.JavaSparkContext;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class WidgetNumberTransformer {

    @Inject
    private WidgetNumberRepository widgetNumberRepository;

    @Inject
    private WidgetDataRepository widgetDataRepository;

    @Inject
    private JavaSparkContext sc;

    public WidgetMetricsWeek transformWeek(Calendar calDate){

        calDate.setFirstDayOfWeek(Calendar.MONDAY);

        WidgetMetricsWeek widgetMetricsWeek = new WidgetMetricsWeek( CalendarUtil.createYearWeekFormat(calDate) );
        List<WidgetNumber> rs = widgetNumberRepository.findByDateBetween(CalendarUtil.getStartDateOfWeek(calDate), CalendarUtil.getEndDateOfWeek(calDate));

        //accumulate
        for( WidgetNumber wn : rs ){
            widgetMetricsWeek.setSuccess(widgetMetricsWeek.getSuccess() + wn.getSuccess());
            widgetMetricsWeek.setFailed(widgetMetricsWeek.getFailed() + wn.getFailed());
            widgetMetricsWeek.setDeleted(widgetMetricsWeek.getDeleted() + wn.getDeleted());
        }

        return widgetMetricsWeek;
    }

    public WidgetMetricsMonth transformMonth(Calendar calDate){

        WidgetMetricsMonth widgetMetricsMonth = new WidgetMetricsMonth(CalendarUtil.createYearMonthFormat(calDate));
        List<WidgetNumber> rs = widgetNumberRepository.findByDateBetween(CalendarUtil.getStartDateOfMonth(calDate), CalendarUtil.getEndDateOfMonth(calDate));

        //accumulate
        for( WidgetNumber wn : rs ){
            widgetMetricsMonth.setSuccess(widgetMetricsMonth.getSuccess() + wn.getSuccess());
            widgetMetricsMonth.setFailed(widgetMetricsMonth.getFailed() + wn.getFailed());
            widgetMetricsMonth.setDeleted(widgetMetricsMonth.getDeleted() + wn.getDeleted());
        }

        return widgetMetricsMonth;
    }

    public WidgetMetricsYear transformYear(Calendar calDate) {

        WidgetMetricsYear widgetMetricsYear = new WidgetMetricsYear(CalendarUtil.createYearFormat(calDate));
        List<WidgetNumber> rs = widgetNumberRepository.findByDateBetween(CalendarUtil.getStartDateOfYear(calDate), CalendarUtil.getEndDateOfYear(calDate));

        //accumulate
        for( WidgetNumber wn : rs ){
            widgetMetricsYear.setSuccess(widgetMetricsYear.getSuccess() + wn.getSuccess());
            widgetMetricsYear.setFailed(widgetMetricsYear.getFailed() + wn.getFailed());
            widgetMetricsYear.setDeleted(widgetMetricsYear.getDeleted() + wn.getDeleted());
        }

        return widgetMetricsYear;
    }

    public WidgetMetricsMonthSnapshot transformMonthSnapshot(Calendar calDate){

        WidgetMetricsMonthSnapshot widgetMetricsMonthSnapshot = new WidgetMetricsMonthSnapshot(CalendarUtil.createYearMonthFormat(calDate));
        List<WidgetData> rsInstallSuccess = widgetDataRepository.findByUpdateDateBetweenAndOperationEqualsAndExecutedStatusEquals(CalendarUtil.getStartDateOfMonth(calDate), CalendarUtil.getEndDateOfMonth(calDate), "Install", "EXECUTED_SUCCESS");
        List<WidgetData> rsInstallFail = widgetDataRepository.findByUpdateDateBetweenAndOperationEqualsAndExecutedStatusEquals(CalendarUtil.getStartDateOfMonth(calDate), CalendarUtil.getEndDateOfMonth(calDate), "Install", "FAILED");
        List<WidgetData> rsDeletedSuccess = widgetDataRepository.findByUpdateDateBetweenAndOperationEqualsAndExecutedStatusEquals(CalendarUtil.getStartDateOfMonth(calDate), CalendarUtil.getEndDateOfMonth(calDate), "Delete", "EXECUTED_SUCCESS");

        //accumulate
        widgetMetricsMonthSnapshot.setSuccess(rsInstallSuccess.size());
        widgetMetricsMonthSnapshot.setFailed(rsInstallFail.size());
        widgetMetricsMonthSnapshot.setDeleted(rsDeletedSuccess.size());

        return widgetMetricsMonthSnapshot;
    }
}