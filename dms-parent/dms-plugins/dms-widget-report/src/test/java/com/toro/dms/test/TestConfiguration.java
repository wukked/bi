package com.toro.dms.test;


import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.springframework.context.annotation.Bean;

import com.toro.dms.extractors.WidgetDataExtractor;
import com.toro.dms.utils.LogFileUtil;

public class TestConfiguration {
	@Bean
	public WidgetDataExtractor widgetDataExtractor() {
		return new WidgetDataExtractor();
	}

	@Bean
	public JavaSparkContext sc() {
		return new JavaSparkContext(new SparkConf().setAppName("Toro-DMS-test").setMaster("local"));
	}
	
	@Bean
	public LogFileUtil logFileUtil() {
		return new LogFileUtil("");
	}
}
