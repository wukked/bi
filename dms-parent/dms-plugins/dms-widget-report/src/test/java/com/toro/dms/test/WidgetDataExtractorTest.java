package com.toro.dms.test;


import java.util.Calendar;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.toro.dms.extractors.WidgetDataExtractor;
import com.toro.dms.model.WidgetData;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = { TestConfiguration.class })
@PropertySource(value = "classpath:spark.properties")
public class WidgetDataExtractorTest {

	@Autowired
	private WidgetDataExtractor widgetDataExtractor;
	
	Logger logger = LoggerFactory.getLogger(getClass());

	@Test
	public void test() {
		
	
		
		
		Map<String, Iterable<WidgetData>> result = widgetDataExtractor.extract(Calendar.getInstance());
		logger.debug(result.toString());
		

	}


}
